## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Amazon Web Services EC2. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Amazon Web Services EC2.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Awsec2. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

Note that each call with the `STSRole` suffix contains an input `stsParams`. This will contain information needed to authenticate by switching roles using the AWS STS Service. In addition to this information, you can optionally pass an alternate region than the one specified in the service instance configuration with the key `region`.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">acceptReservedInstancesExchangeQuoteSTSRole(dryRun, reservedInstanceId, targetConfiguration, stsParams, roleName, callback)</td>
    <td style="padding:15px">Accepts the Convertible Reserved Instance exchange quote described in the  GetReservedInstancesExchangeQuote  call.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">acceptTransitGatewayVpcAttachmentSTSRole(transitGatewayAttachmentId, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Accepts a request to attach a VPC to a transit gateway.   The VPC attachment must be in the  pendingAcceptance  state. Use  DescribeTransitGatewayVpcAttachments  to view your pending VPC attachment requests. Use  RejectTransitGatewayVpcAttachment  to reject a VPC attachment request.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">acceptVpcEndpointConnectionsSTSRole(dryRun, serviceId, vpcEndpointId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Accepts one or more interface VPC endpoint connection requests to your VPC endpoint service.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">acceptVpcPeeringConnectionSTSRole(dryRun, vpcPeeringConnectionId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Accept a VPC peering connection request. To accept a request, the VPC peering connection must be in the  pending-acceptance  state, and you must be the owner of the peer VPC. Use  DescribeVpcPeeringConnections  to view your outstanding VPC peering connection requests.   For an inter-Region VPC peering connection request, you must accept the VPC peering connection in the Region of the accepter VPC.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">advertiseByoipCidrSTSRole(cidr, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Advertises an IPv4 address range that is provisioned for use with your AWS resources through bring your own IP addresses (BYOIP).   You can perform this operation at most once every 10 seconds, even if you specify different address ranges each time.   We recommend that you stop advertising the BYOIP CIDR from other locations when you advertise it from AWS. To minimize down time, you can configure your AWS resources to use an address from a BYOIP CIDR before it is advertised, and then simultaneo...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">allocateAddressSTSRole(domain = 'vpc', address, publicIpv4Pool, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Allocates an Elastic IP address to your AWS account. After you allocate the Elastic IP address you can associate it with an instance or network interface. After you release an Elastic IP address, it is released to the IP address pool and can be allocated to a different AWS account.   You can allocate an Elastic IP address from an address pool owned by AWS or from an address pool created from a public IPv4 address range that you have brought to AWS for use with your AWS resources using bring you...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">allocateHostsSTSRole(autoPlacement = 'on', availabilityZone, clientToken, instanceType, quantity, tagSpecification, stsParams, roleName, callback)</td>
    <td style="padding:15px">Allocates a Dedicated Host to your account. At a minimum, specify the instance size type, Availability Zone, and quantity of hosts to allocate.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applySecurityGroupsToClientVpnTargetNetworkSTSRole(clientVpnEndpointId, vpcId, securityGroupId, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Applies a security group to the association between the target network and the Client VPN endpoint. This action replaces the existing security groups with the specified security groups.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignIpv6AddressesSTSRole(ipv6AddressCount, ipv6Addresses, networkInterfaceId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Assigns one or more IPv6 addresses to the specified network interface. You can specify one or more specific IPv6 addresses, or you can specify the number of IPv6 addresses to be automatically assigned from within the subnet's IPv6 CIDR block range. You can assign as many IPv6 addresses to a network interface as you can assign private IPv4 addresses, and the limit varies per instance type. For information, see  IP Addresses Per Network Interface Per Instance Type  in the  Amazon Elastic Compute C...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignPrivateIpAddressesSTSRole(allowReassignment, networkInterfaceId, privateIpAddress, secondaryPrivateIpAddressCount, stsParams, roleName, callback)</td>
    <td style="padding:15px">Assigns one or more secondary private IP addresses to the specified network interface.   You can specify one or more specific secondary IP addresses, or you can specify the number of secondary IP addresses to be automatically assigned within the subnet's CIDR block range. The number of secondary IP addresses that you can assign to an instance varies by instance type. For information about instance types, see  Instance Types  in the  Amazon Elastic Compute Cloud User Guide . For more information...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">associateAddressSTSRole(allocationId, instanceId, publicIp, allowReassociation, dryRun, networkInterfaceId, privateIpAddress, stsParams, roleName, callback)</td>
    <td style="padding:15px">Associates an Elastic IP address with an instance or a network interface. Before you can use an Elastic IP address, you must allocate it to your account.   An Elastic IP address is for use in either the EC2-Classic platform or in a VPC. For more information, see  Elastic IP Addresses  in the  Amazon Elastic Compute Cloud User Guide .   [EC2-Classic, VPC in an EC2-VPC-only account] If the Elastic IP address is already associated with a different instance, it is disassociated from that instance a...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">associateClientVpnTargetNetworkSTSRole(clientVpnEndpointId, subnetId, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Associates a target network with a Client VPN endpoint. A target network is a subnet in a VPC. You can associate multiple subnets from the same VPC with a Client VPN endpoint. You can associate only one subnet in each Availability Zone. We recommend that you associate at least two subnets to provide Availability Zone redundancy.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">associateDhcpOptionsSTSRole(dhcpOptionsId, vpcId, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Associates a set of DHCP options (that you've previously created) with the specified VPC, or associates no DHCP options with the VPC.   After you associate the options with the VPC, any existing instances and all new instances that you launch in that VPC use the options. You don't need to restart or relaunch the instances. They automatically pick up the changes within a few hours, depending on how frequently the instance renews its DHCP lease. You can explicitly renew the lease using the operat...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">associateIamInstanceProfileSTSRole(iamInstanceProfileArn, iamInstanceProfileName, instanceId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Associates an IAM instance profile with a running or stopped instance. You cannot associate more than one IAM instance profile with an instance.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">associateRouteTableSTSRole(dryRun, routeTableId, subnetId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Associates a subnet with a route table. The subnet and route table must be in the same VPC. This association causes traffic originating from the subnet to be routed according to the routes in the route table. The action returns an association ID, which you need in order to disassociate the route table from the subnet later. A route table can be associated with multiple subnets.   For more information, see  Route Tables  in the  Amazon Virtual Private Cloud User Guide .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">associateSubnetCidrBlockSTSRole(ipv6CidrBlock, subnetId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Associates a CIDR block with your subnet. You can only associate a single IPv6 CIDR block with your subnet. An IPv6 CIDR block must have a prefix length of /64.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">associateTransitGatewayRouteTableSTSRole(transitGatewayRouteTableId, transitGatewayAttachmentId, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Associates the specified attachment with the specified transit gateway route table. You can associate only one route table with an attachment.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">associateVpcCidrBlockSTSRole(amazonProvidedIpv6CidrBlock, cidrBlock, vpcId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Associates a CIDR block with your VPC. You can associate a secondary IPv4 CIDR block, or you can associate an Amazon-provided IPv6 CIDR block. The IPv6 CIDR block size is fixed at /56.   For more information about associating CIDR blocks with your VPC and applicable restrictions, see  VPC and Subnet Sizing  in the  Amazon Virtual Private Cloud User Guide .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">attachClassicLinkVpcSTSRole(dryRun, securityGroupId, instanceId, vpcId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Links an EC2-Classic instance to a ClassicLink-enabled VPC through one or more of the VPC's security groups. You cannot link an EC2-Classic instance to more than one VPC at a time. You can only link an instance that's in the  running  state. An instance is automatically unlinked from a VPC when it's stopped - you can link it to the VPC again when you restart it.   After you've linked an instance, you cannot change the VPC security groups that are associated with it. To change the security group...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">attachInternetGatewaySTSRole(dryRun, internetGatewayId, vpcId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Attaches an internet gateway to a VPC, enabling connectivity between the internet and the VPC. For more information about your VPC and internet gateway, see the  Amazon Virtual Private Cloud User Guide .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">attachNetworkInterfaceSTSRole(deviceIndex, dryRun, instanceId, networkInterfaceId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Attaches a network interface to an instance.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">attachVolumeSTSRole(device, instanceId, volumeId, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Attaches an EBS volume to a running or stopped instance and exposes it to the instance with the specified device name.   Encrypted EBS volumes may only be attached to instances that support Amazon EBS encryption. For more information, see  Amazon EBS Encryption  in the  Amazon Elastic Compute Cloud User Guide .   For a list of supported device names, see  Attaching an EBS Volume to an Instance . Any device names that aren't reserved for instance store volumes can be used for EBS volumes. For mo...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">attachVpnGatewaySTSRole(vpcId, vpnGatewayId, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Attaches a virtual private gateway to a VPC. You can attach one virtual private gateway to one VPC at a time.   For more information, see  AWS Site-to-Site VPN  in the  AWS Site-to-Site VPN User Guide .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">authorizeClientVpnIngressSTSRole(clientVpnEndpointId, targetNetworkCidr, accessGroupId, authorizeAllGroups, description, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Adds an ingress authorization rule to a Client VPN endpoint. Ingress authorization rules act as firewall rules that grant access to networks. You must configure ingress authorization rules to enable clients to access resources in AWS or on-premises networks.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">authorizeSecurityGroupEgressSTSRole(dryRun, groupId, ipPermissions, cidrIp, fromPort, ipProtocol, toPort, sourceSecurityGroupName, sourceSecurityGroupOwnerId, stsParams, roleName, callback)</td>
    <td style="padding:15px">[VPC only] Adds the specified egress rules to a security group for use with a VPC.   An outbound rule permits instances to send traffic to the specified destination IPv4 or IPv6 CIDR address ranges, or to the specified destination security groups for the same VPC.   You specify a protocol for each rule (for example, TCP). For the TCP and UDP protocols, you must also specify the destination port or port range. For the ICMP protocol, you must also specify the ICMP type and code. You can use -1 fo...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">authorizeSecurityGroupIngressSTSRole(cidrIp, fromPort, groupId, groupName, ipPermissions, ipProtocol, sourceSecurityGroupName, sourceSecurityGroupOwnerId, toPort, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Adds the specified ingress rules to a security group.   An inbound rule permits instances to receive traffic from the specified destination IPv4 or IPv6 CIDR address ranges, or from the specified destination security groups.   You specify a protocol for each rule (for example, TCP). For TCP and UDP, you must also specify the destination port or port range. For ICMP/ICMPv6, you must also specify the ICMP/ICMPv6 type and code. You can use -1 to mean all types or all codes.   Rule changes are prop...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">bundleInstanceSTSRole(instanceId, storageS3, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Bundles an Amazon instance store-backed Windows instance.   During bundling, only the root device volume (C:\) is bundled. Data on other instance store volumes is not preserved.     This action is not applicable for Linux/Unix instances or Windows instances that are backed by Amazon EBS.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cancelBundleTaskSTSRole(bundleId, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Cancels a bundling operation for an instance store-backed Windows instance.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cancelCapacityReservationSTSRole(capacityReservationId, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Cancels the specified Capacity Reservation, releases the reserved capacity, and changes the Capacity Reservation's state to  cancelled .   Instances running in the reserved capacity continue running until you stop them. Stopped instances that target the Capacity Reservation can no longer launch. Modify these instances to either target a different Capacity Reservation, launch On-Demand Instance capacity, or run in any open Capacity Reservation that has matching attributes and sufficient capacity...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cancelConversionTaskSTSRole(conversionTaskId, dryRun, reasonMessage, stsParams, roleName, callback)</td>
    <td style="padding:15px">Cancels an active conversion task. The task can be the import of an instance or volume. The action removes all artifacts of the conversion, including a partially uploaded volume or instance. If the conversion is complete or is in the process of transferring the final disk image, the command fails and returns an exception.   For more information, see  Importing a Virtual Machine Using the Amazon EC2 CLI .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cancelExportTaskSTSRole(exportTaskId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Cancels an active export task. The request removes all artifacts of the export, including any partially-created Amazon S3 objects. If the export task is complete or is in the process of transferring the final disk image, the command fails and returns an error.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cancelImportTaskSTSRole(cancelReason, dryRun, importTaskId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Cancels an in-process import virtual machine or import snapshot task.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cancelReservedInstancesListingSTSRole(reservedInstancesListingId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Cancels the specified Reserved Instance listing in the Reserved Instance Marketplace.   For more information, see  Reserved Instance Marketplace  in the  Amazon Elastic Compute Cloud User Guide .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cancelSpotFleetRequestsSTSRole(dryRun, spotFleetRequestId, terminateInstances, stsParams, roleName, callback)</td>
    <td style="padding:15px">Cancels the specified Spot Fleet requests.   After you cancel a Spot Fleet request, the Spot Fleet launches no new Spot Instances. You must specify whether the Spot Fleet should also terminate its Spot Instances. If you terminate the instances, the Spot Fleet request enters the  cancelled_terminating  state. Otherwise, the Spot Fleet request enters the  cancelled_running  state and the instances continue to run until they are interrupted or you terminate them manually.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cancelSpotInstanceRequestsSTSRole(dryRun, spotInstanceRequestId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Cancels one or more Spot Instance requests.     Canceling a Spot Instance request does not terminate running Spot Instances associated with the request.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">confirmProductInstanceSTSRole(instanceId, productCode, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Determines whether a product code is associated with an instance. This action can only be used by the owner of the product code. It is useful when a product code owner must verify whether another user's instance is eligible for support.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">copyFpgaImageSTSRole(dryRun, sourceFpgaImageId, description, name, sourceRegion, clientToken, stsParams, roleName, callback)</td>
    <td style="padding:15px">Copies the specified Amazon FPGA Image (AFI) to the current Region.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">copyImageSTSRole(clientToken, description, encrypted, kmsKeyId, name, sourceImageId, sourceRegion, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Initiates the copy of an AMI from the specified source Region to the current Region. You specify the destination Region by using its endpoint when making the request.   Copies of encrypted backing snapshots for the AMI are encrypted. Copies of unencrypted backing snapshots remain unencrypted, unless you set  Encrypted  during the copy operation. You cannot create an unencrypted copy of an encrypted backing snapshot.   For more information about the prerequisites and limits when copying an AMI, ...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">copySnapshotSTSRole(description, destinationRegion, encrypted, kmsKeyId, presignedUrl, sourceRegion, sourceSnapshotId, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Copies a point-in-time snapshot of an EBS volume and stores it in Amazon S3. You can copy the snapshot within the same Region or from one Region to another. You can use the snapshot to create EBS volumes or Amazon Machine Images (AMIs). The snapshot is copied to the regional endpoint that you send the HTTP request to.   Copies of encrypted EBS snapshots remain encrypted. Copies of unencrypted snapshots remain unencrypted, unless the  Encrypted  flag is specified during the snapshot copy operati...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCapacityReservationSTSRole(clientToken, instanceType, instancePlatform = 'Linux/UNIX', availabilityZone, tenancy = 'default', instanceCount, ebsOptimized, ephemeralStorage, endDate, endDateType = 'unlimited', instanceMatchCriteria = 'open', tagSpecifications, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates a new Capacity Reservation with the specified attributes.   Capacity Reservations enable you to reserve capacity for your Amazon EC2 instances in a specific Availability Zone for any duration. This gives you the flexibility to selectively add capacity reservations and still get the Regional RI discounts for that usage. By creating Capacity Reservations, you ensure that you always have access to Amazon EC2 capacity when you need it, for as long as you need it. For more information, see  ...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createClientVpnEndpointSTSRole(clientCidrBlock, serverCertificateArn, authentication, connectionLogOptionsEnabled, connectionLogOptionsCloudwatchLogGroup, connectionLogOptionsCloudwatchLogStream, dnsServers, transportProtocol = 'tcp', description, dryRun, clientToken, tagSpecification, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates a Client VPN endpoint. A Client VPN endpoint is the resource you create and configure to enable and manage client VPN sessions. It is the destination endpoint at which all client VPN sessions are terminated.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createClientVpnRouteSTSRole(clientVpnEndpointId, destinationCidrBlock, targetVpcSubnetId, description, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Adds a route to a network to a Client VPN endpoint. Each Client VPN endpoint has a route table that describes the available destination network routes. Each route in the route table specifies the path for traﬃc to speciﬁc resources or networks.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createCustomerGatewaySTSRole(bgpAsn, ipAddress, type = 'ipsec.1', dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Provides information to AWS about your VPN customer gateway device. The customer gateway is the appliance at your end of the VPN connection. (The device on the AWS side of the VPN connection is the virtual private gateway.) You must provide the Internet-routable IP address of the customer gateway's external interface. The IP address must be static and may be behind a device performing network address translation (NAT).   For devices that use Border Gateway Protocol (BGP), you can also provide t...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDefaultSubnetSTSRole(availabilityZone, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates a default subnet with a size  /20  IPv4 CIDR block in the specified Availability Zone in your default VPC. You can have only one default subnet per Availability Zone. For more information, see  Creating a Default Subnet  in the  Amazon Virtual Private Cloud User Guide .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDefaultVpcSTSRole(dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates a default VPC with a size  /16  IPv4 CIDR block and a default subnet in each Availability Zone. For more information about the components of a default VPC, see  Default VPC and Default Subnets  in the  Amazon Virtual Private Cloud User Guide . You cannot specify the components of the default VPC yourself.   If you deleted your previous default VPC, you can create a default VPC. You cannot have more than one default VPC per Region.   If your account supports EC2-Classic, you cannot use t...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createDhcpOptionsSTSRole(dhcpConfiguration, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates a set of DHCP options for your VPC. After creating the set, you must associate it with the VPC, causing all existing and new instances that you launch in the VPC to use this set of DHCP options. The following are the individual DHCP options you can specify. For more information about the options, see  RFC 2132 .         domain-name-servers  - The IP addresses of up to four domain name servers, or AmazonProvidedDNS. The default DHCP option set specifies AmazonProvidedDNS. If specifying m...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createEgressOnlyInternetGatewaySTSRole(clientToken, dryRun, vpcId, stsParams, roleName, callback)</td>
    <td style="padding:15px">[IPv6 only] Creates an egress-only internet gateway for your VPC. An egress-only internet gateway is used to enable outbound communication over IPv6 from instances in your VPC to the internet, and prevents hosts outside of your VPC from initiating an IPv6 connection with your instance.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createFleetSTSRole(dryRun, clientToken, spotOptionsAllocationStrategy, spotOptionsInstanceInterruptionBehavior, spotOptionsInstancePoolsToUseCount, spotOptionsSingleInstanceType, spotOptionsSingleAvailabilityZone, spotOptionsMinTargetCapacity, onDemandOptionsAllocationStrategy, onDemandOptionsSingleInstanceType, onDemandOptionsSingleAvailabilityZone, onDemandOptionsMinTargetCapacity, excessCapacityTerminationPolicy = 'no-termination', launchTemplateConfigs, targetCapacitySpecificationTotalTargetCapacity, targetCapacitySpecificationOnDemandTargetCapacity, targetCapacitySpecificationSpotTargetCapacity, targetCapacitySpecificationDefaultTargetCapacityType, terminateInstancesWithExpiration, type = 'request', validFrom, validUntil, replaceUnhealthyInstances, tagSpecification, stsParams, roleName, callback)</td>
    <td style="padding:15px">Launches an EC2 Fleet.   You can create a single EC2 Fleet that includes multiple launch specifications that vary by instance type, AMI, Availability Zone, or subnet.   For more information, see  Launching an EC2 Fleet  in the  Amazon Elastic Compute Cloud User Guide .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createFlowLogsSTSRole(dryRun, clientToken, deliverLogsPermissionArn, logGroupName, resourceId, resourceType = 'VPC', trafficType = 'ACCEPT', logDestinationType = 'cloud-watch-logs', logDestination, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates one or more flow logs to capture information about IP traffic for a specific network interface, subnet, or VPC.    Flow log data for a monitored network interface is recorded as flow log records, which are log events consisting of fields that describe the traffic flow. For more information, see  Flow Log Records  in the  Amazon Virtual Private Cloud User Guide .   When publishing to CloudWatch Logs, flow log records are published to a log group, and each network interface has a unique l...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createFpgaImageSTSRole(dryRun, inputStorageLocationBucket, inputStorageLocationKey, logsStorageLocationBucket, logsStorageLocationKey, description, name, clientToken, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates an Amazon FPGA Image (AFI) from the specified design checkpoint (DCP).   The create operation is asynchronous. To verify that the AFI is ready for use, check the output logs.   An AFI contains the FPGA bitstream that is ready to download to an FPGA. You can securely deploy an AFI on multiple FPGA-accelerated instances. For more information, see the  AWS FPGA Hardware Development Kit .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createImageSTSRole(blockDeviceMapping, description, dryRun, instanceId, name, noReboot, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates an Amazon EBS-backed AMI from an Amazon EBS-backed instance that is either running or stopped.   If you customized your instance with instance store volumes or EBS volumes in addition to the root device volume, the new AMI contains block device mapping information for those volumes. When you launch an instance from this new AMI, the instance automatically launches with those additional volumes.   For more information, see  Creating Amazon EBS-Backed Linux AMIs  in the  Amazon Elastic Co...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createInstanceExportTaskSTSRole(description, exportToS3ContainerFormat, exportToS3DiskImageFormat, exportToS3S3Bucket, exportToS3S3Prefix, instanceId, targetEnvironment = 'citrix', stsParams, roleName, callback)</td>
    <td style="padding:15px">Exports a running or stopped instance to an S3 bucket.   For information about the supported operating systems, image formats, and known limitations for the types of instances you can export, see  Exporting an Instance as a VM Using VM Import/Export  in the  VM Import/Export User Guide .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createInternetGatewaySTSRole(dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates an internet gateway for use with a VPC. After creating the internet gateway, you attach it to a VPC using  AttachInternetGateway .   For more information about your VPC and internet gateway, see the  Amazon Virtual Private Cloud User Guide .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createKeyPairSTSRole(keyName, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates a 2048-bit RSA key pair with the specified name. Amazon EC2 stores the public key and displays the private key for you to save to a file. The private key is returned as an unencrypted PEM encoded PKCS#1 private key. If a key with the specified name already exists, Amazon EC2 returns an error.   You can have up to five thousand key pairs per Region.   The key pair returned to you is available only in the Region in which you create it. If you prefer, you can create your own key pair using...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createLaunchTemplateSTSRole(dryRun, clientToken, launchTemplateName, versionDescription, launchTemplateDataKernelId, launchTemplateDataEbsOptimized, launchTemplateDataIamInstanceProfile, launchTemplateDataBlockDeviceMappings, launchTemplateDataNetworkInterfaces, launchTemplateDataImageId, launchTemplateDataInstanceType, launchTemplateDataKeyName, launchTemplateDataMonitoring, launchTemplateDataPlacement, launchTemplateDataRamDiskId, launchTemplateDataDisableApiTermination, launchTemplateDataInstanceInitiatedShutdownBehavior, launchTemplateDataUserData, launchTemplateDataTagSpecifications, launchTemplateDataElasticGpuSpecifications, launchTemplateDataElasticInferenceAccelerators, launchTemplateDataSecurityGroupIds, launchTemplateDataSecurityGroups, launchTemplateDataInstanceMarketOptions, launchTemplateDataCreditSpecification, launchTemplateDataCpuOptions, launchTemplateDataCapacityReservationSpecification, launchTemplateDataLicenseSpecifications, launchTemplateDataHibernationOptions, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates a launch template. A launch template contains the parameters to launch an instance. When you launch an instance using  RunInstances , you can specify a launch template instead of providing the launch parameters in the request.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createLaunchTemplateVersionSTSRole(dryRun, clientToken, launchTemplateId, launchTemplateName, sourceVersion, versionDescription, launchTemplateDataKernelId, launchTemplateDataEbsOptimized, launchTemplateDataIamInstanceProfile, launchTemplateDataBlockDeviceMappings, launchTemplateDataNetworkInterfaces, launchTemplateDataImageId, launchTemplateDataInstanceType, launchTemplateDataKeyName, launchTemplateDataMonitoring, launchTemplateDataPlacement, launchTemplateDataRamDiskId, launchTemplateDataDisableApiTermination, launchTemplateDataInstanceInitiatedShutdownBehavior, launchTemplateDataUserData, launchTemplateDataTagSpecifications, launchTemplateDataElasticGpuSpecifications, launchTemplateDataElasticInferenceAccelerators, launchTemplateDataSecurityGroupIds, launchTemplateDataSecurityGroups, launchTemplateDataInstanceMarketOptions, launchTemplateDataCreditSpecification, launchTemplateDataCpuOptions, launchTemplateDataCapacityReservationSpecification, launchTemplateDataLicenseSpecifications, launchTemplateDataHibernationOptions, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates a new version for a launch template. You can specify an existing version of launch template from which to base the new version.   Launch template versions are numbered in the order in which they are created. You cannot specify, change, or replace the numbering of launch template versions.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNatGatewaySTSRole(allocationId, clientToken, subnetId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates a NAT gateway in the specified public subnet. This action creates a network interface in the specified subnet with a private IP address from the IP address range of the subnet. Internet-bound traffic from a private subnet can be routed to the NAT gateway, therefore enabling instances in the private subnet to connect to the internet. For more information, see  NAT Gateways  in the  Amazon Virtual Private Cloud User Guide .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNetworkAclSTSRole(dryRun, vpcId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates a network ACL in a VPC. Network ACLs provide an optional layer of security (in addition to security groups) for the instances in your VPC.   For more information, see  Network ACLs  in the  Amazon Virtual Private Cloud User Guide .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNetworkAclEntrySTSRole(cidrBlock, dryRun, egress, icmpCode, icmpType, ipv6CidrBlock, networkAclId, portRangeFrom, portRangeTo, protocol, ruleAction = 'allow', ruleNumber, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates an entry (a rule) in a network ACL with the specified rule number. Each network ACL has a set of numbered ingress rules and a separate set of numbered egress rules. When determining whether a packet should be allowed in or out of a subnet associated with the ACL, we process the entries in the ACL according to the rule numbers, in ascending order. Each network ACL has a set of ingress rules and a separate set of egress rules.   We recommend that you leave room between the rule numbers (f...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNetworkInterfaceSTSRole(description, dryRun, securityGroupId, ipv6AddressCount, ipv6Addresses, privateIpAddress, privateIpAddresses, secondaryPrivateIpAddressCount, interfaceType = 'efa', subnetId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates a network interface in the specified subnet.   For more information about network interfaces, see  Elastic Network Interfaces  in the  Amazon Virtual Private Cloud User Guide .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNetworkInterfacePermissionSTSRole(networkInterfaceId, awsAccountId, awsService, permission = 'INSTANCE-ATTACH', dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Grants an AWS-authorized account permission to attach the specified network interface to an instance in their account.   You can grant permission to a single AWS account only, and only one account at a time.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPlacementGroupSTSRole(dryRun, groupName, strategy = 'cluster', partitionCount, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates a placement group in which to launch instances. The strategy of the placement group determines how the instances are organized within the group.    A  cluster  placement group is a logical grouping of instances within a single Availability Zone that benefit from low network latency, high network throughput. A  spread  placement group places instances on distinct hardware. A  partition  placement group places groups of instances in different partitions, where instances in one partition d...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createReservedInstancesListingSTSRole(clientToken, instanceCount, priceSchedules, reservedInstancesId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates a listing for Amazon EC2 Standard Reserved Instances to be sold in the Reserved Instance Marketplace. You can submit one Standard Reserved Instance listing at a time. To get a list of your Standard Reserved Instances, you can use the  DescribeReservedInstances  operation.     Only Standard Reserved Instances can be sold in the Reserved Instance Marketplace. Convertible Reserved Instances cannot be sold.     The Reserved Instance Marketplace matches sellers who want to resell Standard Re...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRouteSTSRole(destinationCidrBlock, destinationIpv6CidrBlock, dryRun, egressOnlyInternetGatewayId, gatewayId, instanceId, natGatewayId, transitGatewayId, networkInterfaceId, routeTableId, vpcPeeringConnectionId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates a route in a route table within a VPC.   You must specify one of the following targets: internet gateway or virtual private gateway, NAT instance, NAT gateway, VPC peering connection, network interface, or egress-only internet gateway.   When determining how to route traffic, we use the route with the most specific match. For example, traffic is destined for the IPv4 address  192.0.2.3 , and the route table includes the following two IPv4 routes:         192.0.2.0/24  (goes to some targ...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createRouteTableSTSRole(dryRun, vpcId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates a route table for the specified VPC. After you create a route table, you can add routes and associate the table with a subnet.   For more information, see  Route Tables  in the  Amazon Virtual Private Cloud User Guide .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSecurityGroupSTSRole(groupDescription, groupName, vpcId, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates a security group.   A security group acts as a virtual firewall for your instance to control inbound and outbound traffic. For more information, see  Amazon EC2 Security Groups  in the  Amazon Elastic Compute Cloud User Guide  and  Security Groups for Your VPC  in the  Amazon Virtual Private Cloud User Guide .   When you create a security group, you specify a friendly name of your choice. You can have a security group for use in EC2-Classic with the same name as a security group for use...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSnapshotSTSRole(description, volumeId, tagSpecification, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates a snapshot of an EBS volume and stores it in Amazon S3. You can use snapshots for backups, to make copies of EBS volumes, and to save data before shutting down an instance.   When a snapshot is created, any AWS Marketplace product codes that are associated with the source volume are propagated to the snapshot.   You can take a snapshot of an attached volume that is in use. However, snapshots only capture data that has been written to your EBS volume at the time the snapshot command is i...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSpotDatafeedSubscriptionSTSRole(bucket, dryRun, prefix, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates a data feed for Spot Instances, enabling you to view Spot Instance usage logs. You can create one data feed per AWS account. For more information, see  Spot Instance Data Feed  in the  Amazon EC2 User Guide for Linux Instances .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSubnetSTSRole(availabilityZone, availabilityZoneId, cidrBlock, ipv6CidrBlock, vpcId, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates a subnet in an existing VPC.   When you create each subnet, you provide the VPC ID and IPv4 CIDR block for the subnet. After you create a subnet, you can't change its CIDR block. The size of the subnet's IPv4 CIDR block can be the same as a VPC's IPv4 CIDR block, or a subset of a VPC's IPv4 CIDR block. If you create more than one subnet in a VPC, the subnets' CIDR blocks must not overlap. The smallest IPv4 subnet (and VPC) you can create uses a /28 netmask (16 IPv4 addresses), and the l...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTagsSTSRole(dryRun, resourceId, tag, stsParams, roleName, callback)</td>
    <td style="padding:15px">Adds or overwrites the specified tags for the specified Amazon EC2 resource or resources. Each resource can have a maximum of 50 tags. Each tag consists of a key and optional value. Tag keys must be unique per resource.   For more information about tags, see  Tagging Your Resources  in the  Amazon Elastic Compute Cloud User Guide . For more information about creating IAM policies that control users' access to resources based on tags, see  Supported Resource-Level Permissions for Amazon EC2 API ...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTransitGatewaySTSRole(description, optionsAmazonSideAsn, optionsAutoAcceptSharedAttachments, optionsDefaultRouteTableAssociation, optionsDefaultRouteTablePropagation, optionsVpnEcmpSupport, optionsDnsSupport, tagSpecification, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates a transit gateway.   You can use a transit gateway to interconnect your virtual private clouds (VPC) and on-premises networks. After the transit gateway enters the  available  state, you can attach your VPCs and VPN connections to the transit gateway.   To attach your VPCs, use  CreateTransitGatewayVpcAttachment .   To attach a VPN connection, use  CreateCustomerGateway  to create a customer gateway and specify the ID of the customer gateway and the ID of the transit gateway in a call t...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTransitGatewayRouteSTSRole(destinationCidrBlock, transitGatewayRouteTableId, transitGatewayAttachmentId, blackhole, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates a static route for the specified transit gateway route table.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTransitGatewayRouteTableSTSRole(transitGatewayId, tagSpecifications, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates a route table for the specified transit gateway.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createTransitGatewayVpcAttachmentSTSRole(transitGatewayId, vpcId, subnetIds, optionsDnsSupport, optionsIpv6Support, tagSpecifications, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Attaches the specified VPC to the specified transit gateway.   If you attach a VPC with a CIDR range that overlaps the CIDR range of a VPC that is already attached, the new VPC CIDR range is not propagated to the default propagation route table.   To send VPC traffic to an attached transit gateway, add a route to the VPC route table using  CreateRoute .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createVolumeSTSRole(availabilityZone, encrypted, iops, kmsKeyId, size, snapshotId, volumeType = 'standard', dryRun, tagSpecification, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates an EBS volume that can be attached to an instance in the same Availability Zone. The volume is created in the regional endpoint that you send the HTTP request to. For more information see  Regions and Endpoints .   You can create a new empty volume or restore a volume from an EBS snapshot. Any AWS Marketplace product codes from the snapshot are propagated to the volume.   You can create encrypted volumes with the  Encrypted  parameter. Encrypted volumes may only be attached to instances...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createVpcSTSRole(cidrBlock, amazonProvidedIpv6CidrBlock, dryRun, instanceTenancy = 'default', stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates a VPC with the specified IPv4 CIDR block. The smallest VPC you can create uses a /28 netmask (16 IPv4 addresses), and the largest uses a /16 netmask (65,536 IPv4 addresses). For more information about how large to make your VPC, see  Your VPC and Subnets  in the  Amazon Virtual Private Cloud User Guide .   You can optionally request an Amazon-provided IPv6 CIDR block for the VPC. The IPv6 CIDR block uses a /56 prefix length, and is allocated from Amazon's pool of IPv6 addresses. You can...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createVpcEndpointSTSRole(dryRun, vpcEndpointType = 'Interface', vpcId, serviceName, policyDocument, routeTableId, subnetId, securityGroupId, clientToken, privateDnsEnabled, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates a VPC endpoint for a specified service. An endpoint enables you to create a private connection between your VPC and the service. The service may be provided by AWS, an AWS Marketplace partner, or another AWS account. For more information, see  VPC Endpoints  in the  Amazon Virtual Private Cloud User Guide .   A  gateway  endpoint serves as a target for a route in your route table for traffic destined for the AWS service. You can specify an endpoint policy to attach to the endpoint that ...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createVpcEndpointConnectionNotificationSTSRole(dryRun, serviceId, vpcEndpointId, connectionNotificationArn, connectionEvents, clientToken, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates a connection notification for a specified VPC endpoint or VPC endpoint service. A connection notification notifies you of specific endpoint events. You must create an SNS topic to receive notifications. For more information, see  Create a Topic  in the  Amazon Simple Notification Service Developer Guide .   You can create a connection notification for interface endpoints only.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createVpcEndpointServiceConfigurationSTSRole(dryRun, acceptanceRequired, networkLoadBalancerArn, clientToken, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates a VPC endpoint service configuration to which service consumers (AWS accounts, IAM users, and IAM roles) can connect. Service consumers can create an interface VPC endpoint to connect to your service.   To create an endpoint service configuration, you must first create a Network Load Balancer for your service. For more information, see  VPC Endpoint Services  in the  Amazon Virtual Private Cloud User Guide .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createVpcPeeringConnectionSTSRole(dryRun, peerOwnerId, peerVpcId, vpcId, peerRegion, stsParams, roleName, callback)</td>
    <td style="padding:15px">Requests a VPC peering connection between two VPCs: a requester VPC that you own and an accepter VPC with which to create the connection. The accepter VPC can belong to another AWS account and can be in a different Region to the requester VPC. The requester VPC and accepter VPC cannot have overlapping CIDR blocks.     Limitations and rules apply to a VPC peering connection. For more information, see the  limitations  section in the  VPC Peering Guide .     The owner of the accepter VPC must acc...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createVpnConnectionSTSRole(customerGatewayId, type, vpnGatewayId, transitGatewayId, dryRun, optionsStaticRoutesOnly, optionsTunnelOptions, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates a VPN connection between an existing virtual private gateway and a VPN customer gateway. The only supported connection type is  ipsec.1 .   The response includes information that you need to give to your network administrator to configure your customer gateway.     We strongly recommend that you use HTTPS when calling this operation because the response contains sensitive cryptographic information for configuring your customer gateway.     If you decide to shut down your VPN connection ...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createVpnConnectionRouteSTSRole(destinationCidrBlock, vpnConnectionId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates a static route associated with a VPN connection between an existing virtual private gateway and a VPN customer gateway. The static route allows traffic to be routed from the virtual private gateway to the VPN customer gateway.   For more information, see  AWS Site-to-Site VPN  in the  AWS Site-to-Site VPN User Guide .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createVpnGatewaySTSRole(availabilityZone, type = 'ipsec.1', amazonSideAsn, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates a virtual private gateway. A virtual private gateway is the endpoint on the VPC side of your VPN connection. You can create a virtual private gateway before creating the VPC itself.   For more information, see  AWS Site-to-Site VPN  in the  AWS Site-to-Site VPN User Guide .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteClientVpnEndpointSTSRole(clientVpnEndpointId, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes the specified Client VPN endpoint. You must disassociate all target networks before you can delete a Client VPN endpoint.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteClientVpnRouteSTSRole(clientVpnEndpointId, targetVpcSubnetId, destinationCidrBlock, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes a route from a Client VPN endpoint. You can only delete routes that you manually added using the  CreateClientVpnRoute  action. You cannot delete routes that were automatically added when associating a subnet. To remove routes that have been automatically added, disassociate the target subnet from the Client VPN endpoint.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCustomerGatewaySTSRole(customerGatewayId, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes the specified customer gateway. You must delete the VPN connection before you can delete the customer gateway.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDhcpOptionsSTSRole(dhcpOptionsId, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes the specified set of DHCP options. You must disassociate the set of DHCP options before you can delete it. You can disassociate the set of DHCP options by associating either a new set of options or the default set of options with the VPC.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteEgressOnlyInternetGatewaySTSRole(dryRun, egressOnlyInternetGatewayId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes an egress-only internet gateway.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFleetsSTSRole(dryRun, fleetId, terminateInstances, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes the specified EC2 Fleet.   After you delete an EC2 Fleet, it launches no new instances. You must specify whether an EC2 Fleet should also terminate its instances. If you terminate the instances, the EC2 Fleet enters the  deleted_terminating  state. Otherwise, the EC2 Fleet enters the  deleted_running  state, and the instances continue to run until they are interrupted or you terminate them manually.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFlowLogsSTSRole(dryRun, flowLogId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes one or more flow logs.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFpgaImageSTSRole(dryRun, fpgaImageId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes the specified Amazon FPGA Image (AFI).</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteInternetGatewaySTSRole(dryRun, internetGatewayId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes the specified internet gateway. You must detach the internet gateway from the VPC before you can delete it.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteKeyPairSTSRole(keyName, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes the specified key pair, by removing the public key from Amazon EC2.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteLaunchTemplateSTSRole(dryRun, launchTemplateId, launchTemplateName, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes a launch template. Deleting a launch template deletes all of its versions.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteLaunchTemplateVersionsSTSRole(dryRun, launchTemplateId, launchTemplateName, launchTemplateVersion, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes one or more versions of a launch template. You cannot delete the default version of a launch template; you must first assign a different version as the default. If the default version is the only version for the launch template, you must delete the entire launch template using  DeleteLaunchTemplate .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNatGatewaySTSRole(natGatewayId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes the specified NAT gateway. Deleting a NAT gateway disassociates its Elastic IP address, but does not release the address from your account. Deleting a NAT gateway does not delete any NAT gateway routes in your route tables.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkAclSTSRole(dryRun, networkAclId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes the specified network ACL. You can't delete the ACL if it's associated with any subnets. You can't delete the default network ACL.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkAclEntrySTSRole(dryRun, egress, networkAclId, ruleNumber, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes the specified ingress or egress entry (rule) from the specified network ACL.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkInterfaceSTSRole(dryRun, networkInterfaceId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes the specified network interface. You must detach the network interface before you can delete it.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkInterfacePermissionSTSRole(networkInterfacePermissionId, force, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes a permission for a network interface. By default, you cannot delete the permission if the account for which you're removing the permission has attached the network interface to an instance. However, you can force delete the permission, regardless of any attachment.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePlacementGroupSTSRole(dryRun, groupName, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes the specified placement group. You must terminate all instances in the placement group before you can delete the placement group. For more information, see  Placement Groups  in the  Amazon Elastic Compute Cloud User Guide .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRouteSTSRole(destinationCidrBlock, destinationIpv6CidrBlock, dryRun, routeTableId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes the specified route from the specified route table.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRouteTableSTSRole(dryRun, routeTableId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes the specified route table. You must disassociate the route table from any subnets before you can delete it. You can't delete the main route table.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSecurityGroupSTSRole(groupId, groupName, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes a security group.   If you attempt to delete a security group that is associated with an instance, or is referenced by another security group, the operation fails with  InvalidGroup.InUse  in EC2-Classic or  DependencyViolation  in EC2-VPC.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSnapshotSTSRole(snapshotId, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes the specified snapshot.   When you make periodic snapshots of a volume, the snapshots are incremental, and only the blocks on the device that have changed since your last snapshot are saved in the new snapshot. When you delete a snapshot, only the data not needed for any other snapshot is removed. So regardless of which prior snapshots have been deleted, all active snapshots will have access to all the information needed to restore the volume.   You cannot delete a snapshot of the root ...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSpotDatafeedSubscriptionSTSRole(dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes the data feed for Spot Instances.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSubnetSTSRole(subnetId, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes the specified subnet. You must terminate all running instances in the subnet before you can delete the subnet.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTagsSTSRole(dryRun, resourceId, tag, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes the specified set of tags from the specified set of resources.   To list the current tags, use  DescribeTags . For more information about tags, see  Tagging Your Resources  in the  Amazon Elastic Compute Cloud User Guide .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTransitGatewaySTSRole(transitGatewayId, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes the specified transit gateway.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTransitGatewayRouteSTSRole(transitGatewayRouteTableId, destinationCidrBlock, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes the specified route from the specified transit gateway route table.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTransitGatewayRouteTableSTSRole(transitGatewayRouteTableId, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes the specified transit gateway route table. You must disassociate the route table from any transit gateway route tables before you can delete it.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTransitGatewayVpcAttachmentSTSRole(transitGatewayAttachmentId, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes the specified VPC attachment.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVolumeSTSRole(volumeId, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes the specified EBS volume. The volume must be in the  available  state (not attached to an instance).   The volume can remain in the  deleting  state for several minutes.   For more information, see  Deleting an Amazon EBS Volume  in the  Amazon Elastic Compute Cloud User Guide .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVpcSTSRole(vpcId, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes the specified VPC. You must detach or delete all gateways and resources that are associated with the VPC before you can delete it. For example, you must terminate all instances running in the VPC, delete all security groups associated with the VPC (except the default one), delete all route tables associated with the VPC (except the default one), and so on.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVpcEndpointConnectionNotificationsSTSRole(dryRun, connectionNotificationId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes one or more VPC endpoint connection notifications.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVpcEndpointServiceConfigurationsSTSRole(dryRun, serviceId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes one or more VPC endpoint service configurations in your account. Before you delete the endpoint service configuration, you must reject any  Available  or  PendingAcceptance  interface endpoint connections that are attached to the service.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVpcEndpointsSTSRole(dryRun, vpcEndpointId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes one or more specified VPC endpoints. Deleting a gateway endpoint also deletes the endpoint routes in the route tables that were associated with the endpoint. Deleting an interface endpoint deletes the endpoint network interfaces.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVpcPeeringConnectionSTSRole(dryRun, vpcPeeringConnectionId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes a VPC peering connection. Either the owner of the requester VPC or the owner of the accepter VPC can delete the VPC peering connection if it's in the  active  state. The owner of the requester VPC can delete a VPC peering connection in the  pending-acceptance  state. You cannot delete a VPC peering connection that's in the  failed  state.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVpnConnectionSTSRole(vpnConnectionId, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes the specified VPN connection.   If you're deleting the VPC and its associated components, we recommend that you detach the virtual private gateway from the VPC and delete the VPC before deleting the VPN connection. If you believe that the tunnel credentials for your VPN connection have been compromised, you can delete the VPN connection and create a new one that has new keys, without needing to delete the VPC or virtual private gateway. If you create a new VPN connection, you must recon...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVpnConnectionRouteSTSRole(destinationCidrBlock, vpnConnectionId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes the specified static route associated with a VPN connection between an existing virtual private gateway and a VPN customer gateway. The static route allows traffic to be routed from the virtual private gateway to the VPN customer gateway.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVpnGatewaySTSRole(vpnGatewayId, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deletes the specified virtual private gateway. We recommend that before you delete a virtual private gateway, you detach it from the VPC and delete the VPN connection. Note that you don't need to delete the virtual private gateway if you plan to delete and recreate the VPN connection between your VPC and your network.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deprovisionByoipCidrSTSRole(cidr, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Releases the specified address range that you provisioned for use with your AWS resources through bring your own IP addresses (BYOIP) and deletes the corresponding address pool.   Before you can release an address range, you must stop advertising it using  WithdrawByoipCidr  and you must not have any IP addresses allocated from its address range.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deregisterImageSTSRole(imageId, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Deregisters the specified AMI. After you deregister an AMI, it can't be used to launch new instances; however, it doesn't affect any instances that you've already launched from the AMI. You'll continue to incur usage costs for those instances until you terminate them.   When you deregister an Amazon EBS-backed AMI, it doesn't affect the snapshot that was created for the root volume of the instance during the AMI creation process. When you deregister an instance store-backed AMI, it doesn't affe...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeAccountAttributesSTSRole(attributeName, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes attributes of your AWS account. The following are the supported account attributes:         supported-platforms : Indicates whether your account can launch instances into EC2-Classic and EC2-VPC, or only into EC2-VPC.         default-vpc : The ID of the default VPC for your account, or  none .         max-instances : The maximum number of On-Demand Instances that you can run.         vpc-max-security-groups-per-interface : The maximum number of security groups that you can assign to a...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeAddressesSTSRole(filter, publicIp, allocationId, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes the specified Elastic IP addresses or all of your Elastic IP addresses.   An Elastic IP address is for use in either the EC2-Classic platform or in a VPC. For more information, see  Elastic IP Addresses  in the  Amazon Elastic Compute Cloud User Guide .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeAggregateIdFormatSTSRole(dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes the longer ID format settings for all resource types in a specific Region. This request is useful for performing a quick audit to determine whether a specific Region is fully opted in for longer IDs (17-character IDs).   This request only returns information about resource types that support longer IDs.   The following resource types support longer IDs:  bundle  |  conversion-task  |  customer-gateway  |  dhcp-options  |  elastic-ip-allocation  |  elastic-ip-association  |  export-tas...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeAvailabilityZonesSTSRole(filter, zoneName, zoneId, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes the Availability Zones that are available to you. The results include zones only for the Region you're currently using. If there is an event impacting an Availability Zone, you can use this request to view the state and any provided message for that Availability Zone.   For more information, see  Regions and Availability Zones  in the  Amazon Elastic Compute Cloud User Guide .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeBundleTasksSTSRole(bundleId, filter, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes the specified bundle tasks or all of your bundle tasks.     Completed bundle tasks are listed for only a limited time. If your bundle task is no longer in the list, you can still register an AMI from it. Just use  RegisterImage  with the Amazon S3 bucket name and image manifest name you provided to the bundle task.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeByoipCidrsSTSRole(dryRun, maxResults, nextToken, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes the IP address ranges that were specified in calls to  ProvisionByoipCidr .   To describe the address pools that were created when you provisioned the address ranges, use  DescribePublicIpv4Pools .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeCapacityReservationsSTSRole(capacityReservationId, nextToken, maxResults, filter, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes one or more of your Capacity Reservations. The results describe only the Capacity Reservations in the AWS Region that you're currently using.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeClassicLinkInstancesSTSRole(filter, dryRun, instanceId, maxResults, nextToken, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes one or more of your linked EC2-Classic instances. This request only returns information about EC2-Classic instances linked to a VPC through ClassicLink. You cannot use this request to return information about other instances.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeClientVpnAuthorizationRulesSTSRole(clientVpnEndpointId, dryRun, nextToken, filter, maxResults, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes the authorization rules for a specified Client VPN endpoint.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeClientVpnConnectionsSTSRole(clientVpnEndpointId, filter, nextToken, maxResults, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes active client connections and connections that have been terminated within the last 60 minutes for the specified Client VPN endpoint.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeClientVpnEndpointsSTSRole(clientVpnEndpointId, maxResults, nextToken, filter, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes one or more Client VPN endpoints in the account.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeClientVpnRoutesSTSRole(clientVpnEndpointId, filter, maxResults, nextToken, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes the routes for the specified Client VPN endpoint.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeClientVpnTargetNetworksSTSRole(clientVpnEndpointId, associationIds, maxResults, nextToken, filter, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes the target networks associated with the specified Client VPN endpoint.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeConversionTasksSTSRole(conversionTaskId, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes the specified conversion tasks or all your conversion tasks. For more information, see the  VM Import/Export User Guide .   For information about the import manifest referenced by this API action, see  VM Import Manifest .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeCustomerGatewaysSTSRole(customerGatewayId, filter, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes one or more of your VPN customer gateways.   For more information, see  AWS Site-to-Site VPN  in the  AWS Site-to-Site VPN User Guide .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeDhcpOptionsSTSRole(dhcpOptionsId, filter, dryRun, nextToken, maxResults, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes one or more of your DHCP options sets.   For more information, see  DHCP Options Sets  in the  Amazon Virtual Private Cloud User Guide .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeEgressOnlyInternetGatewaysSTSRole(dryRun, egressOnlyInternetGatewayId, maxResults, nextToken, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes one or more of your egress-only internet gateways.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeElasticGpusSTSRole(elasticGpuId, dryRun, filter, maxResults, nextToken, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes the Elastic Graphics accelerator associated with your instances. For more information about Elastic Graphics, see  Amazon Elastic Graphics .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeExportTasksSTSRole(exportTaskId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes the specified export tasks or all your export tasks.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeFleetHistorySTSRole(dryRun, eventType = 'instance-change', maxResults, nextToken, fleetId, startTime, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes the events for the specified EC2 Fleet during the specified time.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeFleetInstancesSTSRole(dryRun, maxResults, nextToken, fleetId, filter, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes the running instances for the specified EC2 Fleet.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeFleetsSTSRole(dryRun, maxResults, nextToken, fleetId, filter, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes the specified EC2 Fleets or all your EC2 Fleets.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeFlowLogsSTSRole(dryRun, filter, flowLogId, maxResults, nextToken, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes one or more flow logs. To view the information in your flow logs (the log streams for the network interfaces), you must use the CloudWatch Logs console or the CloudWatch Logs API.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeFpgaImageAttributeSTSRole(dryRun, fpgaImageId, attribute = 'description', stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes the specified attribute of the specified Amazon FPGA Image (AFI).</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeFpgaImagesSTSRole(dryRun, fpgaImageId, owner, filter, nextToken, maxResults, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes the Amazon FPGA Images (AFIs) available to you. These include public AFIs, private AFIs that you own, and AFIs owned by other AWS accounts for which you have load permissions.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeHostReservationOfferingsSTSRole(filter, maxDuration, maxResults, minDuration, nextToken, offeringId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes the Dedicated Host reservations that are available to purchase.   The results describe all the Dedicated Host reservation offerings, including offerings that may not match the instance family and Region of your Dedicated Hosts. When purchasing an offering, ensure that the instance family and Region of the offering matches that of the Dedicated Hosts with which it is to be associated. For more information about supported instance types, see  Dedicated Hosts Overview  in the  Amazon Ela...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeHostReservationsSTSRole(filter, hostReservationIdSet, maxResults, nextToken, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes reservations that are associated with Dedicated Hosts in your account.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeHostsSTSRole(filter, hostId, maxResults, nextToken, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes the specified Dedicated Hosts or all your Dedicated Hosts.   The results describe only the Dedicated Hosts in the Region you're currently using. All listed instances consume capacity on your Dedicated Host. Dedicated Hosts that have recently been released are listed with the state  released .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeIamInstanceProfileAssociationsSTSRole(associationId, filter, maxResults, nextToken, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes your IAM instance profile associations.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeIdFormatSTSRole(resource, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes the ID format settings for your resources on a per-Region basis, for example, to view which resource types are enabled for longer IDs. This request only returns information about resource types whose ID formats can be modified; it does not return information about other resource types.   The following resource types support longer IDs:  bundle  |  conversion-task  |  customer-gateway  |  dhcp-options  |  elastic-ip-allocation  |  elastic-ip-association  |  export-task  |  flow-log  | ...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeIdentityIdFormatSTSRole(principalArn, resource, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes the ID format settings for resources for the specified IAM user, IAM role, or root user. For example, you can view the resource types that are enabled for longer IDs. This request only returns information about resource types whose ID formats can be modified; it does not return information about other resource types. For more information, see  Resource IDs  in the  Amazon Elastic Compute Cloud User Guide .    The following resource types support longer IDs:  bundle  |  conversion-task...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeImageAttributeSTSRole(attribute = 'description', imageId, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes the specified attribute of the specified AMI. You can specify only one attribute at a time.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeImagesSTSRoleAdditionalOptions(executableBy, filter, imageId, owner, dryRun, stsParams, roleName, includeDeprecated, includeDisabled, maxResults, nextToken, callback)</td>
    <td style="padding:15px">Describes the specified images (AMIs, AKIs, and ARIs) available to you or all of the images available to you.   The images available to you include public images, private images that you own, and private images owned by other AWS accounts for which you have explicit launch permissions.   Recently deregistered images appear in the returned results for a short interval and then return empty results. After all instances that reference a deregistered AMI are terminated, specifying the ID of the ima...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeImportImageTasksSTSRole(dryRun, filters, importTaskId, maxResults, nextToken, stsParams, roleName, callback)</td>
    <td style="padding:15px">Displays details about an import virtual machine or import snapshot tasks that are already created.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeImportSnapshotTasksSTSRole(dryRun, filters, importTaskId, maxResults, nextToken, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes your import snapshot tasks.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeInstanceAttributeSTSRole(attribute = 'instanceType', dryRun, instanceId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes the specified attribute of the specified instance. You can specify only one attribute at a time. Valid attribute values are:  instanceType  |  kernel  |  ramdisk  |  userData  |  disableApiTermination  |  instanceInitiatedShutdownBehavior  |  rootDeviceName  |  blockDeviceMapping  |  productCodes  |  sourceDestCheck  |  groupSet  |  ebsOptimized  |  sriovNetSupport</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeInstanceCreditSpecificationsSTSRole(dryRun, filter, instanceId, maxResults, nextToken, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes the credit option for CPU usage of the specified T2 or T3 instances. The credit options are  standard  and  unlimited .   If you do not specify an instance ID, Amazon EC2 returns T2 and T3 instances with the  unlimited  credit option, as well as instances that were previously configured as T2 or T3 with the  unlimited  credit option. For example, if you resize a T2 instance, while it is configured as  unlimited , to an M4 instance, Amazon EC2 returns the M4 instance.   If you specify ...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeInstanceStatusSTSRole(filter, instanceId, maxResults, nextToken, dryRun, includeAllInstances, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes the status of the specified instances or all of your instances. By default, only running instances are described, unless you specifically indicate to return the status of all instances.   Instance status includes the following components:         Status checks  - Amazon EC2 performs status checks on running EC2 instances to identify hardware and software issues. For more information, see  Status Checks for Your Instances  and  Troubleshooting Instances with Failed Status Checks  in th...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeInstancesSTSRole(filter, instanceId, dryRun, maxResults, nextToken, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes the specified instances or all of your instances.   If you specify one or more instance IDs, Amazon EC2 returns information for those instances. If you do not specify instance IDs, Amazon EC2 returns information for all relevant instances. If you specify an instance ID that is not valid, an error is returned. If you specify an instance that you do not own, it is not included in the returned results.   Recently terminated instances might appear in the returned results. This interval is...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeInternetGatewaysSTSRole(filter, dryRun, internetGatewayId, nextToken, maxResults, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes one or more of your internet gateways.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeKeyPairsSTSRole(filter, keyName, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes the specified key pairs or all of your key pairs.   For more information about key pairs, see  Key Pairs  in the  Amazon Elastic Compute Cloud User Guide .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeLaunchTemplateVersionsSTSRole(dryRun, launchTemplateId, launchTemplateName, launchTemplateVersion, minVersion, maxVersion, nextToken, maxResults, filter, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes one or more versions of a specified launch template. You can describe all versions, individual versions, or a range of versions.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeLaunchTemplatesSTSRole(dryRun, launchTemplateId, launchTemplateName, filter, nextToken, maxResults, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes one or more launch templates.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeMovingAddressesSTSRole(filter, dryRun, maxResults, nextToken, publicIp, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes your Elastic IP addresses that are being moved to the EC2-VPC platform, or that are being restored to the EC2-Classic platform. This request does not return information about any other Elastic IP addresses in your account.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeNatGatewaysSTSRole(filter, maxResults, natGatewayId, nextToken, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes one or more of your NAT gateways.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeNetworkAclsSTSRole(filter, dryRun, networkAclId, nextToken, maxResults, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes one or more of your network ACLs.   For more information, see  Network ACLs  in the  Amazon Virtual Private Cloud User Guide .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeNetworkInterfaceAttributeSTSRole(attribute = 'description', dryRun, networkInterfaceId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes a network interface attribute. You can specify only one attribute at a time.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeNetworkInterfacePermissionsSTSRole(networkInterfacePermissionId, filter, nextToken, maxResults, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes the permissions for your network interfaces.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeNetworkInterfacesSTSRole(filter, dryRun, networkInterfaceId, nextToken, maxResults, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes one or more of your network interfaces.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describePlacementGroupsSTSRole(filter, dryRun, groupName, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes the specified placement groups or all of your placement groups. For more information, see  Placement Groups  in the  Amazon Elastic Compute Cloud User Guide .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describePrefixListsSTSRole(dryRun, filter, maxResults, nextToken, prefixListId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes available AWS services in a prefix list format, which includes the prefix list name and prefix list ID of the service and the IP address range for the service. A prefix list ID is required for creating an outbound security group rule that allows traffic from a VPC to access an AWS service through a gateway VPC endpoint. Currently, the services that support this action are Amazon S3 and Amazon DynamoDB.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describePrincipalIdFormatSTSRole(dryRun, resource, maxResults, nextToken, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes the ID format settings for the root user and all IAM roles and IAM users that have explicitly specified a longer ID (17-character ID) preference.    By default, all IAM roles and IAM users default to the same ID settings as the root user, unless they explicitly override the settings. This request is useful for identifying those IAM users and IAM roles that have overridden the default ID settings.   The following resource types support longer IDs:  bundle  |  conversion-task  |  custom...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describePublicIpv4PoolsSTSRole(poolId, nextToken, maxResults, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes the specified IPv4 address pools.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeRegionsSTSRole(filter, regionName, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes the Regions that are currently available to you. The API returns a list of all the Regions, including Regions that are disabled for your account. For information about enabling Regions for your account, see  Enabling and Disabling Regions  in the  AWS Billing and Cost Management User Guide .   For a list of the Regions supported by Amazon EC2, see   Regions and Endpoints .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeReservedInstancesSTSRole(filter, offeringClass = 'standard', reservedInstancesId, dryRun, offeringType = 'Heavy Utilization', stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes one or more of the Reserved Instances that you purchased.   For more information about Reserved Instances, see  Reserved Instances  in the  Amazon Elastic Compute Cloud User Guide .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeReservedInstancesListingsSTSRole(filter, reservedInstancesId, reservedInstancesListingId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes your account's Reserved Instance listings in the Reserved Instance Marketplace.   The Reserved Instance Marketplace matches sellers who want to resell Reserved Instance capacity that they no longer need with buyers who want to purchase additional capacity. Reserved Instances bought and sold through the Reserved Instance Marketplace work like any other Reserved Instances.   As a seller, you choose to list some or all of your Reserved Instances, and you specify the upfront price to rece...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeReservedInstancesModificationsSTSRole(filter, reservedInstancesModificationId, nextToken, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes the modifications made to your Reserved Instances. If no parameter is specified, information about all your Reserved Instances modification requests is returned. If a modification ID is specified, only information about the specific modification is returned.   For more information, see  Modifying Reserved Instances  in the Amazon Elastic Compute Cloud User Guide.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeReservedInstancesOfferingsSTSRole(availabilityZone, filter, includeMarketplace, instanceType = 't1.micro', maxDuration, maxInstanceCount, minDuration, offeringClass = 'standard', productDescription = 'Linux/UNIX', reservedInstancesOfferingId, dryRun, instanceTenancy = 'default', maxResults, nextToken, offeringType = 'Heavy Utilization', stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes Reserved Instance offerings that are available for purchase. With Reserved Instances, you purchase the right to launch instances for a period of time. During that time period, you do not receive insufficient capacity errors, and you pay a lower usage rate than the rate charged for On-Demand instances for the actual time used.   If you have listed your own Reserved Instances for sale in the Reserved Instance Marketplace, they will be excluded from these results. This is to ensure that ...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeRouteTablesSTSRole(filter, dryRun, routeTableId, nextToken, maxResults, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes one or more of your route tables.   Each subnet in your VPC must be associated with a route table. If a subnet is not explicitly associated with any route table, it is implicitly associated with the main route table. This command does not return the subnet ID for implicit associations.   For more information, see  Route Tables  in the  Amazon Virtual Private Cloud User Guide .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeScheduledInstanceAvailabilitySTSRole(dryRun, filter, firstSlotStartTimeRangeEarliestTime, firstSlotStartTimeRangeLatestTime, maxResults, maxSlotDurationInHours, minSlotDurationInHours, nextToken, recurrenceFrequency, recurrenceInterval, recurrenceOccurrenceDays, recurrenceOccurrenceRelativeToEnd, recurrenceOccurrenceUnit, stsParams, roleName, callback)</td>
    <td style="padding:15px">Finds available schedules that meet the specified criteria.   You can search for an available schedule no more than 3 months in advance. You must meet the minimum required duration of 1,200 hours per year. For example, the minimum daily schedule is 4 hours, the minimum weekly schedule is 24 hours, and the minimum monthly schedule is 100 hours.   After you find a schedule that meets your needs, call  PurchaseScheduledInstances  to purchase Scheduled Instances with that schedule.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeScheduledInstancesSTSRole(dryRun, filter, maxResults, nextToken, scheduledInstanceId, slotStartTimeRangeEarliestTime, slotStartTimeRangeLatestTime, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes the specified Scheduled Instances or all your Scheduled Instances.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeSecurityGroupReferencesSTSRole(dryRun, groupId, stsParams, roleName, callback)</td>
    <td style="padding:15px">[VPC only] Describes the VPCs on the other side of a VPC peering connection that are referencing the security groups you've specified in this request.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeSecurityGroupsSTSRole(filter, groupId, groupName, dryRun, nextToken, maxResults, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes the specified security groups or all of your security groups.   A security group is for use with instances either in the EC2-Classic platform or in a specific VPC. For more information, see  Amazon EC2 Security Groups  in the  Amazon Elastic Compute Cloud User Guide  and  Security Groups for Your VPC  in the  Amazon Virtual Private Cloud User Guide .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeSnapshotAttributeSTSRole(attribute = 'productCodes', snapshotId, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes the specified attribute of the specified snapshot. You can specify only one attribute at a time.   For more information about EBS snapshots, see  Amazon EBS Snapshots  in the  Amazon Elastic Compute Cloud User Guide .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeSnapshotsSTSRole(filter, maxResults, nextToken, owner, restorableBy, snapshotId, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes the specified EBS snapshots available to you or all of the EBS snapshots available to you.   The snapshots available to you include public snapshots, private snapshots that you own, and private snapshots owned by other AWS accounts for which you have explicit create volume permissions.   The create volume permissions fall into the following categories:         public : The owner of the snapshot granted create volume permissions for the snapshot to the  all  group. All AWS accounts hav...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeSpotDatafeedSubscriptionSTSRole(dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes the data feed for Spot Instances. For more information, see  Spot Instance Data Feed  in the  Amazon EC2 User Guide for Linux Instances .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeSpotFleetInstancesSTSRole(dryRun, maxResults, nextToken, spotFleetRequestId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes the running instances for the specified Spot Fleet.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeSpotFleetRequestHistorySTSRole(dryRun, eventType = 'instanceChange', maxResults, nextToken, spotFleetRequestId, startTime, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes the events for the specified Spot Fleet request during the specified time.   Spot Fleet events are delayed by up to 30 seconds before they can be described. This ensures that you can query by the last evaluated time and not miss a recorded event. Spot Fleet events are available for 48 hours.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeSpotFleetRequestsSTSRole(dryRun, maxResults, nextToken, spotFleetRequestId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes your Spot Fleet requests.   Spot Fleet requests are deleted 48 hours after they are canceled and their instances are terminated.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeSpotInstanceRequestsSTSRole(filter, dryRun, spotInstanceRequestId, nextToken, maxResults, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes the specified Spot Instance requests.   You can use  DescribeSpotInstanceRequests  to find a running Spot Instance by examining the response. If the status of the Spot Instance is  fulfilled , the instance ID appears in the response and contains the identifier of the instance. Alternatively, you can use  DescribeInstances  with a filter to look for instances where the instance lifecycle is  spot .   We recommend that you set  MaxResults  to a value between 5 and 1000 to limit the numb...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeSpotPriceHistorySTSRole(filter, availabilityZone, dryRun, endTime, instanceType, maxResults, nextToken, productDescription, startTime, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes the Spot price history. For more information, see  Spot Instance Pricing History  in the  Amazon EC2 User Guide for Linux Instances .   When you specify a start and end time, this operation returns the prices of the instance types within the time range that you specified and the time when the price changed. The price is valid within the time period that you specified; the response merely indicates the last time that the price changed.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeStaleSecurityGroupsSTSRole(dryRun, maxResults, nextToken, vpcId, stsParams, roleName, callback)</td>
    <td style="padding:15px">[VPC only] Describes the stale security group rules for security groups in a specified VPC. Rules are stale when they reference a deleted security group in a peer VPC, or a security group in a peer VPC for which the VPC peering connection has been deleted.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeSubnetsSTSRole(filter, subnetId, dryRun, nextToken, maxResults, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes one or more of your subnets.   For more information, see  Your VPC and Subnets  in the  Amazon Virtual Private Cloud User Guide .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeTagsSTSRole(dryRun, filter, maxResults, nextToken, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes the specified tags for your EC2 resources.   For more information about tags, see  Tagging Your Resources  in the  Amazon Elastic Compute Cloud User Guide .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeTransitGatewayAttachmentsSTSRole(transitGatewayAttachmentIds, filter, maxResults, nextToken, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes one or more attachments between resources and transit gateways. By default, all attachments are described. Alternatively, you can filter the results by attachment ID, attachment state, resource ID, or resource owner.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeTransitGatewayRouteTablesSTSRole(transitGatewayRouteTableIds, filter, maxResults, nextToken, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes one or more transit gateway route tables. By default, all transit gateway route tables are described. Alternatively, you can filter the results.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeTransitGatewayVpcAttachmentsSTSRole(transitGatewayAttachmentIds, filter, maxResults, nextToken, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes one or more VPC attachments. By default, all VPC attachments are described. Alternatively, you can filter the results.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeTransitGatewaysSTSRole(transitGatewayIds, filter, maxResults, nextToken, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes one or more transit gateways. By default, all transit gateways are described. Alternatively, you can filter the results.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeVolumeAttributeSTSRole(attribute = 'autoEnableIO', volumeId, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes the specified attribute of the specified volume. You can specify only one attribute at a time.   For more information about EBS volumes, see  Amazon EBS Volumes  in the  Amazon Elastic Compute Cloud User Guide .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeVolumeStatusSTSRole(filter, maxResults, nextToken, volumeId, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes the status of the specified volumes. Volume status provides the result of the checks performed on your volumes to determine events that can impair the performance of your volumes. The performance of a volume can be affected if an issue occurs on the volume's underlying host. If the volume's underlying host experiences a power outage or system issue, after the system is restored, there could be data inconsistencies on the volume. Volume events notify you if this occurs. Volume actions ...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeVolumesSTSRole(filter, volumeId, dryRun, maxResults, nextToken, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes the specified EBS volumes or all of your EBS volumes.   If you are describing a long list of volumes, you can paginate the output to make the list more manageable. The  MaxResults  parameter sets the maximum number of results returned in a single page. If the list of results exceeds your  MaxResults  value, then that number of results is returned along with a  NextToken  value that can be passed to a subsequent  DescribeVolumes  request to retrieve the remaining results.   For more in...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeVolumesModificationsSTSRole(dryRun, volumeId, filter, nextToken, maxResults, stsParams, roleName, callback)</td>
    <td style="padding:15px">Reports the current modification status of EBS volumes.   Current-generation EBS volumes support modification of attributes including type, size, and (for  io1  volumes) IOPS provisioning while either attached to or detached from an instance. Following an action from the API or the console to modify a volume, the status of the modification may be  modifying ,  optimizing ,  completed , or  failed . If a volume has never been modified, then certain elements of the returned  VolumeModification  o...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeVpcAttributeSTSRole(attribute = 'enableDnsSupport', vpcId, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes the specified attribute of the specified VPC. You can specify only one attribute at a time.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeVpcClassicLinkSTSRole(filter, dryRun, vpcId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes the ClassicLink status of one or more VPCs.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeVpcClassicLinkDnsSupportSTSRole(maxResults, nextToken, vpcIds, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes the ClassicLink DNS support status of one or more VPCs. If enabled, the DNS hostname of a linked EC2-Classic instance resolves to its private IP address when addressed from an instance in the VPC to which it's linked. Similarly, the DNS hostname of an instance in a VPC resolves to its private IP address when addressed from a linked EC2-Classic instance. For more information, see  ClassicLink  in the  Amazon Elastic Compute Cloud User Guide .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeVpcEndpointConnectionNotificationsSTSRole(dryRun, connectionNotificationId, filter, maxResults, nextToken, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes the connection notifications for VPC endpoints and VPC endpoint services.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeVpcEndpointConnectionsSTSRole(dryRun, filter, maxResults, nextToken, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes the VPC endpoint connections to your VPC endpoint services, including any endpoints that are pending your acceptance.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeVpcEndpointServiceConfigurationsSTSRole(dryRun, serviceId, filter, maxResults, nextToken, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes the VPC endpoint service configurations in your account (your services).</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeVpcEndpointServicePermissionsSTSRole(dryRun, serviceId, filter, maxResults, nextToken, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes the principals (service consumers) that are permitted to discover your VPC endpoint service.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeVpcEndpointServicesSTSRole(dryRun, serviceName, filter, maxResults, nextToken, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes available services to which you can create a VPC endpoint.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeVpcEndpointsSTSRole(dryRun, vpcEndpointId, filter, maxResults, nextToken, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes one or more of your VPC endpoints.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeVpcPeeringConnectionsSTSRole(filter, dryRun, vpcPeeringConnectionId, nextToken, maxResults, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes one or more of your VPC peering connections.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeVpcsSTSRole(filter, vpcId, dryRun, nextToken, maxResults, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes one or more of your VPCs.</td>
    <td style="padding:15px">{base_path}/{version}/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeVpnConnectionsSTSRole(filter, vpnConnectionId, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes one or more of your VPN connections.   For more information, see  AWS Site-to-Site VPN  in the  AWS Site-to-Site VPN User Guide .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">describeVpnGatewaysSTSRole(filter, vpnGatewayId, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Describes one or more of your virtual private gateways.   For more information, see  AWS Site-to-Site VPN  in the  AWS Site-to-Site VPN User Guide .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">detachClassicLinkVpcSTSRole(dryRun, instanceId, vpcId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Unlinks (detaches) a linked EC2-Classic instance from a VPC. After the instance has been unlinked, the VPC security groups are no longer associated with it. An instance is automatically unlinked from a VPC when it's stopped.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">detachInternetGatewaySTSRole(dryRun, internetGatewayId, vpcId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Detaches an internet gateway from a VPC, disabling connectivity between the internet and the VPC. The VPC must not contain any running instances with Elastic IP addresses or public IPv4 addresses.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">detachNetworkInterfaceSTSRole(attachmentId, dryRun, force, stsParams, roleName, callback)</td>
    <td style="padding:15px">Detaches a network interface from an instance.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">detachVolumeSTSRole(device, force, instanceId, volumeId, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Detaches an EBS volume from an instance. Make sure to unmount any file systems on the device within your operating system before detaching the volume. Failure to do so can result in the volume becoming stuck in the  busy  state while detaching. If this happens, detachment can be delayed indefinitely until you unmount the volume, force detachment, reboot the instance, or all three. If an EBS volume is the root device of an instance, it can't be detached while the instance is running. To detach t...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">detachVpnGatewaySTSRole(vpcId, vpnGatewayId, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Detaches a virtual private gateway from a VPC. You do this if you're planning to turn off the VPC and not use it anymore. You can confirm a virtual private gateway has been completely detached from a VPC by describing the virtual private gateway (any attachments to the virtual private gateway are also described).   You must wait for the attachment's state to switch to  detached  before you can delete the VPC or attach a different VPC to the virtual private gateway.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableTransitGatewayRouteTablePropagationSTSRole(transitGatewayRouteTableId, transitGatewayAttachmentId, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Disables the specified resource attachment from propagating routes to the specified propagation route table.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableVgwRoutePropagationSTSRole(gatewayId, routeTableId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Disables a virtual private gateway (VGW) from propagating routes to a specified route table of a VPC.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableVpcClassicLinkSTSRole(dryRun, vpcId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Disables ClassicLink for a VPC. You cannot disable ClassicLink for a VPC that has EC2-Classic instances linked to it.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableVpcClassicLinkDnsSupportSTSRole(vpcId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Disables ClassicLink DNS support for a VPC. If disabled, DNS hostnames resolve to public IP addresses when addressed between a linked EC2-Classic instance and instances in the VPC to which it's linked. For more information, see  ClassicLink  in the  Amazon Elastic Compute Cloud User Guide .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disassociateAddressSTSRole(associationId, publicIp, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Disassociates an Elastic IP address from the instance or network interface it's associated with.   An Elastic IP address is for use in either the EC2-Classic platform or in a VPC. For more information, see  Elastic IP Addresses  in the  Amazon Elastic Compute Cloud User Guide .   This is an idempotent operation. If you perform the operation more than once, Amazon EC2 doesn't return an error.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disassociateClientVpnTargetNetworkSTSRole(clientVpnEndpointId, associationId, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Disassociates a target network from the specified Client VPN endpoint. When you disassociate the last target network from a Client VPN, the following happens:       The route that was automatically added for the VPC is deleted       All active client connections are terminated       New client connections are disallowed       The Client VPN endpoint's status changes to  pending-associate</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disassociateIamInstanceProfileSTSRole(associationId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Disassociates an IAM instance profile from a running or stopped instance.   Use  DescribeIamInstanceProfileAssociations  to get the association ID.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disassociateRouteTableSTSRole(associationId, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Disassociates a subnet from a route table.   After you perform this action, the subnet no longer uses the routes in the route table. Instead, it uses the routes in the VPC's main route table. For more information about route tables, see  Route Tables  in the  Amazon Virtual Private Cloud User Guide .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disassociateSubnetCidrBlockSTSRole(associationId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Disassociates a CIDR block from a subnet. Currently, you can disassociate an IPv6 CIDR block only. You must detach or delete all gateways and resources that are associated with the CIDR block before you can disassociate it.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disassociateTransitGatewayRouteTableSTSRole(transitGatewayRouteTableId, transitGatewayAttachmentId, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Disassociates a resource attachment from a transit gateway route table.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disassociateVpcCidrBlockSTSRole(associationId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Disassociates a CIDR block from a VPC. To disassociate the CIDR block, you must specify its association ID. You can get the association ID by using  DescribeVpcs . You must detach or delete all gateways and resources that are associated with the CIDR block before you can disassociate it.    You cannot disassociate the CIDR block with which you originally created the VPC (the primary CIDR block).</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableTransitGatewayRouteTablePropagationSTSRole(transitGatewayRouteTableId, transitGatewayAttachmentId, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Enables the specified attachment to propagate routes to the specified propagation route table.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableVgwRoutePropagationSTSRole(gatewayId, routeTableId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Enables a virtual private gateway (VGW) to propagate routes to the specified route table of a VPC.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableVolumeIOSTSRole(dryRun, volumeId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Enables I/O operations for a volume that had I/O operations disabled because the data on the volume was potentially inconsistent.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableVpcClassicLinkSTSRole(dryRun, vpcId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Enables a VPC for ClassicLink. You can then link EC2-Classic instances to your ClassicLink-enabled VPC to allow communication over private IP addresses. You cannot enable your VPC for ClassicLink if any of your VPC route tables have existing routes for address ranges within the  10.0.0.0/8  IP address range, excluding local routes for VPCs in the  10.0.0.0/16  and  10.1.0.0/16  IP address ranges. For more information, see  ClassicLink  in the  Amazon Elastic Compute Cloud User Guide .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableVpcClassicLinkDnsSupportSTSRole(vpcId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Enables a VPC to support DNS hostname resolution for ClassicLink. If enabled, the DNS hostname of a linked EC2-Classic instance resolves to its private IP address when addressed from an instance in the VPC to which it's linked. Similarly, the DNS hostname of an instance in a VPC resolves to its private IP address when addressed from a linked EC2-Classic instance. For more information, see  ClassicLink  in the  Amazon Elastic Compute Cloud User Guide .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">exportClientVpnClientCertificateRevocationListSTSRole(clientVpnEndpointId, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Downloads the client certificate revocation list for the specified Client VPN endpoint.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">exportClientVpnClientConfigurationSTSRole(clientVpnEndpointId, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Downloads the contents of the Client VPN endpoint configuration file for the specified Client VPN endpoint. The Client VPN endpoint configuration file includes the Client VPN endpoint and certificate information clients need to establish a connection with the Client VPN endpoint.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">exportTransitGatewayRoutesSTSRole(transitGatewayRouteTableId, filter, s3Bucket, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Exports routes from the specified transit gateway route table to the specified S3 bucket. By default, all routes are exported. Alternatively, you can filter by CIDR range.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConsoleOutputSTSRole(instanceId, dryRun, latest, stsParams, roleName, callback)</td>
    <td style="padding:15px">Gets the console output for the specified instance. For Linux instances, the instance console output displays the exact console output that would normally be displayed on a physical monitor attached to a computer. For Windows instances, the instance console output includes the last three system event log errors.   By default, the console output returns buffered information that was posted shortly after an instance transition state (start, stop, reboot, or terminate). This information is availab...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getConsoleScreenshotSTSRole(dryRun, instanceId, wakeUp, stsParams, roleName, callback)</td>
    <td style="padding:15px">Retrieve a JPG-format screenshot of a running instance to help with troubleshooting.   The returned content is Base64-encoded.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHostReservationPurchasePreviewSTSRole(hostIdSet, offeringId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Preview a reservation purchase with configurations that match those of your Dedicated Host. You must have active Dedicated Hosts in your account before you purchase a reservation.   This is a preview of the  PurchaseHostReservation  action and does not result in the offering being purchased.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLaunchTemplateDataSTSRole(dryRun, instanceId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Retrieves the configuration data of the specified instance. You can use this data to create a launch template.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPasswordDataSTSRole(instanceId, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Retrieves the encrypted administrator password for a running Windows instance.   The Windows password is generated at boot by the  EC2Config  service or  EC2Launch  scripts (Windows Server 2016 and later). This usually only happens the first time an instance is launched. For more information, see  EC2Config  and  EC2Launch  in the Amazon Elastic Compute Cloud User Guide.   For the  EC2Config  service, the password is not generated for rebundled AMIs unless  Ec2SetPassword  is enabled before bun...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReservedInstancesExchangeQuoteSTSRole(dryRun, reservedInstanceId, targetConfiguration, stsParams, roleName, callback)</td>
    <td style="padding:15px">Returns a quote and exchange information for exchanging one or more specified Convertible Reserved Instances for a new Convertible Reserved Instance. If the exchange cannot be performed, the reason is returned in the response. Use  AcceptReservedInstancesExchangeQuote  to perform the exchange.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTransitGatewayAttachmentPropagationsSTSRole(transitGatewayAttachmentId, filter, maxResults, nextToken, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Lists the route tables to which the specified resource attachment propagates routes.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTransitGatewayRouteTableAssociationsSTSRole(transitGatewayRouteTableId, filter, maxResults, nextToken, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Gets information about the associations for the specified transit gateway route table.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTransitGatewayRouteTablePropagationsSTSRole(transitGatewayRouteTableId, filter, maxResults, nextToken, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Gets information about the route table propagations for the specified transit gateway route table.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importClientVpnClientCertificateRevocationListSTSRole(clientVpnEndpointId, certificateRevocationList, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Uploads a client certificate revocation list to the specified Client VPN endpoint. Uploading a client certificate revocation list overwrites the existing client certificate revocation list.   Uploading a client certificate revocation list resets existing client connections.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importImageSTSRole(architecture, clientDataComment, clientDataUploadEnd, clientDataUploadSize, clientDataUploadStart, clientToken, description, diskContainer, dryRun, encrypted, hypervisor, kmsKeyId, licenseType, platform, roleName, stsParams, callback)</td>
    <td style="padding:15px">Import single or multi-volume disk images or EBS snapshots into an Amazon Machine Image (AMI). For more information, see  Importing a VM as an Image Using VM Import/Export  in the  VM Import/Export User Guide .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importInstanceSTSRole(description, diskImage, dryRun, launchSpecificationAdditionalInfo, launchSpecificationArchitecture, launchSpecificationGroupIds, launchSpecificationGroupNames, launchSpecificationInstanceInitiatedShutdownBehavior, launchSpecificationInstanceType, launchSpecificationMonitoring, launchSpecificationPlacement, launchSpecificationPrivateIpAddress, launchSpecificationSubnetId, launchSpecificationUserData, platform = 'Windows', stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates an import instance task using metadata from the specified disk image.  ImportInstance  only supports single-volume VMs. To import multi-volume VMs, use  ImportImage . For more information, see  Importing a Virtual Machine Using the Amazon EC2 CLI .   For information about the import manifest referenced by this API action, see  VM Import Manifest .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importKeyPairSTSRole(dryRun, keyName, publicKeyMaterial, stsParams, roleName, callback)</td>
    <td style="padding:15px">Imports the public key from an RSA key pair that you created with a third-party tool. Compare this with  CreateKeyPair , in which AWS creates the key pair and gives the keys to you (AWS keeps a copy of the public key). With ImportKeyPair, you create the key pair and give AWS just the public key. The private key is never transferred between you and AWS.   For more information about key pairs, see  Key Pairs  in the  Amazon Elastic Compute Cloud User Guide .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importSnapshotSTSRole(clientDataComment, clientDataUploadEnd, clientDataUploadSize, clientDataUploadStart, clientToken, description, diskContainerDescription, diskContainerFormat, diskContainerUrl, diskContainerUserBucket, dryRun, encrypted, kmsKeyId, roleName, stsParams, callback)</td>
    <td style="padding:15px">Imports a disk into an EBS snapshot.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importVolumeSTSRole(availabilityZone, description, dryRun, imageBytes, imageFormat, imageImportManifestUrl, volumeSize, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates an import volume task using metadata from the specified disk image.For more information, see  Importing Disks to Amazon EBS .   For information about the import manifest referenced by this API action, see  VM Import Manifest .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyCapacityReservationSTSRole(capacityReservationId, instanceCount, endDate, endDateType = 'unlimited', dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Modifies a Capacity Reservation's capacity and the conditions under which it is to be released. You cannot change a Capacity Reservation's instance type, EBS optimization, instance store settings, platform, Availability Zone, or instance eligibility. If you need to modify any of these attributes, we recommend that you cancel the Capacity Reservation, and then create a new one with the required attributes.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyClientVpnEndpointSTSRole(clientVpnEndpointId, serverCertificateArn, connectionLogOptionsEnabled, connectionLogOptionsCloudwatchLogGroup, connectionLogOptionsCloudwatchLogStream, dnsServersCustomDnsServers, dnsServersEnabled, description, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Modifies the specified Client VPN endpoint. You can only modify an endpoint's server certificate information, client connection logging information, DNS server, and description. Modifying the DNS server resets existing client connections.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyFleetSTSRole(dryRun, excessCapacityTerminationPolicy = 'no-termination', fleetId, targetCapacitySpecificationTotalTargetCapacity, targetCapacitySpecificationOnDemandTargetCapacity, targetCapacitySpecificationSpotTargetCapacity, targetCapacitySpecificationDefaultTargetCapacityType, stsParams, roleName, callback)</td>
    <td style="padding:15px">Modifies the specified EC2 Fleet.   While the EC2 Fleet is being modified, it is in the  modifying  state.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyFpgaImageAttributeSTSRole(dryRun, fpgaImageId, attribute = 'description', operationType = 'add', userId, userGroup, productCode, loadPermissionAdd, loadPermissionRemove, description, name, stsParams, roleName, callback)</td>
    <td style="padding:15px">Modifies the specified attribute of the specified Amazon FPGA Image (AFI).</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyHostsSTSRole(autoPlacement = 'on', hostId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Modify the auto-placement setting of a Dedicated Host. When auto-placement is enabled, any instances that you launch with a tenancy of  host  but without a specific host ID are placed onto any available Dedicated Host in your account that has auto-placement enabled. When auto-placement is disabled, you need to provide a host ID to have the instance launch onto a specific host. If no host ID is provided, the instance is launched onto a suitable host with auto-placement enabled.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyIdFormatSTSRole(resource, useLongIds, stsParams, roleName, callback)</td>
    <td style="padding:15px">Modifies the ID format for the specified resource on a per-Region basis. You can specify that resources should receive longer IDs (17-character IDs) when they are created.   This request can only be used to modify longer ID settings for resource types that are within the opt-in period. Resources currently in their opt-in period include:  bundle  |  conversion-task  |  customer-gateway  |  dhcp-options  |  elastic-ip-allocation  |  elastic-ip-association  |  export-task  |  flow-log  |  image  |...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyIdentityIdFormatSTSRole(principalArn, resource, useLongIds, stsParams, roleName, callback)</td>
    <td style="padding:15px">Modifies the ID format of a resource for a specified IAM user, IAM role, or the root user for an account; or all IAM users, IAM roles, and the root user for an account. You can specify that resources should receive longer IDs (17-character IDs) when they are created.    This request can only be used to modify longer ID settings for resource types that are within the opt-in period. Resources currently in their opt-in period include:  bundle  |  conversion-task  |  customer-gateway  |  dhcp-optio...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyImageAttributeSTSRole(attribute, descriptionValue, imageId, launchPermissionAdd, launchPermissionRemove, operationType = 'add', productCode, userGroup, userId, value, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Modifies the specified attribute of the specified AMI. You can specify only one attribute at a time. You can use the  Attribute  parameter to specify the attribute or one of the following parameters:  Description ,  LaunchPermission , or  ProductCode .   AWS Marketplace product codes cannot be modified. Images with an AWS Marketplace product code cannot be made public.   To enable the SriovNetSupport enhanced networking attribute of an image, enable SriovNetSupport on an instance and create an ...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyInstanceAttributeSTSRole(sourceDestCheckValue, attribute = 'instanceType', blockDeviceMapping, disableApiTerminationValue, dryRun, ebsOptimizedValue, enaSupportValue, groupId, instanceId, instanceInitiatedShutdownBehaviorValue, instanceTypeValue, kernelValue, ramdiskValue, sriovNetSupportValue, userDataValue, value, stsParams, roleName, callback)</td>
    <td style="padding:15px">Modifies the specified attribute of the specified instance. You can specify only one attribute at a time.     Note:  Using this action to change the security groups associated with an elastic network interface (ENI) attached to an instance in a VPC can result in an error if the instance has more than one ENI. To change the security groups associated with an ENI attached to an instance that has multiple ENIs, we recommend that you use the  ModifyNetworkInterfaceAttribute  action.   To modify som...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyInstanceCapacityReservationAttributesSTSRole(instanceId, capacityReservationSpecificationCapacityReservationPreference, capacityReservationSpecificationCapacityReservationTarget, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Modifies the Capacity Reservation settings for a stopped instance. Use this action to configure an instance to target a specific Capacity Reservation, run in any  open  Capacity Reservation with matching attributes, or run On-Demand Instance capacity.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyInstanceCreditSpecificationSTSRole(dryRun, clientToken, instanceCreditSpecification, stsParams, roleName, callback)</td>
    <td style="padding:15px">Modifies the credit option for CPU usage on a running or stopped T2 or T3 instance. The credit options are  standard  and  unlimited .   For more information, see  Burstable Performance Instances  in the  Amazon Elastic Compute Cloud User Guide .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyInstanceEventStartTimeSTSRole(dryRun, instanceId, instanceEventId, notBefore, stsParams, roleName, callback)</td>
    <td style="padding:15px">Modifies the start time for a scheduled Amazon EC2 instance event.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyInstancePlacementSTSRole(affinity = 'default', groupName, hostId, instanceId, tenancy = 'dedicated', partitionNumber, stsParams, roleName, callback)</td>
    <td style="padding:15px">Modifies the placement attributes for a specified instance. You can do the following:       Modify the affinity between an instance and a  Dedicated Host . When affinity is set to  host  and the instance is not associated with a specific Dedicated Host, the next time the instance is launched, it is automatically associated with the host on which it lands. If the instance is restarted or rebooted, this relationship persists.       Change the Dedicated Host with which an instance is associated.  ...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyLaunchTemplateSTSRole(dryRun, clientToken, launchTemplateId, launchTemplateName, setDefaultVersion, stsParams, roleName, callback)</td>
    <td style="padding:15px">Modifies a launch template. You can specify which version of the launch template to set as the default version. When launching an instance, the default version applies when a launch template version is not specified.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyNetworkInterfaceAttributeSTSRole(attachmentAttachmentId, attachmentDeleteOnTermination, descriptionValue, dryRun, securityGroupId, networkInterfaceId, sourceDestCheckValue, stsParams, roleName, callback)</td>
    <td style="padding:15px">Modifies the specified network interface attribute. You can specify only one attribute at a time. You can use this action to attach and detach security groups from an existing EC2 instance.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyReservedInstancesSTSRole(reservedInstancesId, clientToken, reservedInstancesConfigurationSetItemType, stsParams, roleName, callback)</td>
    <td style="padding:15px">Modifies the Availability Zone, instance count, instance type, or network platform (EC2-Classic or EC2-VPC) of your Reserved Instances. The Reserved Instances to be modified must be identical, except for Availability Zone, network platform, and instance type.   For more information, see  Modifying Reserved Instances  in the Amazon Elastic Compute Cloud User Guide.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifySnapshotAttributeSTSRole(attribute = 'productCodes', createVolumePermissionAdd, createVolumePermissionRemove, userGroup, operationType = 'add', snapshotId, userId, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Adds or removes permission settings for the specified snapshot. You may add or remove specified AWS account IDs from a snapshot's list of create volume permissions, but you cannot do both in a single API call. If you need to both add and remove account IDs for a snapshot, you must use multiple API calls.   Encrypted snapshots and snapshots with AWS Marketplace product codes cannot be made public. Snapshots encrypted with your default CMK cannot be shared with other accounts.   For more informat...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifySpotFleetRequestSTSRole(excessCapacityTerminationPolicy = 'noTermination', spotFleetRequestId, targetCapacity, stsParams, roleName, callback)</td>
    <td style="padding:15px">Modifies the specified Spot Fleet request.   While the Spot Fleet request is being modified, it is in the  modifying  state.   To scale up your Spot Fleet, increase its target capacity. The Spot Fleet launches the additional Spot Instances according to the allocation strategy for the Spot Fleet request. If the allocation strategy is  lowestPrice , the Spot Fleet launches instances using the Spot pool with the lowest price. If the allocation strategy is  diversified , the Spot Fleet distributes ...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifySubnetAttributeSTSRole(assignIpv6AddressOnCreationValue, mapPublicIpOnLaunchValue, subnetId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Modifies a subnet attribute. You can only modify one attribute at a time.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyTransitGatewayVpcAttachmentSTSRole(transitGatewayAttachmentId, addSubnetIds, removeSubnetIds, optionsDnsSupport, optionsIpv6Support, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Modifies the specified VPC attachment.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyVolumeSTSRole(dryRun, volumeId, size, volumeType = 'standard', iops, stsParams, roleName, callback)</td>
    <td style="padding:15px">You can modify several parameters of an existing EBS volume, including volume size, volume type, and IOPS capacity. If your EBS volume is attached to a current-generation EC2 instance type, you may be able to apply these changes without stopping the instance or detaching the volume from it. For more information about modifying an EBS volume running Linux, see  Modifying the Size, IOPS, or Type of an EBS Volume on Linux . For more information about modifying an EBS volume running Windows, see  M...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyVolumeAttributeSTSRole(autoEnableIOValue, volumeId, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Modifies a volume attribute.   By default, all I/O operations for the volume are suspended when the data on the volume is determined to be potentially inconsistent, to prevent undetectable, latent data corruption. The I/O access to the volume can be resumed by first enabling I/O access and then checking the data consistency on your volume.   You can change the default behavior to resume I/O operations. We recommend that you change this only for boot volumes or for volumes that are stateless or ...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyVpcAttributeSTSRole(enableDnsHostnamesValue, enableDnsSupportValue, vpcId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Modifies the specified attribute of the specified VPC.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyVpcEndpointSTSRole(dryRun, vpcEndpointId, resetPolicy, policyDocument, addRouteTableId, removeRouteTableId, addSubnetId, removeSubnetId, addSecurityGroupId, removeSecurityGroupId, privateDnsEnabled, stsParams, roleName, callback)</td>
    <td style="padding:15px">Modifies attributes of a specified VPC endpoint. The attributes that you can modify depend on the type of VPC endpoint (interface or gateway). For more information, see  VPC Endpoints  in the  Amazon Virtual Private Cloud User Guide .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyVpcEndpointConnectionNotificationSTSRole(dryRun, connectionNotificationId, connectionNotificationArn, connectionEvents, stsParams, roleName, callback)</td>
    <td style="padding:15px">Modifies a connection notification for VPC endpoint or VPC endpoint service. You can change the SNS topic for the notification, or the events for which to be notified.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyVpcEndpointServiceConfigurationSTSRole(dryRun, serviceId, acceptanceRequired, addNetworkLoadBalancerArn, removeNetworkLoadBalancerArn, stsParams, roleName, callback)</td>
    <td style="padding:15px">Modifies the attributes of your VPC endpoint service configuration. You can change the Network Load Balancers for your service, and you can specify whether acceptance is required for requests to connect to your endpoint service through an interface VPC endpoint.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyVpcEndpointServicePermissionsSTSRole(dryRun, serviceId, addAllowedPrincipals, removeAllowedPrincipals, stsParams, roleName, callback)</td>
    <td style="padding:15px">Modifies the permissions for your  VPC endpoint service . You can add or remove permissions for service consumers (IAM users, IAM roles, and AWS accounts) to connect to your endpoint service.   If you grant permissions to all principals, the service is public. Any users who know the name of a public service can send a request to attach an endpoint. If the service does not require manual approval, attachments are automatically approved.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyVpcPeeringConnectionOptionsSTSRole(accepterPeeringConnectionOptionsAllowDnsResolutionFromRemoteVpc, accepterPeeringConnectionOptionsAllowEgressFromLocalClassicLinkToRemoteVpc, accepterPeeringConnectionOptionsAllowEgressFromLocalVpcToRemoteClassicLink, dryRun, requesterPeeringConnectionOptionsAllowDnsResolutionFromRemoteVpc, requesterPeeringConnectionOptionsAllowEgressFromLocalClassicLinkToRemoteVpc, requesterPeeringConnectionOptionsAllowEgressFromLocalVpcToRemoteClassicLink, vpcPeeringConnectionId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Modifies the VPC peering connection options on one side of a VPC peering connection. You can do the following:       Enable/disable communication over the peering connection between an EC2-Classic instance that's linked to your VPC (using ClassicLink) and instances in the peer VPC.       Enable/disable communication over the peering connection between instances in your VPC and an EC2-Classic instance that's linked to the peer VPC.       Enable/disable the ability to resolve public DNS hostnames...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyVpcTenancySTSRole(vpcId, instanceTenancy = 'default', dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Modifies the instance tenancy attribute of the specified VPC. You can change the instance tenancy attribute of a VPC to  default  only. You cannot change the instance tenancy attribute to  dedicated .   After you modify the tenancy of the VPC, any new instances that you launch into the VPC have a tenancy of  default , unless you specify otherwise during launch. The tenancy of any existing instances in the VPC is not affected.   For more information, see  Dedicated Instances  in the  Amazon Elas...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyVpnConnectionSTSRole(vpnConnectionId, transitGatewayId, vpnGatewayId, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Modifies the target gateway of a AWS Site-to-Site VPN connection. The following migration options are available:       An existing virtual private gateway to a new virtual private gateway       An existing virtual private gateway to a transit gateway       An existing transit gateway to a new transit gateway       An existing transit gateway to a virtual private gateway       Before you perform the migration to the new gateway, you must configure the new gateway. Use  CreateVpnGateway  to creat...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">monitorInstancesSTSRole(instanceId, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Enables detailed monitoring for a running instance. Otherwise, basic monitoring is enabled. For more information, see  Monitoring Your Instances and Volumes  in the  Amazon Elastic Compute Cloud User Guide .   To disable detailed monitoring, see .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">moveAddressToVpcSTSRole(dryRun, publicIp, stsParams, roleName, callback)</td>
    <td style="padding:15px">Moves an Elastic IP address from the EC2-Classic platform to the EC2-VPC platform. The Elastic IP address must be allocated to your account for more than 24 hours, and it must not be associated with an instance. After the Elastic IP address is moved, it is no longer available for use in the EC2-Classic platform, unless you move it back using the  RestoreAddressToClassic  request. You cannot move an Elastic IP address that was originally allocated for use in the EC2-VPC platform to the EC2-Classi...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">provisionByoipCidrSTSRole(cidr, cidrAuthorizationContextMessage, cidrAuthorizationContextSignature, description, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Provisions an address range for use with your AWS resources through bring your own IP addresses (BYOIP) and creates a corresponding address pool. After the address range is provisioned, it is ready to be advertised using  AdvertiseByoipCidr .   AWS verifies that you own the address range and are authorized to advertise it. You must ensure that the address range is registered to you and that you created an RPKI ROA to authorize Amazon ASNs 16509 and 14618 to advertise the address range. For more...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">purchaseHostReservationSTSRole(clientToken, currencyCode = 'USD', hostIdSet, limitPrice, offeringId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Purchase a reservation with configurations that match those of your Dedicated Host. You must have active Dedicated Hosts in your account before you purchase a reservation. This action results in the specified reservation being purchased and charged to your account.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">purchaseReservedInstancesOfferingSTSRole(instanceCount, reservedInstancesOfferingId, dryRun, limitPriceAmount, limitPriceCurrencyCode, stsParams, roleName, callback)</td>
    <td style="padding:15px">Purchases a Reserved Instance for use with your account. With Reserved Instances, you pay a lower hourly rate compared to On-Demand instance pricing.   Use  DescribeReservedInstancesOfferings  to get a list of Reserved Instance offerings that match your specifications. After you've purchased a Reserved Instance, you can check for your new Reserved Instance with  DescribeReservedInstances .   For more information, see  Reserved Instances  and  Reserved Instance Marketplace  in the  Amazon Elasti...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">purchaseScheduledInstancesSTSRole(clientToken, dryRun, purchaseRequest, stsParams, roleName, callback)</td>
    <td style="padding:15px">Purchases the Scheduled Instances with the specified schedule.   Scheduled Instances enable you to purchase Amazon EC2 compute capacity by the hour for a one-year term. Before you can purchase a Scheduled Instance, you must call  DescribeScheduledInstanceAvailability  to check for available schedules and obtain a purchase token. After you purchase a Scheduled Instance, you must call  RunScheduledInstances  during each scheduled time period.   After you purchase a Scheduled Instance, you can't c...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rebootInstancesSTSRole(instanceId, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Requests a reboot of the specified instances. This operation is asynchronous; it only queues a request to reboot the specified instances. The operation succeeds if the instances are valid and belong to you. Requests to reboot terminated instances are ignored.   If an instance does not cleanly shut down within four minutes, Amazon EC2 performs a hard reboot.   For more information about troubleshooting, see  Getting Console Output and Rebooting Instances  in the  Amazon Elastic Compute Cloud Use...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">registerImageSTSRole(imageLocation, architecture = 'i386', blockDeviceMapping, description, dryRun, enaSupport, kernelId, name, billingProduct, ramdiskId, rootDeviceName, sriovNetSupport, virtualizationType, stsParams, roleName, callback)</td>
    <td style="padding:15px">Registers an AMI. When you're creating an AMI, this is the final step you must complete before you can launch an instance from the AMI. For more information about creating AMIs, see  Creating Your Own AMIs  in the  Amazon Elastic Compute Cloud User Guide .     For Amazon EBS-backed instances,  CreateImage  creates and registers the AMI in a single request, so you don't have to register the AMI yourself.     You can also use  RegisterImage  to create an Amazon EBS-backed Linux AMI from a snapsho...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rejectTransitGatewayVpcAttachmentSTSRole(transitGatewayAttachmentId, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Rejects a request to attach a VPC to a transit gateway.   The VPC attachment must be in the  pendingAcceptance  state. Use  DescribeTransitGatewayVpcAttachments  to view your pending VPC attachment requests. Use  AcceptTransitGatewayVpcAttachment  to accept a VPC attachment request.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rejectVpcEndpointConnectionsSTSRole(dryRun, serviceId, vpcEndpointId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Rejects one or more VPC endpoint connection requests to your VPC endpoint service.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rejectVpcPeeringConnectionSTSRole(dryRun, vpcPeeringConnectionId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Rejects a VPC peering connection request. The VPC peering connection must be in the  pending-acceptance  state. Use the  DescribeVpcPeeringConnections  request to view your outstanding VPC peering connection requests. To delete an active VPC peering connection, or to delete a VPC peering connection request that you initiated, use  DeleteVpcPeeringConnection .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">releaseAddressSTSRole(allocationId, publicIp, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Releases the specified Elastic IP address.   [EC2-Classic, default VPC] Releasing an Elastic IP address automatically disassociates it from any instance that it's associated with. To disassociate an Elastic IP address without releasing it, use  DisassociateAddress .   [Nondefault VPC] You must use  DisassociateAddress  to disassociate the Elastic IP address before you can release it. Otherwise, Amazon EC2 returns an error ( InvalidIPAddress.InUse ).   After releasing an Elastic IP address, it i...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">releaseHostsSTSRole(hostId, stsParams, roleName, callback)</td>
    <td style="padding:15px">When you no longer want to use an On-Demand Dedicated Host it can be released. On-Demand billing is stopped and the host goes into  released  state. The host ID of Dedicated Hosts that have been released can no longer be specified in another request, for example, to modify the host. You must stop or terminate all instances on a host before it can be released.   When Dedicated Hosts are released, it may take some time for them to stop counting toward your limit and you may receive capacity error...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceIamInstanceProfileAssociationSTSRole(iamInstanceProfileArn, iamInstanceProfileName, associationId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Replaces an IAM instance profile for the specified running instance. You can use this action to change the IAM instance profile that's associated with an instance without having to disassociate the existing IAM instance profile first.   Use  DescribeIamInstanceProfileAssociations  to get the association ID.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceNetworkAclAssociationSTSRole(associationId, dryRun, networkAclId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Changes which network ACL a subnet is associated with. By default when you create a subnet, it's automatically associated with the default network ACL. For more information, see  Network ACLs  in the  Amazon Virtual Private Cloud User Guide .   This is an idempotent operation.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceNetworkAclEntrySTSRole(cidrBlock, dryRun, egress, icmpCode, icmpType, ipv6CidrBlock, networkAclId, portRangeFrom, portRangeTo, protocol, ruleAction = 'allow', ruleNumber, stsParams, roleName, callback)</td>
    <td style="padding:15px">Replaces an entry (rule) in a network ACL. For more information, see  Network ACLs  in the  Amazon Virtual Private Cloud User Guide .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceRouteSTSRole(destinationCidrBlock, destinationIpv6CidrBlock, dryRun, egressOnlyInternetGatewayId, gatewayId, instanceId, natGatewayId, transitGatewayId, networkInterfaceId, routeTableId, vpcPeeringConnectionId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Replaces an existing route within a route table in a VPC. You must provide only one of the following: internet gateway or virtual private gateway, NAT instance, NAT gateway, VPC peering connection, network interface, or egress-only internet gateway.   For more information, see  Route Tables  in the  Amazon Virtual Private Cloud User Guide .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceRouteTableAssociationSTSRole(associationId, dryRun, routeTableId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Changes the route table associated with a given subnet in a VPC. After the operation completes, the subnet uses the routes in the new route table it's associated with. For more information about route tables, see  Route Tables  in the  Amazon Virtual Private Cloud User Guide .   You can also use ReplaceRouteTableAssociation to change which table is the main route table in the VPC. You just specify the main route table's association ID and the route table to be the new main route table.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceTransitGatewayRouteSTSRole(destinationCidrBlock, transitGatewayRouteTableId, transitGatewayAttachmentId, blackhole, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Replaces the specified route in the specified transit gateway route table.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reportInstanceStatusSTSRole(description, dryRun, endTime, instanceId, reasonCode, startTime, status = 'ok', stsParams, roleName, callback)</td>
    <td style="padding:15px">Submits feedback about the status of an instance. The instance must be in the  running  state. If your experience with the instance differs from the instance status returned by  DescribeInstanceStatus , use  ReportInstanceStatus  to report your experience with the instance. Amazon EC2 collects this information to improve the accuracy of status checks.   Use of this action does not change the value returned by  DescribeInstanceStatus .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">requestSpotFleetSTSRole(dryRun, spotFleetRequestConfigAllocationStrategy, spotFleetRequestConfigOnDemandAllocationStrategy, spotFleetRequestConfigClientToken, spotFleetRequestConfigExcessCapacityTerminationPolicy, spotFleetRequestConfigFulfilledCapacity, spotFleetRequestConfigOnDemandFulfilledCapacity, spotFleetRequestConfigIamFleetRole, spotFleetRequestConfigLaunchSpecifications, spotFleetRequestConfigLaunchTemplateConfigs, spotFleetRequestConfigSpotPrice, spotFleetRequestConfigTargetCapacity, spotFleetRequestConfigOnDemandTargetCapacity, spotFleetRequestConfigTerminateInstancesWithExpiration, spotFleetRequestConfigType, spotFleetRequestConfigValidFrom, spotFleetRequestConfigValidUntil, spotFleetRequestConfigReplaceUnhealthyInstances, spotFleetRequestConfigInstanceInterruptionBehavior, spotFleetRequestConfigLoadBalancersConfig, spotFleetRequestConfigInstancePoolsToUseCount, stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates a Spot Fleet request.   The Spot Fleet request specifies the total target capacity and the On-Demand target capacity. Amazon EC2 calculates the difference between the total capacity and On-Demand capacity, and launches the difference as Spot capacity.   You can submit a single request that includes multiple launch specifications that vary by instance type, AMI, Availability Zone, or subnet.   By default, the Spot Fleet requests Spot Instances in the Spot pool where the price per unit is...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">requestSpotInstancesSTSRole(availabilityZoneGroup, blockDurationMinutes, clientToken, dryRun, instanceCount, launchGroup, launchSpecificationSecurityGroupIds, launchSpecificationSecurityGroups, launchSpecificationAddressingType, launchSpecificationBlockDeviceMappings, launchSpecificationEbsOptimized, launchSpecificationIamInstanceProfile, launchSpecificationImageId, launchSpecificationInstanceType, launchSpecificationKernelId, launchSpecificationKeyName, launchSpecificationMonitoring, launchSpecificationNetworkInterfaces, launchSpecificationPlacement, launchSpecificationRamdiskId, launchSpecificationSubnetId, launchSpecificationUserData, spotPrice, type = 'one-time', validFrom, validUntil, instanceInterruptionBehavior = 'hibernate', stsParams, roleName, callback)</td>
    <td style="padding:15px">Creates a Spot Instance request.   For more information, see  Spot Instance Requests  in the  Amazon EC2 User Guide for Linux Instances .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resetFpgaImageAttributeSTSRole(dryRun, fpgaImageId, attribute = 'loadPermission', stsParams, roleName, callback)</td>
    <td style="padding:15px">Resets the specified attribute of the specified Amazon FPGA Image (AFI) to its default value. You can only reset the load permission attribute.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resetImageAttributeSTSRole(attribute = 'launchPermission', imageId, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Resets an attribute of an AMI to its default value.     The productCodes attribute can't be reset.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resetInstanceAttributeSTSRole(attribute = 'instanceType', dryRun, instanceId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Resets an attribute of an instance to its default value. To reset the  kernel  or  ramdisk , the instance must be in a stopped state. To reset the  sourceDestCheck , the instance can be either running or stopped.   The  sourceDestCheck  attribute controls whether source/destination checking is enabled. The default value is  true , which means checking is enabled. This value must be  false  for a NAT instance to perform NAT. For more information, see  NAT Instances  in the  Amazon Virtual Privat...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resetNetworkInterfaceAttributeSTSRole(dryRun, networkInterfaceId, sourceDestCheck, stsParams, roleName, callback)</td>
    <td style="padding:15px">Resets a network interface attribute. You can specify only one attribute at a time.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resetSnapshotAttributeSTSRole(attribute = 'productCodes', snapshotId, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Resets permission settings for the specified snapshot.   For more information about modifying snapshot permissions, see  Sharing Snapshots  in the  Amazon Elastic Compute Cloud User Guide .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">restoreAddressToClassicSTSRole(dryRun, publicIp, stsParams, roleName, callback)</td>
    <td style="padding:15px">Restores an Elastic IP address that was previously moved to the EC2-VPC platform back to the EC2-Classic platform. You cannot move an Elastic IP address that was originally allocated for use in EC2-VPC. The Elastic IP address must not be associated with an instance or network interface.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">revokeClientVpnIngressSTSRole(clientVpnEndpointId, targetNetworkCidr, accessGroupId, revokeAllGroups, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Removes an ingress authorization rule from a Client VPN endpoint.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">revokeSecurityGroupEgressSTSRole(dryRun, groupId, ipPermissions, cidrIp, fromPort, ipProtocol, toPort, sourceSecurityGroupName, sourceSecurityGroupOwnerId, stsParams, roleName, callback)</td>
    <td style="padding:15px">[VPC only] Removes the specified egress rules from a security group for EC2-VPC. This action doesn't apply to security groups for use in EC2-Classic. To remove a rule, the values that you specify (for example, ports) must match the existing rule's values exactly.   Each rule consists of the protocol and the IPv4 or IPv6 CIDR range or source security group. For the TCP and UDP protocols, you must also specify the destination port or range of ports. For the ICMP protocol, you must also specify th...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">revokeSecurityGroupIngressWithOptionsSTSRole(cidrIp, fromPort, groupId, groupName, ipPermissions, ipProtocol, sourceSecurityGroupName, sourceSecurityGroupOwnerId, toPort, dryRun, iapMetadata, stsParams, roleName, callback)</td>
    <td style="padding:15px">Removes the specified ingress rules from a security group. To remove a rule, the values that you specify (for example, ports) must match the existing rule's values exactly.     [EC2-Classic only] If the values you specify do not match the existing rule's values, no error is returned. Use  DescribeSecurityGroups  to verify that the rule has been removed.     Each rule consists of the protocol and the CIDR range or source security group. For the TCP and UDP protocols, you must also specify the de...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runInstancesSTSRole(blockDeviceMapping, imageId, instanceType = 't1.micro', ipv6AddressCount, ipv6Address, kernelId, keyName, maxCount, minCount, monitoringEnabled, placementAvailabilityZone, placementAffinity, placementGroupName, placementPartitionNumber, placementHostId, placementTenancy, placementSpreadDomain, ramdiskId, securityGroupId, securityGroup, subnetId, userData, additionalInfo, clientToken, disableApiTermination, dryRun, ebsOptimized, iamInstanceProfileArn, iamInstanceProfileName, instanceInitiatedShutdownBehavior = 'stop', networkInterface, privateIpAddress, elasticGpuSpecification, elasticInferenceAccelerator, tagSpecification, launchTemplateLaunchTemplateId, launchTemplateLaunchTemplateName, launchTemplateVersion, instanceMarketOptionsMarketType, instanceMarketOptionsSpotOptions, creditSpecificationCpuCredits, cpuOptionsCoreCount, cpuOptionsThreadsPerCore, capacityReservationSpecificationCapacityReservationPreference, capacityReservationSpecificationCapacityReservationTarget, hibernationOptionsConfigured, licenseSpecification, stsParams, roleName, callback)</td>
    <td style="padding:15px">Launches the specified number of instances using an AMI for which you have permissions.    You can specify a number of options, or leave the default options. The following rules apply:       [EC2-VPC] If you don't specify a subnet ID, we choose a default subnet from your default VPC for you. If you don't have a default VPC, you must specify a subnet ID in the request.       [EC2-Classic] If don't specify an Availability Zone, we choose one for you.       Some instance types must be launched int...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">runScheduledInstancesSTSRole(clientToken, dryRun, instanceCount, launchSpecificationBlockDeviceMappings, launchSpecificationEbsOptimized, launchSpecificationIamInstanceProfile, launchSpecificationImageId, launchSpecificationInstanceType, launchSpecificationKernelId, launchSpecificationKeyName, launchSpecificationMonitoring, launchSpecificationNetworkInterfaces, launchSpecificationPlacement, launchSpecificationRamdiskId, launchSpecificationSecurityGroupIds, launchSpecificationSubnetId, launchSpecificationUserData, scheduledInstanceId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Launches the specified Scheduled Instances.   Before you can launch a Scheduled Instance, you must purchase it and obtain an identifier using  PurchaseScheduledInstances .   You must launch a Scheduled Instance during its scheduled time period. You can't stop or reboot a Scheduled Instance, but you can terminate it as needed. If you terminate a Scheduled Instance before the current scheduled time period ends, you can launch it again after a few minutes. For more information, see  Scheduled Inst...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchTransitGatewayRoutesSTSRole(transitGatewayRouteTableId, filter, maxResults, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Searches for routes in the specified transit gateway route table.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startInstancesSTSRole(instanceId, additionalInfo, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Starts an Amazon EBS-backed instance that you've previously stopped.   Instances that use Amazon EBS volumes as their root devices can be quickly stopped and started. When an instance is stopped, the compute resources are released and you are not billed for instance usage. However, your root partition Amazon EBS volume remains and continues to persist your data, and you are charged for Amazon EBS volume usage. You can restart your instance at any time. Every time you start your Windows instance...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">stopInstancesSTSRole(instanceId, hibernate, dryRun, force, stsParams, roleName, callback)</td>
    <td style="padding:15px">Stops an Amazon EBS-backed instance.   You can use the Stop action to hibernate an instance if the instance is  enabled for hibernation  and it meets the  hibernation prerequisites . For more information, see  Hibernate Your Instance  in the  Amazon Elastic Compute Cloud User Guide .   We don't charge usage for a stopped instance, or data transfer fees; however, your root partition Amazon EBS volume remains and continues to persist your data, and you are charged for Amazon EBS volume usage. Eve...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">terminateClientVpnConnectionsSTSRole(clientVpnEndpointId, connectionId, username, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Terminates active Client VPN endpoint connections. This action can be used to terminate a specific client connection, or up to five connections established by a specific user.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">terminateInstancesSTSRole(instanceId, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Shuts down the specified instances. This operation is idempotent; if you terminate an instance more than once, each call succeeds.    If you specify multiple instances and the request fails (for example, because of a single incorrect instance ID), none of the instances are terminated.   Terminated instances remain visible after termination (for approximately one hour).   By default, Amazon EC2 deletes all EBS volumes that were attached when the instance launched. Volumes attached after instance...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unassignIpv6AddressesSTSRole(ipv6Addresses, networkInterfaceId, stsParams, roleName, callback)</td>
    <td style="padding:15px">Unassigns one or more IPv6 addresses from a network interface.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unassignPrivateIpAddressesSTSRole(networkInterfaceId, privateIpAddress, stsParams, roleName, callback)</td>
    <td style="padding:15px">Unassigns one or more secondary private IP addresses from a network interface.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unmonitorInstancesSTSRole(instanceId, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Disables detailed monitoring for a running instance. For more information, see  Monitoring Your Instances and Volumes  in the  Amazon Elastic Compute Cloud User Guide .</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSecurityGroupRuleDescriptionsEgressSTSRole(dryRun, groupId, groupName, ipPermissions, stsParams, roleName, callback)</td>
    <td style="padding:15px">[VPC only] Updates the description of an egress (outbound) security group rule. You can replace an existing description, or add a description to a rule that did not have one previously.   You specify the description as part of the IP permissions structure. You can remove a description for a security group rule by omitting the description parameter in the request.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSecurityGroupRuleDescriptionsIngressSTSRole(dryRun, groupId, groupName, ipPermissions, stsParams, roleName, callback)</td>
    <td style="padding:15px">Updates the description of an ingress (inbound) security group rule. You can replace an existing description, or add a description to a rule that did not have one previously.   You specify the description as part of the IP permissions structure. You can remove a description for a security group rule by omitting the description parameter in the request.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">withdrawByoipCidrSTSRole(cidr, dryRun, stsParams, roleName, callback)</td>
    <td style="padding:15px">Stops advertising an IPv4 address range that is provisioned as an address pool.   You can perform this operation at most once every 10 seconds, even if you specify different address ranges each time.   It can take a few minutes before traffic to the specified addresses stops routing to AWS because of BGP propagation delays.</td>
    <td style="padding:15px">{base_path}/{version}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
