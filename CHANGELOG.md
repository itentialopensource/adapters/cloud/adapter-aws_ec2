
## 0.9.10 [11-10-2024]

* more auth additions

See merge request itentialopensource/adapters/adapter-aws_ec2!51

---

## 0.9.9 [10-15-2024]

* Changes made at 2024.10.14_21:24PM

See merge request itentialopensource/adapters/adapter-aws_ec2!50

---

## 0.9.8 [10-10-2024]

* add sts region

See merge request itentialopensource/adapters/adapter-aws_ec2!48

---

## 0.9.7 [09-30-2024]

* update auth documentation

See merge request itentialopensource/adapters/adapter-aws_ec2!47

---

## 0.9.6 [09-12-2024]

* fix properties

See merge request itentialopensource/adapters/adapter-aws_ec2!46

---

## 0.9.5 [09-10-2024]

* add sts properties

See merge request itentialopensource/adapters/adapter-aws_ec2!45

---

## 0.9.4 [08-23-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-aws_ec2!44

---

## 0.9.3 [08-14-2024]

* Changes made at 2024.08.14_19:42PM

See merge request itentialopensource/adapters/adapter-aws_ec2!43

---

## 0.9.2 [08-07-2024]

* Changes made at 2024.08.06_21:46PM

See merge request itentialopensource/adapters/adapter-aws_ec2!42

---

## 0.9.1 [08-06-2024]

* Changes made at 2024.08.06_15:33PM

See merge request itentialopensource/adapters/adapter-aws_ec2!41

---

## 0.9.0 [08-05-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/cloud/adapter-aws_ec2!40

---

## 0.8.8 [05-13-2024]

* Patch/adapt 3378

See merge request itentialopensource/adapters/cloud/adapter-aws_ec2!39

---

## 0.8.7 [03-28-2024]

* Changes made at 2024.03.28_13:27PM

See merge request itentialopensource/adapters/cloud/adapter-aws_ec2!38

---

## 0.8.6 [03-20-2024]

* Fix pronghorn.json duplicate inputs

See merge request itentialopensource/adapters/cloud/adapter-aws_ec2!35

---

## 0.8.5 [03-15-2024]

* Update metadata.json

See merge request itentialopensource/adapters/cloud/adapter-aws_ec2!34

---

## 0.8.4 [03-11-2024]

* Changes made at 2024.03.11_15:38PM

See merge request itentialopensource/adapters/cloud/adapter-aws_ec2!33

---

## 0.8.3 [02-28-2024]

* Changes made at 2024.02.28_11:54AM

See merge request itentialopensource/adapters/cloud/adapter-aws_ec2!32

---

## 0.8.2 [02-21-2024]

* updates for broker calls

See merge request itentialopensource/adapters/cloud/adapter-aws_ec2!31

---

## 0.8.1 [01-27-2024]

* make changes non breaking

See merge request itentialopensource/adapters/cloud/adapter-aws_ec2!30

---

## 0.8.0 [01-02-2024]

* Added support for dynamic region per call

See merge request itentialopensource/adapters/cloud/adapter-aws_ec2!29

---

## 0.7.3 [12-14-2023]

* Remediation Merge Request

See merge request itentialopensource/adapters/cloud/adapter-aws_ec2!28

---

## 0.7.2 [11-17-2023]

* Patch/adapt 2943

See merge request itentialopensource/adapters/cloud/adapter-aws_ec2!27

---

## 0.7.1 [11-10-2023]

* updated ec2 to use new auth handler

See merge request itentialopensource/adapters/cloud/adapter-aws_ec2!25

---

## 0.7.0 [11-10-2023]

* More migration changes

See merge request itentialopensource/adapters/cloud/adapter-aws_ec2!26

---

## 0.6.9 [09-18-2023]

* Update AUTH Markdown

See merge request itentialopensource/adapters/cloud/adapter-aws_ec2!24

---

## 0.6.8 [01-04-2023]

* fix url

See merge request itentialopensource/adapters/cloud/adapter-aws_ec2!15

---

## 0.6.7 [10-22-2021]

* fix url

See merge request itentialopensource/adapters/cloud/adapter-aws_ec2!15

---

## 0.6.6 [10-22-2021]

* Add the ability to refresh the properties

See merge request itentialopensource/adapters/cloud/adapter-aws_ec2!14

---

## 0.6.5 [03-02-2021]

* migration to the latest adapter foundation

See merge request itentialopensource/adapters/cloud/adapter-aws_ec2!12

---

## 0.6.4 [09-03-2020]

* updates to Run Instance

See merge request itentialopensource/adapters/cloud/adapter-aws_ec2!11

---

## 0.6.3 [07-06-2020]

* Migration

See merge request itentialopensource/adapters/cloud/adapter-aws_ec2!10

---

## 0.6.2 [07-06-2020]

* Migration

See merge request itentialopensource/adapters/cloud/adapter-aws_ec2!10

---

## 0.6.1 [06-23-2020]

* Patch/adapt 231

See merge request itentialopensource/adapters/cloud/adapter-aws_ec2!8

---

## 0.6.0 [03-12-2020]

* Minor/adapt 125

See merge request itentialopensource/adapters/cloud/adapter-aws_ec2!7

---

## 0.5.0 [03-04-2020]

* Minor/descibe vpc details

See merge request itentialopensource/adapters/cloud/adapter-aws_ec2!6

---

## 0.4.0 [02-20-2020]

* Minor/adapt 33

See merge request itentialopensource/adapters/cloud/adapter-aws_ec2!5

---

## 0.3.3 [01-16-2020]

* Patch/dec migration

See merge request itentialopensource/adapters/cloud/adapter-aws_ec2!3

---

## 0.3.2 [01-16-2020]

* Patch/dec migration

See merge request itentialopensource/adapters/cloud/adapter-aws_ec2!3

---

## 0.3.1 [01-16-2020]

* Patch/dec migration

See merge request itentialopensource/adapters/cloud/adapter-aws_ec2!3

---

## 0.3.0 [11-07-2019]

* Minor/october migration

See merge request itentialopensource/adapters/cloud/adapter-aws_ec2!2

---

## 0.2.0 [09-11-2019]

* september migration

See merge request itentialopensource/adapters/cloud/adapter-aws_ec2!1

---

## 0.1.2 [08-14-2019]

* Bug fixes and performance improvements

See commit 66e387e

---

## 0.1.1 [08-14-2019]

* Bug fixes and performance improvements

See commit 067ce1b

---
