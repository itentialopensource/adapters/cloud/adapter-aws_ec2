# AWS EC2

Vendor: Amazon Web Services
Homepage: https://aws.amazon.com/

Product: Elastic Compute Cloud (EC2)
Product Page: https://aws.amazon.com/ec2/

## Introduction
We classify AWS EC2 into the Cloud domain as EC2 provides Cloud Services. We also classify it into the Inventory domain because it contains an inventory of VPCs.

"EC2 offers the broadest cloud compute platform" 
"EC2 provides secure and resizable compute capacity for virtually any workload" 

The AWS EC2 adapter can be integrated to the Itential Device Broker which will allow your VPCs to be managed within the Itential Configuration Manager Application.

## Why Integrate
The AWS EC2 adapter from Itential is used to integrate the Itential Automation Platform (IAP) with AWS EC2. With this adapter you have the ability to perform operations such as:

- Configure and Manage VPCs. 

## Additional Product Documentation
The [API documents for AWS EC2](https://docs.aws.amazon.com/AWSEC2/latest/APIReference/making-api-requests.html)
