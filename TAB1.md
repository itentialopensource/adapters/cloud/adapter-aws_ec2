# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Awsec2 System. The API that was used to build the adapter for Awsec2 is usually available in the report directory of this adapter. The adapter utilizes the Awsec2 API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The AWS EC2 adapter from Itential is used to integrate the Itential Automation Platform (IAP) with AWS EC2. With this adapter you have the ability to perform operations such as:

- Configure and Manage VPCs. 

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
