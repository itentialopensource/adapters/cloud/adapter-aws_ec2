
## 0.6.8 [01-04-2023]

* fix url

See merge request itentialopensource/adapters/cloud/adapter-aws_ec2!15

---

## 0.6.7 [10-22-2021]

* fix url

See merge request itentialopensource/adapters/cloud/adapter-aws_ec2!15

---

## 0.6.6 [10-22-2021]

* Add the ability to refresh the properties

See merge request itentialopensource/adapters/cloud/adapter-aws_ec2!14

---

## 0.6.5 [03-02-2021]

* migration to the latest adapter foundation

See merge request itentialopensource/adapters/cloud/adapter-aws_ec2!12

---

## 0.6.4 [09-03-2020]

* updates to Run Instance

See merge request itentialopensource/adapters/cloud/adapter-aws_ec2!11

---

## 0.6.3 [07-06-2020]

* Migration

See merge request itentialopensource/adapters/cloud/adapter-aws_ec2!10

---

## 0.6.2 [07-06-2020]

* Migration

See merge request itentialopensource/adapters/cloud/adapter-aws_ec2!10

---

## 0.6.1 [06-23-2020]

* Patch/adapt 231

See merge request itentialopensource/adapters/cloud/adapter-aws_ec2!8

---

## 0.6.0 [03-12-2020]

* Minor/adapt 125

See merge request itentialopensource/adapters/cloud/adapter-aws_ec2!7

---

## 0.5.0 [03-04-2020]

* Minor/descibe vpc details

See merge request itentialopensource/adapters/cloud/adapter-aws_ec2!6

---

## 0.4.0 [02-20-2020]

* Minor/adapt 33

See merge request itentialopensource/adapters/cloud/adapter-aws_ec2!5

---

## 0.3.3 [01-16-2020]

* Patch/dec migration

See merge request itentialopensource/adapters/cloud/adapter-aws_ec2!3

---

## 0.3.2 [01-16-2020]

* Patch/dec migration

See merge request itentialopensource/adapters/cloud/adapter-aws_ec2!3

---

## 0.3.1 [01-16-2020]

* Patch/dec migration

See merge request itentialopensource/adapters/cloud/adapter-aws_ec2!3

---

## 0.3.0 [11-07-2019]

* Minor/october migration

See merge request itentialopensource/adapters/cloud/adapter-aws_ec2!2

---

## 0.2.0 [09-11-2019]

* september migration

See merge request itentialopensource/adapters/cloud/adapter-aws_ec2!1

---

## 0.1.2 [08-14-2019]

* Bug fixes and performance improvements

See commit 66e387e

---

## 0.1.1 [08-14-2019]

* Bug fixes and performance improvements

See commit 067ce1b

---
