/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;
samProps.host = 'replace.hostorip.here';
samProps.authentication.username = 'username';
samProps.authentication.password = 'password';
samProps.protocol = 'http';
samProps.port = 80;
samProps.ssl.enabled = false;
samProps.ssl.accept_invalid_cert = false;
if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-aws_ec2',
      type: 'Awsec2',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const Awsec2 = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Awsec2 Adapter Test', () => {
  describe('Awsec2 Class Tests', () => {
    const a = new Awsec2(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-aws_ec2-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-aws_ec2-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    const actionAcceptReservedInstancesExchangeQuoteReservedInstanceId = [];
    describe('#acceptReservedInstancesExchangeQuote - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.acceptReservedInstancesExchangeQuote(null, actionAcceptReservedInstancesExchangeQuoteReservedInstanceId, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionAcceptReservedInstancesExchangeQuote', 'acceptReservedInstancesExchangeQuote', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionAcceptTransitGatewayVpcAttachmentTransitGatewayAttachmentId = 'fakedata';
    describe('#acceptTransitGatewayVpcAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.acceptTransitGatewayVpcAttachment(actionAcceptTransitGatewayVpcAttachmentTransitGatewayAttachmentId, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionAcceptTransitGatewayVpcAttachment', 'acceptTransitGatewayVpcAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionAcceptVpcEndpointConnectionsServiceId = 'fakedata';
    const actionAcceptVpcEndpointConnectionsVpcEndpointId = [];
    describe('#acceptVpcEndpointConnections - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.acceptVpcEndpointConnections(null, actionAcceptVpcEndpointConnectionsServiceId, actionAcceptVpcEndpointConnectionsVpcEndpointId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionAcceptVpcEndpointConnections', 'acceptVpcEndpointConnections', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#acceptVpcPeeringConnection - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.acceptVpcPeeringConnection(null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionAcceptVpcPeeringConnection', 'acceptVpcPeeringConnection', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionAdvertiseByoipCidrCidr = 'fakedata';
    describe('#advertiseByoipCidr - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.advertiseByoipCidr(actionAdvertiseByoipCidrCidr, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionAdvertiseByoipCidr', 'advertiseByoipCidr', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#allocateAddress - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.allocateAddress(null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionAllocateAddress', 'allocateAddress', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionAllocateHostsAvailabilityZone = 'fakedata';
    const actionAllocateHostsInstanceType = 'fakedata';
    const actionAllocateHostsQuantity = 555;
    describe('#allocateHosts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.allocateHosts(null, actionAllocateHostsAvailabilityZone, null, actionAllocateHostsInstanceType, actionAllocateHostsQuantity, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionAllocateHosts', 'allocateHosts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionApplySecurityGroupsToClientVpnTargetNetworkClientVpnEndpointId = 'fakedata';
    const actionApplySecurityGroupsToClientVpnTargetNetworkVpcId = 'fakedata';
    const actionApplySecurityGroupsToClientVpnTargetNetworkSecurityGroupId = [];
    describe('#applySecurityGroupsToClientVpnTargetNetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.applySecurityGroupsToClientVpnTargetNetwork(actionApplySecurityGroupsToClientVpnTargetNetworkClientVpnEndpointId, actionApplySecurityGroupsToClientVpnTargetNetworkVpcId, actionApplySecurityGroupsToClientVpnTargetNetworkSecurityGroupId, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionApplySecurityGroupsToClientVpnTargetNetwork', 'applySecurityGroupsToClientVpnTargetNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionAssignIpv6AddressesNetworkInterfaceId = 'fakedata';
    describe('#assignIpv6Addresses - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.assignIpv6Addresses(null, null, actionAssignIpv6AddressesNetworkInterfaceId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionAssignIpv6Addresses', 'assignIpv6Addresses', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionAssignPrivateIpAddressesNetworkInterfaceId = 'fakedata';
    describe('#assignPrivateIpAddresses - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.assignPrivateIpAddresses(null, actionAssignPrivateIpAddressesNetworkInterfaceId, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_ec2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionAssignPrivateIpAddresses', 'assignPrivateIpAddresses', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#associateAddress - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.associateAddress(null, null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionAssociateAddress', 'associateAddress', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionAssociateClientVpnTargetNetworkClientVpnEndpointId = 'fakedata';
    const actionAssociateClientVpnTargetNetworkSubnetId = 'fakedata';
    describe('#associateClientVpnTargetNetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.associateClientVpnTargetNetwork(actionAssociateClientVpnTargetNetworkClientVpnEndpointId, actionAssociateClientVpnTargetNetworkSubnetId, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionAssociateClientVpnTargetNetwork', 'associateClientVpnTargetNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionAssociateDhcpOptionsDhcpOptionsId = 'fakedata';
    const actionAssociateDhcpOptionsVpcId = 'fakedata';
    describe('#associateDhcpOptions - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.associateDhcpOptions(actionAssociateDhcpOptionsDhcpOptionsId, actionAssociateDhcpOptionsVpcId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_ec2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionAssociateDhcpOptions', 'associateDhcpOptions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionAssociateIamInstanceProfileInstanceId = 'fakedata';
    describe('#associateIamInstanceProfile - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.associateIamInstanceProfile(null, null, actionAssociateIamInstanceProfileInstanceId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionAssociateIamInstanceProfile', 'associateIamInstanceProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionAssociateRouteTableRouteTableId = 'fakedata';
    const actionAssociateRouteTableSubnetId = 'fakedata';
    describe('#associateRouteTable - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.associateRouteTable(null, actionAssociateRouteTableRouteTableId, actionAssociateRouteTableSubnetId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionAssociateRouteTable', 'associateRouteTable', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionAssociateSubnetCidrBlockIpv6CidrBlock = 'fakedata';
    const actionAssociateSubnetCidrBlockSubnetId = 'fakedata';
    describe('#associateSubnetCidrBlock - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.associateSubnetCidrBlock(actionAssociateSubnetCidrBlockIpv6CidrBlock, actionAssociateSubnetCidrBlockSubnetId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionAssociateSubnetCidrBlock', 'associateSubnetCidrBlock', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionAssociateTransitGatewayRouteTableTransitGatewayRouteTableId = 'fakedata';
    const actionAssociateTransitGatewayRouteTableTransitGatewayAttachmentId = 'fakedata';
    describe('#associateTransitGatewayRouteTable - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.associateTransitGatewayRouteTable(actionAssociateTransitGatewayRouteTableTransitGatewayRouteTableId, actionAssociateTransitGatewayRouteTableTransitGatewayAttachmentId, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionAssociateTransitGatewayRouteTable', 'associateTransitGatewayRouteTable', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionAssociateVpcCidrBlockVpcId = 'fakedata';
    describe('#associateVpcCidrBlock - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.associateVpcCidrBlock(null, null, actionAssociateVpcCidrBlockVpcId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionAssociateVpcCidrBlock', 'associateVpcCidrBlock', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionAttachClassicLinkVpcSecurityGroupId = [];
    const actionAttachClassicLinkVpcInstanceId = 'fakedata';
    const actionAttachClassicLinkVpcVpcId = 'fakedata';
    describe('#attachClassicLinkVpc - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.attachClassicLinkVpc(null, actionAttachClassicLinkVpcSecurityGroupId, actionAttachClassicLinkVpcInstanceId, actionAttachClassicLinkVpcVpcId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionAttachClassicLinkVpc', 'attachClassicLinkVpc', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionAttachInternetGatewayInternetGatewayId = 'fakedata';
    const actionAttachInternetGatewayVpcId = 'fakedata';
    describe('#attachInternetGateway - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.attachInternetGateway(null, actionAttachInternetGatewayInternetGatewayId, actionAttachInternetGatewayVpcId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_ec2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionAttachInternetGateway', 'attachInternetGateway', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionAttachNetworkInterfaceDeviceIndex = 555;
    const actionAttachNetworkInterfaceInstanceId = 'fakedata';
    const actionAttachNetworkInterfaceNetworkInterfaceId = 'fakedata';
    describe('#attachNetworkInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.attachNetworkInterface(actionAttachNetworkInterfaceDeviceIndex, null, actionAttachNetworkInterfaceInstanceId, actionAttachNetworkInterfaceNetworkInterfaceId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionAttachNetworkInterface', 'attachNetworkInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionAttachVolumeDevice = 'fakedata';
    const actionAttachVolumeInstanceId = 'fakedata';
    const actionAttachVolumeVolumeId = 'fakedata';
    describe('#attachVolume - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.attachVolume(actionAttachVolumeDevice, actionAttachVolumeInstanceId, actionAttachVolumeVolumeId, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionAttachVolume', 'attachVolume', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionAttachVpnGatewayVpcId = 'fakedata';
    const actionAttachVpnGatewayVpnGatewayId = 'fakedata';
    describe('#attachVpnGateway - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.attachVpnGateway(actionAttachVpnGatewayVpcId, actionAttachVpnGatewayVpnGatewayId, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionAttachVpnGateway', 'attachVpnGateway', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionAuthorizeClientVpnIngressClientVpnEndpointId = 'fakedata';
    const actionAuthorizeClientVpnIngressTargetNetworkCidr = 'fakedata';
    describe('#authorizeClientVpnIngress - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.authorizeClientVpnIngress(actionAuthorizeClientVpnIngressClientVpnEndpointId, actionAuthorizeClientVpnIngressTargetNetworkCidr, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionAuthorizeClientVpnIngress', 'authorizeClientVpnIngress', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionAuthorizeSecurityGroupEgressGroupId = 'fakedata';
    describe('#authorizeSecurityGroupEgress - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.authorizeSecurityGroupEgress(null, actionAuthorizeSecurityGroupEgressGroupId, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_ec2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionAuthorizeSecurityGroupEgress', 'authorizeSecurityGroupEgress', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#authorizeSecurityGroupIngress - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.authorizeSecurityGroupIngress(null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_ec2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionAuthorizeSecurityGroupIngress', 'authorizeSecurityGroupIngress', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionBundleInstanceInstanceId = 'fakedata';
    describe('#bundleInstance - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.bundleInstance(actionBundleInstanceInstanceId, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionBundleInstance', 'bundleInstance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionCancelBundleTaskBundleId = 'fakedata';
    describe('#cancelBundleTask - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.cancelBundleTask(actionCancelBundleTaskBundleId, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionCancelBundleTask', 'cancelBundleTask', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionCancelCapacityReservationCapacityReservationId = 'fakedata';
    describe('#cancelCapacityReservation - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.cancelCapacityReservation(actionCancelCapacityReservationCapacityReservationId, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionCancelCapacityReservation', 'cancelCapacityReservation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionCancelConversionTaskConversionTaskId = 'fakedata';
    describe('#cancelConversionTask - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.cancelConversionTask(actionCancelConversionTaskConversionTaskId, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_ec2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionCancelConversionTask', 'cancelConversionTask', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionCancelExportTaskExportTaskId = 'fakedata';
    describe('#cancelExportTask - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.cancelExportTask(actionCancelExportTaskExportTaskId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_ec2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionCancelExportTask', 'cancelExportTask', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cancelImportTask - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.cancelImportTask(null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionCancelImportTask', 'cancelImportTask', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionCancelReservedInstancesListingReservedInstancesListingId = 'fakedata';
    describe('#cancelReservedInstancesListing - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.cancelReservedInstancesListing(actionCancelReservedInstancesListingReservedInstancesListingId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionCancelReservedInstancesListing', 'cancelReservedInstancesListing', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionCancelSpotFleetRequestsSpotFleetRequestId = [];
    const actionCancelSpotFleetRequestsTerminateInstances = true;
    describe('#cancelSpotFleetRequests - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.cancelSpotFleetRequests(null, actionCancelSpotFleetRequestsSpotFleetRequestId, actionCancelSpotFleetRequestsTerminateInstances, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionCancelSpotFleetRequests', 'cancelSpotFleetRequests', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionCancelSpotInstanceRequestsSpotInstanceRequestId = [];
    describe('#cancelSpotInstanceRequests - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.cancelSpotInstanceRequests(null, actionCancelSpotInstanceRequestsSpotInstanceRequestId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionCancelSpotInstanceRequests', 'cancelSpotInstanceRequests', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionConfirmProductInstanceInstanceId = 'fakedata';
    const actionConfirmProductInstanceProductCode = 'fakedata';
    describe('#confirmProductInstance - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.confirmProductInstance(actionConfirmProductInstanceInstanceId, actionConfirmProductInstanceProductCode, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionConfirmProductInstance', 'confirmProductInstance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionCopyFpgaImageSourceFpgaImageId = 'fakedata';
    const actionCopyFpgaImageSourceRegion = 'fakedata';
    describe('#copyFpgaImage - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.copyFpgaImage(null, actionCopyFpgaImageSourceFpgaImageId, null, null, actionCopyFpgaImageSourceRegion, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionCopyFpgaImage', 'copyFpgaImage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionCopyImageSourceImageId = 'fakedata';
    const actionCopyImageName = 'fakedata';
    const actionCopyImageSourceRegion = 'fakedata';
    describe('#copyImage - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.copyImage(null, null, null, null, actionCopyImageName, actionCopyImageSourceImageId, actionCopyImageSourceRegion, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionCopyImage', 'copyImage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionCopySnapshotSourceSnapshotId = 'fakedata';
    const actionCopySnapshotSourceRegion = 'fakedata';
    describe('#copySnapshot - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.copySnapshot(null, null, null, null, null, actionCopySnapshotSourceRegion, actionCopySnapshotSourceSnapshotId, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionCopySnapshot', 'copySnapshot', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionCreateCapacityReservationInstanceType = 'fakedata';
    const actionCreateCapacityReservationInstancePlatform = 'fakedata';
    const actionCreateCapacityReservationAvailabilityZone = 'fakedata';
    const actionCreateCapacityReservationInstanceCount = 555;
    describe('#createCapacityReservation - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createCapacityReservation(null, actionCreateCapacityReservationInstanceType, actionCreateCapacityReservationInstancePlatform, actionCreateCapacityReservationAvailabilityZone, null, actionCreateCapacityReservationInstanceCount, null, null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionCreateCapacityReservation', 'createCapacityReservation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionCapacityReservationArray = [{
      instanceType: 'sfwf', instancePlatform: 'wefwef', availabilityZone: 'sdfwef', instanceCount: 12331
    }];
    describe('#createCapacityReservations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createCapacityReservations(actionCapacityReservationArray, (data, error) => {
            try {
              runCommonAsserts(data[0], error);
              if (stub) {
                assert.equal('object', typeof data[0].response);
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionCreateClientVpnEndpointClientCidrBlock = 'fakedata';
    const actionCreateClientVpnEndpointServerCertificateArn = 'fakedata';
    const actionCreateClientVpnEndpointAuthentication = [];
    describe('#createClientVpnEndpoint - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createClientVpnEndpoint(actionCreateClientVpnEndpointClientCidrBlock, actionCreateClientVpnEndpointServerCertificateArn, actionCreateClientVpnEndpointAuthentication, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionCreateClientVpnEndpoint', 'createClientVpnEndpoint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionClientVpnEndpointArray = [{ clientCidrBlock: 'sfwf', serverCertificateArn: 'wefwef', authentication: [] }];
    describe('#createClientVpnEndpoints - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createClientVpnEndpoints(actionClientVpnEndpointArray, (data, error) => {
            try {
              runCommonAsserts(data[0], error);
              if (stub) {
                assert.equal('object', typeof data[0].response);
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionCreateClientVpnRouteClientVpnEndpointId = 'fakedata';
    const actionCreateClientVpnRouteDestinationCidrBlock = 'fakedata';
    const actionCreateClientVpnRouteTargetVpcSubnetId = 'fakedata';
    describe('#createClientVpnRoute - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createClientVpnRoute(actionCreateClientVpnRouteClientVpnEndpointId, actionCreateClientVpnRouteDestinationCidrBlock, actionCreateClientVpnRouteTargetVpcSubnetId, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionCreateClientVpnRoute', 'createClientVpnRoute', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionClientVpnRouteArray = [{ clientVpnEndpointId: 'sfwf', destinationCidrBlock: 'wefwef', targetVpcSubnetId: 'sdfwef' }];
    describe('#createClientVpnRoutes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createClientVpnRoutes(actionClientVpnRouteArray, (data, error) => {
            try {
              runCommonAsserts(data[0], error);
              if (stub) {
                assert.equal('object', typeof data[0].response);
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionCreateCustomerGatewayBgpAsn = 555;
    const actionCreateCustomerGatewayIpAddress = 'fakedata';
    const actionCreateCustomerGatewayType = 'fakedata';
    describe('#createCustomerGateway - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createCustomerGateway(actionCreateCustomerGatewayBgpAsn, actionCreateCustomerGatewayIpAddress, actionCreateCustomerGatewayType, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionCreateCustomerGateway', 'createCustomerGateway', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionCustomerGatewayArray = [{ bgpAsn: 'fakeparam', ipAddress: 'fakeparam', type: 'fakeparam' }];
    describe('#createCustomerGateways - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createCustomerGateways(actionCustomerGatewayArray, (data, error) => {
            try {
              runCommonAsserts(data[0], error);
              if (stub) {
                assert.equal('object', typeof data[0].response);
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionCreateDefaultSubnetAvailabilityZone = 'fakedata';
    describe('#createDefaultSubnet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createDefaultSubnet(actionCreateDefaultSubnetAvailabilityZone, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionCreateDefaultSubnet', 'createDefaultSubnet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionCreateDefaultSubnetArray = [{ availabilityZone: 'fakeparam' }];
    describe('#createDefaultSubnets - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createDefaultSubnets(actionCreateDefaultSubnetArray, (data, error) => {
            try {
              runCommonAsserts(data[0], error);
              if (stub) {
                assert.equal('object', typeof data[0].response);
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createDefaultVpc - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createDefaultVpc(null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionCreateDefaultVpc', 'createDefaultVpc', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionCreateDefaultVpcArray = [{ dryRun: true }];
    describe('#createDefaultVpcs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createDefaultVpcs(actionCreateDefaultVpcArray, (data, error) => {
            try {
              runCommonAsserts(data[0], error);
              if (stub) {
                assert.equal('object', typeof data[0].response);
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionCreateDhcpOptionsDhcpConfiguration = [];
    describe('#createDhcpOptions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createDhcpOptions(actionCreateDhcpOptionsDhcpConfiguration, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionCreateDhcpOptions', 'createDhcpOptions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dhcpOptionsArray = [{ dhcpConfiguration: [] }];
    describe('#createMultipleDhcpOptions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createMultipleDhcpOptions(dhcpOptionsArray, (data, error) => {
            try {
              runCommonAsserts(data[0], error);
              if (stub) {
                assert.equal('object', typeof data[0].response);
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionCreateEgressOnlyInternetGatewayVpcId = 'fakedata';
    describe('#createEgressOnlyInternetGateway - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createEgressOnlyInternetGateway(null, null, actionCreateEgressOnlyInternetGatewayVpcId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionCreateEgressOnlyInternetGateway', 'createEgressOnlyInternetGateway', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const egressOnlyInternetGatewaysArray = [{ vpcId: 'fakeparam' }];
    describe('#createEgressOnlyInternetGateways - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createEgressOnlyInternetGateways(egressOnlyInternetGatewaysArray, (data, error) => {
            try {
              runCommonAsserts(data[0], error);
              if (stub) {
                assert.equal('object', typeof data[0].response);
              } else {
                runCommonAsserts(data[0], error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionCreateFleetLaunchTemplateConfigs = [];
    describe('#createFleet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createFleet(null, null, null, null, null, null, null, null, null, null, null, null, null, actionCreateFleetLaunchTemplateConfigs, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionCreateFleet', 'createFleet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const fleetsArray = [{ launchTemplateConfigs: [] }];
    describe('#createFleets - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createFleets(fleetsArray, (data, error) => {
            try {
              runCommonAsserts(data[0], error);
              if (stub) {
                assert.equal('object', typeof data[0].response);
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionCreateFlowLogsResourceId = [];
    const actionCreateFlowLogsResourceType = 'fakedata';
    const actionCreateFlowLogsTrafficType = 'fakedata';
    describe('#createFlowLogs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createFlowLogs(null, null, null, null, actionCreateFlowLogsResourceId, actionCreateFlowLogsResourceType, actionCreateFlowLogsTrafficType, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionCreateFlowLogs', 'createFlowLogs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const flowLogsArray = [{ resourceId: 'fakedata', resourceType: [], trafficType: 'fakedata' }];
    describe('#createMultipleFlowLogs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createMultipleFlowLogs(flowLogsArray, (data, error) => {
            try {
              runCommonAsserts(data[0], error);
              if (stub) {
                assert.equal('object', typeof data[0].response);
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createFpgaImage - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createFpgaImage(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionCreateFpgaImage', 'createFpgaImage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const FpgaImagesArray = [{ instanceId: 'fakedata', name: 'fakedata' }];
    describe('#createFpgaImages - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createFpgaImages(FpgaImagesArray, (data, error) => {
            try {
              runCommonAsserts(data[0], error);
              if (stub) {
                assert.equal('object', typeof data[0].response);
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionCreateImageInstanceId = 'fakedata';
    const actionCreateImageName = 'fakedata';
    describe('#createImage - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createImage(null, null, null, actionCreateImageInstanceId, actionCreateImageName, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionCreateImage', 'createImage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ImagesArray = [{ instanceId: 'fakedata', name: 'fakedata' }];
    describe('#createImages - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createImages(ImagesArray, (data, error) => {
            try {
              runCommonAsserts(data[0], error);
              if (stub) {
                assert.equal('object', typeof data[0].response);
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionCreateInstanceExportTaskInstanceId = 'fakedata';
    describe('#createInstanceExportTask - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createInstanceExportTask(null, null, null, null, null, actionCreateInstanceExportTaskInstanceId, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionCreateInstanceExportTask', 'createInstanceExportTask', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const InstanceExportTasksArray = [{ instanceId: 'fakedata', name: 'fakedata' }];
    describe('#createInstanceExportTasks - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createInstanceExportTasks(InstanceExportTasksArray, (data, error) => {
            try {
              runCommonAsserts(data[0], error);
              if (stub) {
                assert.equal('object', typeof data[0].response);
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createInternetGateway - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createInternetGateway(null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionCreateInternetGateway', 'createInternetGateway', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionCreateInternetGatewaysInternetGatewayArray = [{ dryRun: true }];
    describe('#createInternetGateways - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createInternetGateways(actionCreateInternetGatewaysInternetGatewayArray, (data, error) => {
            try {
              runCommonAsserts(data[0], error);
              if (stub) {
                assert.equal('object', typeof data[0].response);
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionCreateKeyPairKeyName = 'fakedata';
    describe('#createKeyPair - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createKeyPair(actionCreateKeyPairKeyName, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionCreateKeyPair', 'createKeyPair', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const KeyPairsArray = [{ keyName: 'fakedata' }];
    describe('#createKeyPairs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createKeyPairs(KeyPairsArray, (data, error) => {
            try {
              runCommonAsserts(data[0], error);
              if (stub) {
                assert.equal('object', typeof data[0].response);
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionCreateLaunchTemplateLaunchTemplateName = 'fakedata';
    describe('#createLaunchTemplate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createLaunchTemplate(null, null, actionCreateLaunchTemplateLaunchTemplateName, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionCreateLaunchTemplate', 'createLaunchTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const LaunchTemplatesArray = [{ launchTemplateName: 'fakedata' }];
    describe('#createLaunchTemplates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createLaunchTemplates(LaunchTemplatesArray, (data, error) => {
            try {
              runCommonAsserts(data[0], error);
              if (stub) {
                assert.equal('object', typeof data[0].response);
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createLaunchTemplateVersion - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createLaunchTemplateVersion(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionCreateLaunchTemplateVersion', 'createLaunchTemplateVersion', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const LaunchTemplateVersionsArray = [{ keyName: 'fakedata' }];
    describe('#createLaunchTemplateVersions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createLaunchTemplateVersions(LaunchTemplateVersionsArray, (data, error) => {
            try {
              runCommonAsserts(data[0], error);
              if (stub) {
                assert.equal('object', typeof data[0].response);
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionCreateNatGatewayAllocationId = 'fakedata';
    const actionCreateNatGatewaySubnetId = 'fakedata';
    describe('#createNatGateway - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createNatGateway(actionCreateNatGatewayAllocationId, null, actionCreateNatGatewaySubnetId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionCreateNatGateway', 'createNatGateway', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const NatGatewaysArray = [{ allocationId: 'fakedata', subnetId: 'fakedata' }];
    describe('#createNatGateways - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createNatGateways(NatGatewaysArray, (data, error) => {
            try {
              runCommonAsserts(data[0], error);
              if (stub) {
                assert.equal('object', typeof data[0].response);
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionCreateNetworkAclVpcId = 'fakedata';
    describe('#createNetworkAcl - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createNetworkAcl(null, actionCreateNetworkAclVpcId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionCreateNetworkAcl', 'createNetworkAcl', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const NetworkAclsArray = [{ vpcId: 'fakedata' }];
    describe('#createNetworkAcls - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createNetworkAcls(NetworkAclsArray, (data, error) => {
            try {
              runCommonAsserts(data[0], error);
              if (stub) {
                assert.equal('object', typeof data[0].response);
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionCreateNetworkAclEntryEgress = true;
    const actionCreateNetworkAclEntryNetworkAclId = 'fakedata';
    const actionCreateNetworkAclEntryProtocol = 'fakedata';
    const actionCreateNetworkAclEntryRuleAction = 'fakedata';
    const actionCreateNetworkAclEntryRuleNumber = 555;
    describe('#createNetworkAclEntry - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createNetworkAclEntry(null, null, actionCreateNetworkAclEntryEgress, null, null, null, actionCreateNetworkAclEntryNetworkAclId, null, null, actionCreateNetworkAclEntryProtocol, actionCreateNetworkAclEntryRuleAction, actionCreateNetworkAclEntryRuleNumber, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_ec2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionCreateNetworkAclEntry', 'createNetworkAclEntry', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionCreateNetworkInterfaceSubnetId = 'fakedata';
    describe('#createNetworkInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createNetworkInterface(null, null, null, null, null, null, null, null, null, actionCreateNetworkInterfaceSubnetId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionCreateNetworkInterface', 'createNetworkInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const NetworkInterfacesArray = [{ subnetId: 'fakedata' }];
    describe('#createNetworkInterfaces - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createNetworkInterfaces(NetworkInterfacesArray, (data, error) => {
            try {
              runCommonAsserts(data[0], error);
              if (stub) {
                assert.equal('object', typeof data[0].response);
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionCreateNetworkInterfacePermissionNetworkInterfaceId = 'fakedata';
    const actionCreateNetworkInterfacePermissionPermission = 'fakedata';
    describe('#createNetworkInterfacePermission - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createNetworkInterfacePermission(actionCreateNetworkInterfacePermissionNetworkInterfaceId, null, null, actionCreateNetworkInterfacePermissionPermission, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionCreateNetworkInterfacePermission', 'createNetworkInterfacePermission', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const InterfacePermissionsArray = [{ networkInterfaceId: 'fakedata', permission: 'fakedata' }];
    describe('#createNetworkInterfacePermissions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createNetworkInterfacePermissions(InterfacePermissionsArray, (data, error) => {
            try {
              runCommonAsserts(data[0], error);
              if (stub) {
                assert.equal('object', typeof data[0].response);
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createPlacementGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createPlacementGroup(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_ec2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionCreatePlacementGroup', 'createPlacementGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const PlacementGroupsArray = [{}];
    describe('#createPlacementGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createPlacementGroups(PlacementGroupsArray, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(data[0].icode, 'AD.500');
                assert.equal(data[0].IAPerror.origin, 'Test-aws_ec2-connectorRest-handleEndResponse');
                assert.equal(data[0].IAPerror.displayString, displayE);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionCreateReservedInstancesListingClientToken = 'fakedata';
    const actionCreateReservedInstancesListingInstanceCount = 555;
    const actionCreateReservedInstancesListingPriceSchedules = [];
    const actionCreateReservedInstancesListingReservedInstancesId = 'fakedata';
    describe('#createReservedInstancesListing - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createReservedInstancesListing(actionCreateReservedInstancesListingClientToken, actionCreateReservedInstancesListingInstanceCount, actionCreateReservedInstancesListingPriceSchedules, actionCreateReservedInstancesListingReservedInstancesId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionCreateReservedInstancesListing', 'createReservedInstancesListing', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ReservedInstancesListingArray = [{
      clientToken: 'fakedata', instanceCount: 1344, priceSchedules: [], reservedInstancesId: 'fakedata'
    }];
    describe('#createReservedInstancesListings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createReservedInstancesListings(ReservedInstancesListingArray, (data, error) => {
            try {
              runCommonAsserts(data[0], error);
              if (stub) {
                assert.equal('object', typeof data[0].response);
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionCreateRouteRouteTableId = 'fakedata';
    describe('#createRoute - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createRoute(null, null, null, null, null, null, null, null, null, actionCreateRouteRouteTableId, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionCreateRoute', 'createRoute', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionCreateRoutesRouteArray = [{ routeTableId: 's1f14fqawe' }];
    describe('#createRoutes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createRoutes(actionCreateRoutesRouteArray, (data, error) => {
            try {
              runCommonAsserts(data[0], error);
              if (stub) {
                assert.equal('object', typeof data[0].response);
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionCreateRouteTableVpcId = 'fakedata';
    describe('#createRouteTable - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createRouteTable(null, actionCreateRouteTableVpcId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionCreateRouteTable', 'createRouteTable', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionCreateRouteTablesRouteTableArray = [{ vpcId: 'sdfwqef12312' }];
    describe('#createRouteTables - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createRouteTables(actionCreateRouteTablesRouteTableArray, (data, error) => {
            try {
              runCommonAsserts(data[0], error);
              if (stub) {
                assert.equal('object', typeof data[0].response);
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionCreateSecurityGroupGroupDescription = 'fakedata';
    const actionCreateSecurityGroupGroupName = 'fakedata';
    describe('#createSecurityGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createSecurityGroup(actionCreateSecurityGroupGroupDescription, actionCreateSecurityGroupGroupName, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionCreateSecurityGroup', 'createSecurityGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionCreateSecurityGroupArray = [{ groupDescription: 'sdfwqef12312', groupName: '23rqef' }];
    describe('#createSecurityGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createSecurityGroups(actionCreateSecurityGroupArray, (data, error) => {
            try {
              runCommonAsserts(data[0], error);
              if (stub) {
                assert.equal('object', typeof data[0].response);
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionCreateSnapshotVolumeId = 'fakedata';
    describe('#createSnapshot - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createSnapshot(null, actionCreateSnapshotVolumeId, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionCreateSnapshot', 'createSnapshot', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const SnapshotsArray = [{ volumeId: 'fakedata' }];
    describe('#createSnapshots - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createSnapshots(SnapshotsArray, (data, error) => {
            try {
              runCommonAsserts(data[0], error);
              if (stub) {
                assert.equal('object', typeof data[0].response);
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionCreateSpotDatafeedSubscriptionBucket = 'fakedata';
    describe('#createSpotDatafeedSubscription - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createSpotDatafeedSubscription(actionCreateSpotDatafeedSubscriptionBucket, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionCreateSpotDatafeedSubscription', 'createSpotDatafeedSubscription', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const SpotDatafeedSubscriptionsArray = [{ bucket: 'fakedata' }];
    describe('#createSpotDatafeedSubscriptions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createSpotDatafeedSubscriptions(SpotDatafeedSubscriptionsArray, (data, error) => {
            try {
              runCommonAsserts(data[0], error);
              if (stub) {
                assert.equal('object', typeof data[0].response);
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionCreateSubnetCidrBlock = 'fakedata';
    const actionCreateSubnetVpcId = 'fakedata';
    describe('#createSubnet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createSubnet(null, null, actionCreateSubnetCidrBlock, null, actionCreateSubnetVpcId, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionCreateSubnet', 'createSubnet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionCreateSubnetsSubnetArray = [{ cidrBlock: '10.0.0.0/16', vpcId: '324124wfewf' }];
    describe('#createSubnets - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createSubnets(actionCreateSubnetsSubnetArray, (data, error) => {
            try {
              runCommonAsserts(data[0], error);
              if (stub) {
                assert.equal('object', typeof data[0].response);
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionCreateTagsResourceId = [];
    const actionCreateTagsTag = [];
    describe('#createTags - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createTags(null, actionCreateTagsResourceId, actionCreateTagsTag, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_ec2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionCreateTags', 'createTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const MultipleTagsArray = [{ resourceId: [], tag: [] }];
    describe('#createMultipleTags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createMultipleTags(MultipleTagsArray, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(data[0].icode, 'AD.500');
                assert.equal(data[0].IAPerror.origin, 'Test-aws_ec2-connectorRest-handleEndResponse');
                assert.equal(data[0].IAPerror.displayString, displayE);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createTransitGateway - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createTransitGateway(null, null, null, null, null, null, 'abc', null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionCreateTransitGateway', 'createTransitGateway', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const TransitGatewaysArray = [{}];
    describe('#createTransitGateways - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createTransitGateways(TransitGatewaysArray, (data, error) => {
            try {
              runCommonAsserts(data[0], error);
              if (stub) {
                assert.equal('object', typeof data[0].response);
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionCreateTransitGatewayRouteDestinationCidrBlock = 'fakedata';
    const actionCreateTransitGatewayRouteTransitGatewayRouteTableId = 'fakedata';
    describe('#createTransitGatewayRoute - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createTransitGatewayRoute(actionCreateTransitGatewayRouteDestinationCidrBlock, actionCreateTransitGatewayRouteTransitGatewayRouteTableId, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionCreateTransitGatewayRoute', 'createTransitGatewayRoute', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const TransitGatewayRoutesArray = [{ destinationCidrBlock: 'fakedata', transitGatewayRouteTableId: 'fakedata' }];
    describe('#createTransitGatewayRoutes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createTransitGatewayRoutes(TransitGatewayRoutesArray, (data, error) => {
            try {
              runCommonAsserts(data[0], error);
              if (stub) {
                assert.equal('object', typeof data[0].response);
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionCreateTransitGatewayRouteTableTransitGatewayId = 'fakedata';
    describe('#createTransitGatewayRouteTable - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createTransitGatewayRouteTable(actionCreateTransitGatewayRouteTableTransitGatewayId, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionCreateTransitGatewayRouteTable', 'createTransitGatewayRouteTable', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const TransitGatewayRouteTablesArray = [{ transitGatewayId: 'fakedata' }];
    describe('#createTransitGatewayRouteTables - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createTransitGatewayRouteTables(TransitGatewayRouteTablesArray, (data, error) => {
            try {
              runCommonAsserts(data[0], error);
              if (stub) {
                assert.equal('object', typeof data[0].response);
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionCreateTransitGatewayVpcAttachmentTransitGatewayId = 'fakedata';
    const actionCreateTransitGatewayVpcAttachmentVpcId = 'fakedata';
    const actionCreateTransitGatewayVpcAttachmentSubnetIds = [];
    describe('#createTransitGatewayVpcAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createTransitGatewayVpcAttachment(actionCreateTransitGatewayVpcAttachmentTransitGatewayId, actionCreateTransitGatewayVpcAttachmentVpcId, actionCreateTransitGatewayVpcAttachmentSubnetIds, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionCreateTransitGatewayVpcAttachment', 'createTransitGatewayVpcAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const TransitGatewayVpcAttachmentsArray = [{ transitGatewayId: 'fakedata', vpcId: 'fakedata', subnetIds: [] }];
    describe('#createTransitGatewayVpcAttachments - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createTransitGatewayVpcAttachments(TransitGatewayVpcAttachmentsArray, (data, error) => {
            try {
              runCommonAsserts(data[0], error);
              if (stub) {
                assert.equal('object', typeof data[0].response);
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionCreateVolumeAvailabilityZone = 'fakedata';
    describe('#createVolume - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createVolume(actionCreateVolumeAvailabilityZone, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionCreateVolume', 'createVolume', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const TransitGatewayVpcAttachmentssArray = [{ availabilityZone: 'fakedata' }];
    describe('#createVolumes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createVolumes(TransitGatewayVpcAttachmentssArray, (data, error) => {
            try {
              runCommonAsserts(data[0], error);
              if (stub) {
                assert.equal('object', typeof data[0].response);
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionCreateVpcCidrBlock = 'fakedata';
    describe('#createVpc - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createVpc(actionCreateVpcCidrBlock, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionCreateVpc', 'createVpc', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionCreateVpcsVpcArray = [{ cidrBlock: '10.0.0.0/16' }];
    describe('#createVpcs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createVpcs(actionCreateVpcsVpcArray, (data, error) => {
            try {
              runCommonAsserts(data[0], error);
              if (stub) {
                assert.equal('object', typeof data[0].response);
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionCreateVpcEndpointVpcId = 'fakedata';
    const actionCreateVpcEndpointServiceName = 'fakedata';
    describe('#createVpcEndpoint - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createVpcEndpoint(null, null, actionCreateVpcEndpointVpcId, actionCreateVpcEndpointServiceName, null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionCreateVpcEndpoint', 'createVpcEndpoint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const createVpcEndpointsArray = [{ vpcId: 'fakedata', serviceName: 'fakedata' }];
    describe('#createVpcEndpoints - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createVpcEndpoints(createVpcEndpointsArray, (data, error) => {
            try {
              runCommonAsserts(data[0], error);
              if (stub) {
                assert.equal('object', typeof data[0].response);
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionCreateVpcEndpointConnectionNotificationConnectionNotificationArn = 'fakedata';
    const actionCreateVpcEndpointConnectionNotificationConnectionEvents = [];
    describe('#createVpcEndpointConnectionNotification - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createVpcEndpointConnectionNotification(null, null, null, actionCreateVpcEndpointConnectionNotificationConnectionNotificationArn, actionCreateVpcEndpointConnectionNotificationConnectionEvents, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionCreateVpcEndpointConnectionNotification', 'createVpcEndpointConnectionNotification', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const createVpcEndpointConnectionNotificationsArray = [{ connectionNotificationArn: 'fakedata', connectionEvents: [] }];
    describe('#createVpcEndpointConnectionNotifications - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createVpcEndpointConnectionNotifications(createVpcEndpointConnectionNotificationsArray, (data, error) => {
            try {
              runCommonAsserts(data[0], error);
              if (stub) {
                assert.equal('object', typeof data[0].response);
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionCreateVpcEndpointServiceConfigurationNetworkLoadBalancerArn = [];
    describe('#createVpcEndpointServiceConfiguration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createVpcEndpointServiceConfiguration(null, null, actionCreateVpcEndpointServiceConfigurationNetworkLoadBalancerArn, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionCreateVpcEndpointServiceConfiguration', 'createVpcEndpointServiceConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const createVpcEndpointServiceConfigurationsArray = [{ networkLoadBalancerArn: 'fakedata' }];
    describe('#createVpcEndpointServiceConfigurations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createVpcEndpointServiceConfigurations(createVpcEndpointServiceConfigurationsArray, (data, error) => {
            try {
              runCommonAsserts(data[0], error);
              if (stub) {
                assert.equal('object', typeof data[0].response);
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createVpcPeeringConnection - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createVpcPeeringConnection(null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionCreateVpcPeeringConnection', 'createVpcPeeringConnection', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const createVpcPeeringConnectionsArray = [{ networkLoadBalancerArn: 'fakedata' }];
    describe('#createVpcPeeringConnections - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createVpcPeeringConnections(createVpcPeeringConnectionsArray, (data, error) => {
            try {
              runCommonAsserts(data[0], error);
              if (stub) {
                assert.equal('object', typeof data[0].response);
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionCreateVpnConnectionCustomerGatewayId = 'fakedata';
    const actionCreateVpnConnectionType = 'fakedata';
    describe('#createVpnConnection - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createVpnConnection(actionCreateVpnConnectionCustomerGatewayId, actionCreateVpnConnectionType, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionCreateVpnConnection', 'createVpnConnection', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const createVpnConnectionsArray = [{ customerGatewayId: 'fakedata', type: 'fakedata' }];
    describe('#createVpnConnections - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createVpnConnections(createVpnConnectionsArray, (data, error) => {
            try {
              runCommonAsserts(data[0], error);
              if (stub) {
                assert.equal('object', typeof data[0].response);
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionCreateVpnConnectionRouteDestinationCidrBlock = 'fakedata';
    const actionCreateVpnConnectionRouteVpnConnectionId = 'fakedata';
    describe('#createVpnConnectionRoute - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createVpnConnectionRoute(actionCreateVpnConnectionRouteDestinationCidrBlock, actionCreateVpnConnectionRouteVpnConnectionId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_ec2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionCreateVpnConnectionRoute', 'createVpnConnectionRoute', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const createVpnConnectionRoutesArray = [{ destinationCidrBlock: 'fakedata', vpnConnectionId: 'fakedata' }];
    describe('#createVpnConnectionRoutes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createVpnConnectionRoutes(createVpnConnectionRoutesArray, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(data[0].icode, 'AD.500');
                assert.equal(data[0].IAPerror.origin, 'Test-aws_ec2-connectorRest-handleEndResponse');
                assert.equal(data[0].IAPerror.displayString, displayE);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionCreateVpnGatewayType = 'fakedata';
    describe('#createVpnGateway - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createVpnGateway(null, actionCreateVpnGatewayType, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionCreateVpnGateway', 'createVpnGateway', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const createVpnGatewaysArray = [{ type: 'fakedata' }];
    describe('#createVpnGateways - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createVpnGateways(createVpnGatewaysArray, (data, error) => {
            try {
              runCommonAsserts(data[0], error);
              if (stub) {
                assert.equal('object', typeof data[0].response);
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDeleteClientVpnEndpointClientVpnEndpointId = 'fakedata';
    describe('#deleteClientVpnEndpoint - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteClientVpnEndpoint(actionDeleteClientVpnEndpointClientVpnEndpointId, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDeleteClientVpnEndpoint', 'deleteClientVpnEndpoint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDeleteClientVpnRouteClientVpnEndpointId = 'fakedata';
    const actionDeleteClientVpnRouteDestinationCidrBlock = 'fakedata';
    describe('#deleteClientVpnRoute - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteClientVpnRoute(actionDeleteClientVpnRouteClientVpnEndpointId, null, actionDeleteClientVpnRouteDestinationCidrBlock, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDeleteClientVpnRoute', 'deleteClientVpnRoute', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDeleteCustomerGatewayCustomerGatewayId = 'fakedata';
    describe('#deleteCustomerGateway - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteCustomerGateway(actionDeleteCustomerGatewayCustomerGatewayId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_ec2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDeleteCustomerGateway', 'deleteCustomerGateway', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDeleteDhcpOptionsDhcpOptionsId = 'fakedata';
    describe('#deleteDhcpOptions - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDhcpOptions(actionDeleteDhcpOptionsDhcpOptionsId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_ec2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDeleteDhcpOptions', 'deleteDhcpOptions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDeleteEgressOnlyInternetGatewayEgressOnlyInternetGatewayId = 'fakedata';
    describe('#deleteEgressOnlyInternetGateway - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteEgressOnlyInternetGateway(null, actionDeleteEgressOnlyInternetGatewayEgressOnlyInternetGatewayId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDeleteEgressOnlyInternetGateway', 'deleteEgressOnlyInternetGateway', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDeleteFleetsFleetId = [];
    const actionDeleteFleetsTerminateInstances = true;
    describe('#deleteFleets - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteFleets(null, actionDeleteFleetsFleetId, actionDeleteFleetsTerminateInstances, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDeleteFleets', 'deleteFleets', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDeleteFlowLogsFlowLogId = [];
    describe('#deleteFlowLogs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteFlowLogs(null, actionDeleteFlowLogsFlowLogId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDeleteFlowLogs', 'deleteFlowLogs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDeleteFpgaImageFpgaImageId = 'fakedata';
    describe('#deleteFpgaImage - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteFpgaImage(null, actionDeleteFpgaImageFpgaImageId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDeleteFpgaImage', 'deleteFpgaImage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDeleteInternetGatewayInternetGatewayId = 'fakedata';
    describe('#deleteInternetGateway - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteInternetGateway(null, actionDeleteInternetGatewayInternetGatewayId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_ec2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDeleteInternetGateway', 'deleteInternetGateway', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDeleteInternetGatewayInternetGatewayArray = [{ internetGatewayId: '3fea23' }];
    describe('#deleteInternetGateways - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteInternetGateways(actionDeleteInternetGatewayInternetGatewayArray, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(data[0].icode, 'AD.500');
                assert.equal(data[0].IAPerror.origin, 'Test-aws_ec2-connectorRest-handleEndResponse');
                assert.equal(data[0].IAPerror.displayString, displayE);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDeleteKeyPairKeyName = 'fakedata';
    describe('#deleteKeyPair - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteKeyPair(actionDeleteKeyPairKeyName, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_ec2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDeleteKeyPair', 'deleteKeyPair', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteLaunchTemplate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteLaunchTemplate(null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDeleteLaunchTemplate', 'deleteLaunchTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDeleteLaunchTemplateVersionsLaunchTemplateVersion = [];
    describe('#deleteLaunchTemplateVersions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteLaunchTemplateVersions(null, null, null, actionDeleteLaunchTemplateVersionsLaunchTemplateVersion, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDeleteLaunchTemplateVersions', 'deleteLaunchTemplateVersions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDeleteNatGatewayNatGatewayId = 'fakedata';
    describe('#deleteNatGateway - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteNatGateway(actionDeleteNatGatewayNatGatewayId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDeleteNatGateway', 'deleteNatGateway', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDeleteNetworkAclNetworkAclId = 'fakedata';
    describe('#deleteNetworkAcl - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteNetworkAcl(null, actionDeleteNetworkAclNetworkAclId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_ec2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDeleteNetworkAcl', 'deleteNetworkAcl', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDeleteNetworkAclEntryEgress = true;
    const actionDeleteNetworkAclEntryNetworkAclId = 'fakedata';
    const actionDeleteNetworkAclEntryRuleNumber = 555;
    describe('#deleteNetworkAclEntry - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteNetworkAclEntry(null, actionDeleteNetworkAclEntryEgress, actionDeleteNetworkAclEntryNetworkAclId, actionDeleteNetworkAclEntryRuleNumber, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_ec2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDeleteNetworkAclEntry', 'deleteNetworkAclEntry', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDeleteNetworkInterfaceNetworkInterfaceId = 'fakedata';
    describe('#deleteNetworkInterface - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteNetworkInterface(null, actionDeleteNetworkInterfaceNetworkInterfaceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_ec2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDeleteNetworkInterface', 'deleteNetworkInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDeleteNetworkInterfacePermissionNetworkInterfacePermissionId = 'fakedata';
    describe('#deleteNetworkInterfacePermission - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteNetworkInterfacePermission(actionDeleteNetworkInterfacePermissionNetworkInterfacePermissionId, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDeleteNetworkInterfacePermission', 'deleteNetworkInterfacePermission', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDeletePlacementGroupGroupName = 'fakedata';
    describe('#deletePlacementGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePlacementGroup(null, actionDeletePlacementGroupGroupName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_ec2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDeletePlacementGroup', 'deletePlacementGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDeleteRouteRouteTableId = 'fakedata';
    describe('#deleteRoute - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRoute(null, null, null, actionDeleteRouteRouteTableId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_ec2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDeleteRoute', 'deleteRoute', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDeleteRoutesRouteArray = [{ routeTableId: '3fea23' }];
    describe('#deleteRoutes - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRoutes(actionDeleteRoutesRouteArray, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(data[0].icode, 'AD.500');
                assert.equal(data[0].IAPerror.origin, 'Test-aws_ec2-connectorRest-handleEndResponse');
                assert.equal(data[0].IAPerror.displayString, displayE);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDeleteRouteTableRouteTableId = 'fakedata';
    describe('#deleteRouteTable - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRouteTable(null, actionDeleteRouteTableRouteTableId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_ec2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDeleteRouteTable', 'deleteRouteTable', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDeleteRouteTablesRouteTableArray = [{ routeTableId: '3fea23' }];
    describe('#deleteRouteTables - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRouteTables(actionDeleteRouteTablesRouteTableArray, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(data[0].icode, 'AD.500');
                assert.equal(data[0].IAPerror.origin, 'Test-aws_ec2-connectorRest-handleEndResponse');
                assert.equal(data[0].IAPerror.displayString, displayE);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSecurityGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSecurityGroup(null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_ec2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDeleteSecurityGroup', 'deleteSecurityGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDeleteSecurityGroupsSecurityGroupArray = [{ groupId: 'we2r3r' }];
    describe('#deleteSecurityGroups - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSecurityGroups(actionDeleteSecurityGroupsSecurityGroupArray, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(data[0].icode, 'AD.500');
                assert.equal(data[0].IAPerror.origin, 'Test-aws_ec2-connectorRest-handleEndResponse');
                assert.equal(data[0].IAPerror.displayString, displayE);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDeleteSnapshotSnapshotId = 'fakedata';
    describe('#deleteSnapshot - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSnapshot(actionDeleteSnapshotSnapshotId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_ec2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDeleteSnapshot', 'deleteSnapshot', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSpotDatafeedSubscription - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSpotDatafeedSubscription(null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_ec2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDeleteSpotDatafeedSubscription', 'deleteSpotDatafeedSubscription', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDeleteSubnetSubnetId = 'fakedata';
    describe('#deleteSubnet - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSubnet(actionDeleteSubnetSubnetId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_ec2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDeleteSubnet', 'deleteSubnet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDeleteSubnetsSubnetArray = [{ subnetId: '3fea23' }];
    describe('#deleteSubnets - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSubnets(actionDeleteSubnetsSubnetArray, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(data[0].icode, 'AD.500');
                assert.equal(data[0].IAPerror.origin, 'Test-aws_ec2-connectorRest-handleEndResponse');
                assert.equal(data[0].IAPerror.displayString, displayE);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDeleteTagsResourceId = [];
    describe('#deleteTags - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTags(null, actionDeleteTagsResourceId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_ec2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDeleteTags', 'deleteTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDeleteTransitGatewayTransitGatewayId = 'fakedata';
    describe('#deleteTransitGateway - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteTransitGateway(actionDeleteTransitGatewayTransitGatewayId, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDeleteTransitGateway', 'deleteTransitGateway', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDeleteTransitGatewayRouteTransitGatewayRouteTableId = 'fakedata';
    const actionDeleteTransitGatewayRouteDestinationCidrBlock = 'fakedata';
    describe('#deleteTransitGatewayRoute - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteTransitGatewayRoute(actionDeleteTransitGatewayRouteTransitGatewayRouteTableId, actionDeleteTransitGatewayRouteDestinationCidrBlock, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDeleteTransitGatewayRoute', 'deleteTransitGatewayRoute', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDeleteTransitGatewayRouteTableTransitGatewayRouteTableId = 'fakedata';
    describe('#deleteTransitGatewayRouteTable - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteTransitGatewayRouteTable(actionDeleteTransitGatewayRouteTableTransitGatewayRouteTableId, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDeleteTransitGatewayRouteTable', 'deleteTransitGatewayRouteTable', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDeleteTransitGatewayVpcAttachmentTransitGatewayAttachmentId = 'fakedata';
    describe('#deleteTransitGatewayVpcAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteTransitGatewayVpcAttachment(actionDeleteTransitGatewayVpcAttachmentTransitGatewayAttachmentId, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDeleteTransitGatewayVpcAttachment', 'deleteTransitGatewayVpcAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDeleteVolumeVolumeId = 'fakedata';
    describe('#deleteVolume - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteVolume(actionDeleteVolumeVolumeId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_ec2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDeleteVolume', 'deleteVolume', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDeleteVpcVpcId = 'fakedata';
    describe('#deleteVpc - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteVpc(actionDeleteVpcVpcId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_ec2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDeleteVpc', 'deleteVpc', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDeleteVpcsVpcArray = [{ vpcId: '3fea23' }];
    describe('#deleteVpcs - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteVpcs(actionDeleteVpcsVpcArray, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(data[0].icode, 'AD.500');
                assert.equal(data[0].IAPerror.origin, 'Test-aws_ec2-connectorRest-handleEndResponse');
                assert.equal(data[0].IAPerror.displayString, displayE);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDeleteVpcEndpointConnectionNotificationsConnectionNotificationId = [];
    describe('#deleteVpcEndpointConnectionNotifications - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteVpcEndpointConnectionNotifications(null, actionDeleteVpcEndpointConnectionNotificationsConnectionNotificationId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDeleteVpcEndpointConnectionNotifications', 'deleteVpcEndpointConnectionNotifications', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDeleteVpcEndpointServiceConfigurationsServiceId = [];
    describe('#deleteVpcEndpointServiceConfigurations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteVpcEndpointServiceConfigurations(null, actionDeleteVpcEndpointServiceConfigurationsServiceId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDeleteVpcEndpointServiceConfigurations', 'deleteVpcEndpointServiceConfigurations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDeleteVpcEndpointsVpcEndpointId = [];
    describe('#deleteVpcEndpoints - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteVpcEndpoints(null, actionDeleteVpcEndpointsVpcEndpointId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDeleteVpcEndpoints', 'deleteVpcEndpoints', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDeleteVpcPeeringConnectionVpcPeeringConnectionId = 'fakedata';
    describe('#deleteVpcPeeringConnection - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteVpcPeeringConnection(null, actionDeleteVpcPeeringConnectionVpcPeeringConnectionId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDeleteVpcPeeringConnection', 'deleteVpcPeeringConnection', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDeleteVpnConnectionVpnConnectionId = 'fakedata';
    describe('#deleteVpnConnection - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteVpnConnection(actionDeleteVpnConnectionVpnConnectionId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_ec2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDeleteVpnConnection', 'deleteVpnConnection', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDeleteVpnConnectionRouteDestinationCidrBlock = 'fakedata';
    const actionDeleteVpnConnectionRouteVpnConnectionId = 'fakedata';
    describe('#deleteVpnConnectionRoute - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteVpnConnectionRoute(actionDeleteVpnConnectionRouteDestinationCidrBlock, actionDeleteVpnConnectionRouteVpnConnectionId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_ec2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDeleteVpnConnectionRoute', 'deleteVpnConnectionRoute', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDeleteVpnGatewayVpnGatewayId = 'fakedata';
    describe('#deleteVpnGateway - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteVpnGateway(actionDeleteVpnGatewayVpnGatewayId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_ec2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDeleteVpnGateway', 'deleteVpnGateway', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deleteClientVpnEndpointsArray = [{ clientVpnEndpointId: 'fakedata' }];
    describe('#deleteClientVpnEndpoints - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteClientVpnEndpoints(deleteClientVpnEndpointsArray, (data, error) => {
            try {
              runCommonAsserts(data[0], error);
              if (stub) {
                assert.equal('object', typeof data[0].response);
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deleteCustomerGatewaysArray = [{ customerGatewayId: 'fakedata' }];
    describe('#deleteCustomerGateways - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteCustomerGateways(deleteCustomerGatewaysArray, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(data[0].icode, 'AD.500');
                assert.equal(data[0].IAPerror.origin, 'Test-aws_ec2-connectorRest-handleEndResponse');
                assert.equal(data[0].IAPerror.displayString, displayE);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deleteMultipleDhcpOptionsArray = [{ dhcpOptionsId: 'fakedata' }];
    describe('#deleteMultipleDhcpOptions - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteMultipleDhcpOptions(deleteMultipleDhcpOptionsArray, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(data[0].icode, 'AD.500');
                assert.equal(data[0].IAPerror.origin, 'Test-aws_ec2-connectorRest-handleEndResponse');
                assert.equal(data[0].IAPerror.displayString, displayE);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deleteClientVpnRoutesArray = [{ clientVpnEndpointId: 'fakedata', destinationCidrBlock: 'fakedata' }];
    describe('#deleteClientVpnRoutes - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteClientVpnRoutes(deleteClientVpnRoutesArray, (data, error) => {
            try {
              runCommonAsserts(data[0], error);
              if (stub) {
                assert.equal('object', typeof data[0].response);
              } else {
                runCommonAsserts(data[0], error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deleteMultipleFleetsArray = [{ fleetId: [], terminateInstances: false }];
    describe('#deleteMultipleFleets - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteMultipleFleets(deleteMultipleFleetsArray, (data, error) => {
            try {
              try {
                runCommonAsserts(data[0], error);
                if (stub) {
                  assert.equal('object', typeof data[0].response);
                } else {
                  runCommonAsserts(data[0], error);
                }
                done();
              } catch (err) {
                log.error(`Test Failure: ${err}`);
                done(err);
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deleteEgressOnlyInternetGatewaysArray = [{ egressOnlyInternetGatewayId: 'fakedata' }];
    describe('#deleteEgressOnlyInternetGateways - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteEgressOnlyInternetGateways(deleteEgressOnlyInternetGatewaysArray, (data, error) => {
            try {
              runCommonAsserts(data[0], error);
              if (stub) {
                assert.equal('object', typeof data[0].response);
              } else {
                runCommonAsserts(data[0], error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deleteMultipleFlowLogsArray = [{ flowLogId: 'fakedata' }];
    describe('#deleteMultipleFlowLogs - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteMultipleFlowLogs(deleteMultipleFlowLogsArray, (data, error) => {
            try {
              runCommonAsserts(data[0], error);
              if (stub) {
                assert.equal('object', typeof data[0].response);
              } else {
                runCommonAsserts(data[0], error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deleteLaunchTemplatesArray = [{}];
    describe('#deleteLaunchTemplates - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteLaunchTemplates(deleteLaunchTemplatesArray, (data, error) => {
            try {
              runCommonAsserts(data[0], error);
              if (stub) {
                assert.equal('object', typeof data[0].response);
              } else {
                runCommonAsserts(data[0], error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deleteNatGatewaysArray = [{ natGatewayId: 'fakedata' }];
    describe('#deleteNatGateways - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteNatGateways(deleteNatGatewaysArray, (data, error) => {
            try {
              runCommonAsserts(data[0], error);
              if (stub) {
                assert.equal('object', typeof data[0].response);
              } else {
                runCommonAsserts(data[0], error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deleteFpgaImagesArray = [{ fpgaImageId: 'fakedata' }];
    describe('#deleteFpgaImages - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteFpgaImages(deleteFpgaImagesArray, (data, error) => {
            try {
              runCommonAsserts(data[0], error);
              if (stub) {
                assert.equal('object', typeof data[0].response);
              } else {
                runCommonAsserts(data[0], error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deleteNetworkAclEntriesArray = [{ egress: false, networkAclId: 'fakedata', ruleNumber: 1234 }];
    describe('#deleteNetworkAclEntries - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteNetworkAclEntries(deleteNetworkAclEntriesArray, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(data[0].icode, 'AD.500');
                assert.equal(data[0].IAPerror.origin, 'Test-aws_ec2-connectorRest-handleEndResponse');
                assert.equal(data[0].IAPerror.displayString, displayE);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deleteNetworkInterfacesArray = [{ networkInterfaceId: 'fakedata' }];
    describe('#deleteNetworkInterfaces - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteNetworkInterfaces(deleteNetworkInterfacesArray, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(data[0].icode, 'AD.500');
                assert.equal(data[0].IAPerror.origin, 'Test-aws_ec2-connectorRest-handleEndResponse');
                assert.equal(data[0].IAPerror.displayString, displayE);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deleteNetworkInterfacePermissionsArray = [{ networkInterfacePermissionId: 'fakedata' }];
    describe('#deleteNetworkInterfacePermissions - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteNetworkInterfacePermissions(deleteNetworkInterfacePermissionsArray, (data, error) => {
            try {
              runCommonAsserts(data[0], error);
              if (stub) {
                assert.equal('object', typeof data[0].response);
              } else {
                runCommonAsserts(data[0], error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deletePlacementGroupsArray = [{ groupName: 'fakedata' }];
    describe('#deletePlacementGroups - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePlacementGroups(deletePlacementGroupsArray, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(data[0].icode, 'AD.500');
                assert.equal(data[0].IAPerror.origin, 'Test-aws_ec2-connectorRest-handleEndResponse');
                assert.equal(data[0].IAPerror.displayString, displayE);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deleteSnapshotsArray = [{ snapshotId: 'fakedata' }];
    describe('#deleteSnapshots - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSnapshots(deleteSnapshotsArray, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(data[0].icode, 'AD.500');
                assert.equal(data[0].IAPerror.origin, 'Test-aws_ec2-connectorRest-handleEndResponse');
                assert.equal(data[0].IAPerror.displayString, displayE);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deleteSpotDatafeedSubscriptionsArray = [{}];
    describe('#deleteSpotDatafeedSubscriptions - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSpotDatafeedSubscriptions(deleteSpotDatafeedSubscriptionsArray, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(data[0].icode, 'AD.500');
                assert.equal(data[0].IAPerror.origin, 'Test-aws_ec2-connectorRest-handleEndResponse');
                assert.equal(data[0].IAPerror.displayString, displayE);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deleteMultipleTagsArray = [{ resourceId: 'fakedata' }];
    describe('#deleteMultipleTags - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteMultipleTags(deleteMultipleTagsArray, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(data[0].icode, 'AD.500');
                assert.equal(data[0].IAPerror.origin, 'Test-aws_ec2-connectorRest-handleEndResponse');
                assert.equal(data[0].IAPerror.displayString, displayE);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deleteTransitGatewaysArray = [{ transitGatewayId: 'fakedata' }];
    describe('#deleteTransitGateways - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTransitGateways(deleteTransitGatewaysArray, (data, error) => {
            try {
              runCommonAsserts(data[0], error);
              if (stub) {
                assert.equal('object', typeof data[0].response);
              } else {
                runCommonAsserts(data[0], error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deleteTransitGatewayRouteTablesArray = [{ transitGatewayRouteTableId: 'fakedata' }];
    describe('#deleteTransitGatewayRouteTables - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTransitGatewayRouteTables(deleteTransitGatewayRouteTablesArray, (data, error) => {
            try {
              runCommonAsserts(data[0], error);
              if (stub) {
                assert.equal('object', typeof data[0].response);
              } else {
                runCommonAsserts(data[0], error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deleteTransitGatewayRoutesArray = [{ transitGatewayRouteTableId: 'fakedata', destinationCidrBlock: 'fakedata' }];
    describe('#deleteTransitGatewayRoutes - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTransitGatewayRoutes(deleteTransitGatewayRoutesArray, (data, error) => {
            try {
              runCommonAsserts(data[0], error);
              if (stub) {
                assert.equal('object', typeof data[0].response);
              } else {
                runCommonAsserts(data[0], error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deleteTransitGatewayVpcAttachmentsArray = [{ transitGatewayAttachmentId: 'fakedata' }];
    describe('#deleteTransitGatewayVpcAttachments - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTransitGatewayVpcAttachments(deleteTransitGatewayVpcAttachmentsArray, (data, error) => {
            try {
              runCommonAsserts(data[0], error);
              if (stub) {
                assert.equal('object', typeof data[0].response);
              } else {
                runCommonAsserts(data[0], error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deleteVolumesArray = [{ volumeId: 'fakedata' }];
    describe('#deleteVolumes - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteVolumes(deleteVolumesArray, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(data[0].icode, 'AD.500');
                assert.equal(data[0].IAPerror.origin, 'Test-aws_ec2-connectorRest-handleEndResponse');
                assert.equal(data[0].IAPerror.displayString, displayE);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deleteMultipleVpcEndpointConnectionNotificationsArray = [{ connectionNotificationId: 'fakedata' }];
    describe('#deleteMultipleVpcEndpointConnectionNotifications - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteMultipleVpcEndpointConnectionNotifications(deleteMultipleVpcEndpointConnectionNotificationsArray, (data, error) => {
            try {
              runCommonAsserts(data[0], error);
              if (stub) {
                assert.equal('object', typeof data[0].response);
              } else {
                runCommonAsserts(data[0], error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deleteMultipleVpcEndpointServiceConfigurationsArray = [{ serviceId: 'fakedata' }];
    describe('#deleteMultipleVpcEndpointServiceConfigurations - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteMultipleVpcEndpointServiceConfigurations(deleteMultipleVpcEndpointServiceConfigurationsArray, (data, error) => {
            try {
              runCommonAsserts(data[0], error);
              if (stub) {
                assert.equal('object', typeof data[0].response);
              } else {
                runCommonAsserts(data[0], error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deleteMultipleVpcEndpointsArray = [{ vpcEndpointId: 'fakedata' }];
    describe('#deleteMultipleVpcEndpoints - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteMultipleVpcEndpoints(deleteMultipleVpcEndpointsArray, (data, error) => {
            try {
              runCommonAsserts(data[0], error);
              if (stub) {
                assert.equal('object', typeof data[0].response);
              } else {
                runCommonAsserts(data[0], error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deleteVpcPeeringConnectionsArray = [{ vpcPeeringConnectionId: 'fakedata' }];
    describe('#deleteVpcPeeringConnections - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteVpcPeeringConnections(deleteVpcPeeringConnectionsArray, (data, error) => {
            try {
              runCommonAsserts(data[0], error);
              if (stub) {
                assert.equal('object', typeof data[0].response);
              } else {
                runCommonAsserts(data[0], error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deleteVpnConnectionsArray = [{ vpnConnectionId: 'fakedata' }];
    describe('#deleteVpnConnections - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteVpnConnections(deleteVpnConnectionsArray, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(data[0].icode, 'AD.500');
                assert.equal(data[0].IAPerror.origin, 'Test-aws_ec2-connectorRest-handleEndResponse');
                assert.equal(data[0].IAPerror.displayString, displayE);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deleteVpnConnectionRoutesArray = [{ destinationCidrBlock: 'fakedata', vpnConnectionId: 'fakedata' }];
    describe('#deleteVpnConnectionRoutes - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteVpnConnectionRoutes(deleteVpnConnectionRoutesArray, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(data[0].icode, 'AD.500');
                assert.equal(data[0].IAPerror.origin, 'Test-aws_ec2-connectorRest-handleEndResponse');
                assert.equal(data[0].IAPerror.displayString, displayE);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deleteVpnGatewaysArray = [{ vpnGatewayId: 'fakedata' }];
    describe('#deleteVpnGateways - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteVpnGateways(deleteVpnGatewaysArray, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(data[0].icode, 'AD.500');
                assert.equal(data[0].IAPerror.origin, 'Test-aws_ec2-connectorRest-handleEndResponse');
                assert.equal(data[0].IAPerror.displayString, displayE);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deleteNetworkAclsArray = [{ networkAclId: 'fakedata' }];
    describe('#deleteNetworkAcls - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteNetworkAcls(deleteNetworkAclsArray, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(data[0].icode, 'AD.500');
                assert.equal(data[0].IAPerror.origin, 'Test-aws_ec2-connectorRest-handleEndResponse');
                assert.equal(data[0].IAPerror.displayString, displayE);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deleteKeyPairsArray = [{ keyName: 'fakedata' }];
    describe('#deleteKeyPairs - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteKeyPairs(deleteKeyPairsArray, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.equal(data[0].icode, 'AD.500');
                assert.equal(data[0].IAPerror.origin, 'Test-aws_ec2-connectorRest-handleEndResponse');
                assert.equal(data[0].IAPerror.displayString, displayE);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deleteMultipleLaunchTemplateVersionsArray = [{ launchTemplateVersion: [] }];
    describe('#deleteMultipleLaunchTemplateVersions - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteMultipleLaunchTemplateVersions(deleteMultipleLaunchTemplateVersionsArray, (data, error) => {
            try {
              runCommonAsserts(data[0], error);
              if (stub) {
                assert.equal('object', typeof data[0].response);
              } else {
                runCommonAsserts(data[0], error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDeprovisionByoipCidrCidr = 'fakedata';
    describe('#deprovisionByoipCidr - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deprovisionByoipCidr(actionDeprovisionByoipCidrCidr, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDeprovisionByoipCidr', 'deprovisionByoipCidr', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDeregisterImageImageId = 'fakedata';
    describe('#deregisterImage - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deregisterImage(actionDeregisterImageImageId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_ec2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDeregisterImage', 'deregisterImage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeAccountAttributes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeAccountAttributes(null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeAccountAttributes', 'describeAccountAttributes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeAddresses - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeAddresses(null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeAddresses', 'describeAddresses', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeAggregateIdFormat - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeAggregateIdFormat(null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeAggregateIdFormat', 'describeAggregateIdFormat', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeAvailabilityZones - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeAvailabilityZones(null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeAvailabilityZones', 'describeAvailabilityZones', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeBundleTasks - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeBundleTasks(null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeBundleTasks', 'describeBundleTasks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDescribeByoipCidrsMaxResults = 555;
    describe('#describeByoipCidrs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeByoipCidrs(null, actionDescribeByoipCidrsMaxResults, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeByoipCidrs', 'describeByoipCidrs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeCapacityReservations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeCapacityReservations(null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeCapacityReservations', 'describeCapacityReservations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeClassicLinkInstances - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeClassicLinkInstances(null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeClassicLinkInstances', 'describeClassicLinkInstances', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDescribeClientVpnAuthorizationRulesClientVpnEndpointId = 'fakedata';
    describe('#describeClientVpnAuthorizationRules - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeClientVpnAuthorizationRules(actionDescribeClientVpnAuthorizationRulesClientVpnEndpointId, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeClientVpnAuthorizationRules', 'describeClientVpnAuthorizationRules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDescribeClientVpnConnectionsClientVpnEndpointId = 'fakedata';
    describe('#describeClientVpnConnections - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeClientVpnConnections(actionDescribeClientVpnConnectionsClientVpnEndpointId, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeClientVpnConnections', 'describeClientVpnConnections', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeClientVpnEndpoints - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeClientVpnEndpoints(null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeClientVpnEndpoints', 'describeClientVpnEndpoints', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDescribeClientVpnRoutesClientVpnEndpointId = 'fakedata';
    describe('#describeClientVpnRoutes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeClientVpnRoutes(actionDescribeClientVpnRoutesClientVpnEndpointId, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeClientVpnRoutes', 'describeClientVpnRoutes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDescribeClientVpnTargetNetworksClientVpnEndpointId = 'fakedata';
    describe('#describeClientVpnTargetNetworks - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeClientVpnTargetNetworks(actionDescribeClientVpnTargetNetworksClientVpnEndpointId, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeClientVpnTargetNetworks', 'describeClientVpnTargetNetworks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeConversionTasks - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeConversionTasks(null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeConversionTasks', 'describeConversionTasks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeCustomerGateways - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeCustomerGateways(null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeCustomerGateways', 'describeCustomerGateways', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeDhcpOptions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeDhcpOptions(null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeDhcpOptions', 'describeDhcpOptions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeEgressOnlyInternetGateways - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeEgressOnlyInternetGateways(null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeEgressOnlyInternetGateways', 'describeEgressOnlyInternetGateways', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeElasticGpus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeElasticGpus(null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeElasticGpus', 'describeElasticGpus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeExportTasks - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeExportTasks(null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeExportTasks', 'describeExportTasks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDescribeFleetHistoryFleetId = 'fakedata';
    const actionDescribeFleetHistoryStartTime = 'fakedata';
    describe('#describeFleetHistory - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeFleetHistory(null, null, null, null, actionDescribeFleetHistoryFleetId, actionDescribeFleetHistoryStartTime, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeFleetHistory', 'describeFleetHistory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDescribeFleetInstancesFleetId = 'fakedata';
    describe('#describeFleetInstances - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeFleetInstances(null, null, null, actionDescribeFleetInstancesFleetId, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeFleetInstances', 'describeFleetInstances', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeFleets - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeFleets(null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeFleets', 'describeFleets', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeFlowLogs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeFlowLogs(null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeFlowLogs', 'describeFlowLogs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDescribeFpgaImageAttributeFpgaImageId = 'fakedata';
    const actionDescribeFpgaImageAttributeAttribute = 'fakedata';
    describe('#describeFpgaImageAttribute - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeFpgaImageAttribute(null, actionDescribeFpgaImageAttributeFpgaImageId, actionDescribeFpgaImageAttributeAttribute, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeFpgaImageAttribute', 'describeFpgaImageAttribute', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeFpgaImages - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeFpgaImages(null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeFpgaImages', 'describeFpgaImages', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeHostReservationOfferings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeHostReservationOfferings(null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeHostReservationOfferings', 'describeHostReservationOfferings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeHostReservations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeHostReservations(null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeHostReservations', 'describeHostReservations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeHosts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeHosts(null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeHosts', 'describeHosts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeIamInstanceProfileAssociations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeIamInstanceProfileAssociations(null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeIamInstanceProfileAssociations', 'describeIamInstanceProfileAssociations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeIdFormat - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeIdFormat(null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeIdFormat', 'describeIdFormat', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDescribeIdentityIdFormatPrincipalArn = 'fakedata';
    describe('#describeIdentityIdFormat - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeIdentityIdFormat(actionDescribeIdentityIdFormatPrincipalArn, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeIdentityIdFormat', 'describeIdentityIdFormat', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDescribeImageAttributeImageId = 'fakedata';
    const actionDescribeImageAttributeAttribute = 'fakedata';
    describe('#describeImageAttribute - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeImageAttribute(actionDescribeImageAttributeAttribute, actionDescribeImageAttributeImageId, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeImageAttribute', 'describeImageAttribute', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeImages - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeImages(null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeImages', 'describeImages', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeImportImageTasks - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeImportImageTasks(null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeImportImageTasks', 'describeImportImageTasks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeImportSnapshotTasks - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeImportSnapshotTasks(null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeImportSnapshotTasks', 'describeImportSnapshotTasks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDescribeInstanceAttributeInstanceId = 'fakedata';
    const actionDescribeInstanceAttributeAttribute = 'fakedata';
    describe('#describeInstanceAttribute - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeInstanceAttribute(actionDescribeInstanceAttributeAttribute, null, actionDescribeInstanceAttributeInstanceId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeInstanceAttribute', 'describeInstanceAttribute', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeInstanceCreditSpecifications - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeInstanceCreditSpecifications(null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeInstanceCreditSpecifications', 'describeInstanceCreditSpecifications', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeInstanceStatus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeInstanceStatus(null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeInstanceStatus', 'describeInstanceStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeInstances - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeInstances(null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeInstances', 'describeInstances', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeInternetGateways - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeInternetGateways(null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeInternetGateways', 'describeInternetGateways', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeKeyPairs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeKeyPairs(null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeKeyPairs', 'describeKeyPairs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeLaunchTemplateVersions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeLaunchTemplateVersions(null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeLaunchTemplateVersions', 'describeLaunchTemplateVersions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeLaunchTemplates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeLaunchTemplates(null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeLaunchTemplates', 'describeLaunchTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeMovingAddresses - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeMovingAddresses(null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeMovingAddresses', 'describeMovingAddresses', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeNatGateways - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeNatGateways(null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeNatGateways', 'describeNatGateways', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeNetworkAcls - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeNetworkAcls(null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeNetworkAcls', 'describeNetworkAcls', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDescribeNetworkInterfaceAttributeNetworkInterfaceId = 'fakedata';
    describe('#describeNetworkInterfaceAttribute - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeNetworkInterfaceAttribute(null, null, actionDescribeNetworkInterfaceAttributeNetworkInterfaceId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeNetworkInterfaceAttribute', 'describeNetworkInterfaceAttribute', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeNetworkInterfacePermissions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeNetworkInterfacePermissions(null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeNetworkInterfacePermissions', 'describeNetworkInterfacePermissions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeNetworkInterfaces - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeNetworkInterfaces(null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeNetworkInterfaces', 'describeNetworkInterfaces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describePlacementGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describePlacementGroups(null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribePlacementGroups', 'describePlacementGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describePrefixLists - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describePrefixLists(null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribePrefixLists', 'describePrefixLists', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describePrincipalIdFormat - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describePrincipalIdFormat(null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribePrincipalIdFormat', 'describePrincipalIdFormat', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describePublicIpv4Pools - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describePublicIpv4Pools(null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribePublicIpv4Pools', 'describePublicIpv4Pools', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeRegions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeRegions(null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeRegions', 'describeRegions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeReservedInstances - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeReservedInstances(null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeReservedInstances', 'describeReservedInstances', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeReservedInstancesListings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeReservedInstancesListings(null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeReservedInstancesListings', 'describeReservedInstancesListings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeReservedInstancesModifications - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeReservedInstancesModifications(null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeReservedInstancesModifications', 'describeReservedInstancesModifications', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeReservedInstancesOfferings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeReservedInstancesOfferings(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeReservedInstancesOfferings', 'describeReservedInstancesOfferings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeRouteTables - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeRouteTables(null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeRouteTables', 'describeRouteTables', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeScheduledInstanceAvailability - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeScheduledInstanceAvailability(null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeScheduledInstanceAvailability', 'describeScheduledInstanceAvailability', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeScheduledInstances - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeScheduledInstances(null, null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeScheduledInstances', 'describeScheduledInstances', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDescribeSecurityGroupReferencesGroupId = [];
    describe('#describeSecurityGroupReferences - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeSecurityGroupReferences(null, actionDescribeSecurityGroupReferencesGroupId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeSecurityGroupReferences', 'describeSecurityGroupReferences', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeSecurityGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeSecurityGroups(null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeSecurityGroups', 'describeSecurityGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDescribeSnapshotAttributeSnapshotId = 'fakedata';
    const actionDescribeSnapshotAttributeAttribute = 'fakedata';
    describe('#describeSnapshotAttribute - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeSnapshotAttribute(actionDescribeSnapshotAttributeAttribute, actionDescribeSnapshotAttributeSnapshotId, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeSnapshotAttribute', 'describeSnapshotAttribute', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeSnapshots - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeSnapshots(null, null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeSnapshots', 'describeSnapshots', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeSpotDatafeedSubscription - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeSpotDatafeedSubscription(null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeSpotDatafeedSubscription', 'describeSpotDatafeedSubscription', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDescribeSpotFleetInstancesSpotFleetRequestId = 'fakedata';
    describe('#describeSpotFleetInstances - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeSpotFleetInstances(null, null, null, actionDescribeSpotFleetInstancesSpotFleetRequestId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeSpotFleetInstances', 'describeSpotFleetInstances', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDescribeSpotFleetRequestHistorySpotFleetRequestId = 'fakedata';
    const actionDescribeSpotFleetRequestHistoryStartTime = 'fakedata';
    describe('#describeSpotFleetRequestHistory - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeSpotFleetRequestHistory(null, null, null, null, actionDescribeSpotFleetRequestHistorySpotFleetRequestId, actionDescribeSpotFleetRequestHistoryStartTime, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeSpotFleetRequestHistory', 'describeSpotFleetRequestHistory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeSpotFleetRequests - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeSpotFleetRequests(null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeSpotFleetRequests', 'describeSpotFleetRequests', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeSpotInstanceRequests - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeSpotInstanceRequests(null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeSpotInstanceRequests', 'describeSpotInstanceRequests', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeSpotPriceHistory - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeSpotPriceHistory(null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeSpotPriceHistory', 'describeSpotPriceHistory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDescribeStaleSecurityGroupsVpcId = 'fakedata';
    describe('#describeStaleSecurityGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeStaleSecurityGroups(null, null, null, actionDescribeStaleSecurityGroupsVpcId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeStaleSecurityGroups', 'describeStaleSecurityGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeSubnets - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeSubnets(null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeSubnets', 'describeSubnets', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeTags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeTags(null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeTags', 'describeTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeTransitGatewayAttachments - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeTransitGatewayAttachments(null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeTransitGatewayAttachments', 'describeTransitGatewayAttachments', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeTransitGatewayRouteTables - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeTransitGatewayRouteTables(null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeTransitGatewayRouteTables', 'describeTransitGatewayRouteTables', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeTransitGatewayVpcAttachments - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeTransitGatewayVpcAttachments(null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeTransitGatewayVpcAttachments', 'describeTransitGatewayVpcAttachments', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeTransitGateways - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeTransitGateways(null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeTransitGateways', 'describeTransitGateways', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDescribeVolumeAttributeVolumeId = 'fakedata';
    const actionDescribeVolumeAttributeAttribute = 'fakedata';
    describe('#describeVolumeAttribute - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeVolumeAttribute(actionDescribeVolumeAttributeAttribute, actionDescribeVolumeAttributeVolumeId, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeVolumeAttribute', 'describeVolumeAttribute', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeVolumeStatus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeVolumeStatus(null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeVolumeStatus', 'describeVolumeStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeVolumes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeVolumes(null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeVolumes', 'describeVolumes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeVolumesModifications - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeVolumesModifications(null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeVolumesModifications', 'describeVolumesModifications', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDescribeVpcAttributeVpcId = 'fakedata';
    const actionDescribeVpcAttributeAttribute = 'fakedata';
    describe('#describeVpcAttribute - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeVpcAttribute(actionDescribeVpcAttributeAttribute, actionDescribeVpcAttributeVpcId, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeVpcAttribute', 'describeVpcAttribute', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeVpcClassicLink - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeVpcClassicLink(null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeVpcClassicLink', 'describeVpcClassicLink', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeVpcClassicLinkDnsSupport - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeVpcClassicLinkDnsSupport(null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeVpcClassicLinkDnsSupport', 'describeVpcClassicLinkDnsSupport', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeVpcEndpointConnectionNotifications - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeVpcEndpointConnectionNotifications(null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeVpcEndpointConnectionNotifications', 'describeVpcEndpointConnectionNotifications', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeVpcEndpointConnections - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeVpcEndpointConnections(null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeVpcEndpointConnections', 'describeVpcEndpointConnections', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeVpcEndpointServiceConfigurations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeVpcEndpointServiceConfigurations(null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeVpcEndpointServiceConfigurations', 'describeVpcEndpointServiceConfigurations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDescribeVpcEndpointServicePermissionsServiceId = 'fakedata';
    describe('#describeVpcEndpointServicePermissions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeVpcEndpointServicePermissions(null, actionDescribeVpcEndpointServicePermissionsServiceId, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeVpcEndpointServicePermissions', 'describeVpcEndpointServicePermissions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeVpcEndpointServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeVpcEndpointServices(null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeVpcEndpointServices', 'describeVpcEndpointServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeVpcEndpoints - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeVpcEndpoints(null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeVpcEndpoints', 'describeVpcEndpoints', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeVpcPeeringConnections - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeVpcPeeringConnections(null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeVpcPeeringConnections', 'describeVpcPeeringConnections', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeVpcs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeVpcs(null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
                assert.notEqual(undefined, data.response.DescribeVpcsResponse);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeVpcs', 'describeVpcs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    /*
    const actiondescribeVpcDetailsVpcId = 'fakedata';
    describe('#describeVpcDetails - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeVpcDetails(actiondescribeVpcDetailsVpcId, (data, error) => {
            try {
              runCommonAsserts(data[0], error);
              if (stub) {
                assert.equal('object', typeof data[0].response);
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    */

    /*
    describe('#getConfig - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getConfig(actiondescribeVpcDetailsVpcId, null, (data, error) => {
            try {
              runCommonAsserts(data[0], error);
              if (stub) {
                assert.equal('object', typeof data[0].response);
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    */

    /*
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          // start, limit, sort, order
          const options = {
            filter: {
              name: actiondescribeVpcDetailsVpcId
            }
          }
          a.getDevicesFiltered(options, null, (data, error) => {
            try {
              runCommonAsserts(data[0], error);
              if (stub) {
                assert.equal('object', typeof data[0].response);
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    */

    describe('#describeVpnConnections - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeVpnConnections(null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeVpnConnections', 'describeVpnConnections', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeVpnGateways - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.describeVpnGateways(null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDescribeVpnGateways', 'describeVpnGateways', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDetachClassicLinkVpcInstanceId = 'fakedata';
    const actionDetachClassicLinkVpcVpcId = 'fakedata';
    describe('#detachClassicLinkVpc - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.detachClassicLinkVpc(null, actionDetachClassicLinkVpcInstanceId, actionDetachClassicLinkVpcVpcId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDetachClassicLinkVpc', 'detachClassicLinkVpc', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDetachInternetGatewayInternetGatewayId = 'fakedata';
    const actionDetachInternetGatewayVpcId = 'fakedata';
    describe('#detachInternetGateway - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.detachInternetGateway(null, actionDetachInternetGatewayInternetGatewayId, actionDetachInternetGatewayVpcId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_ec2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDetachInternetGateway', 'detachInternetGateway', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDetachNetworkInterfaceAttachmentId = 'fakedata';
    describe('#detachNetworkInterface - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.detachNetworkInterface(actionDetachNetworkInterfaceAttachmentId, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_ec2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDetachNetworkInterface', 'detachNetworkInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDetachVolumeVolumeId = 'fakedata';
    describe('#detachVolume - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.detachVolume(null, null, null, actionDetachVolumeVolumeId, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDetachVolume', 'detachVolume', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDetachVpnGatewayVpcId = 'fakedata';
    const actionDetachVpnGatewayVpnGatewayId = 'fakedata';
    describe('#detachVpnGateway - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.detachVpnGateway(actionDetachVpnGatewayVpcId, actionDetachVpnGatewayVpnGatewayId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_ec2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDetachVpnGateway', 'detachVpnGateway', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDisableTransitGatewayRouteTablePropagationTransitGatewayRouteTableId = 'fakedata';
    const actionDisableTransitGatewayRouteTablePropagationTransitGatewayAttachmentId = 'fakedata';
    describe('#disableTransitGatewayRouteTablePropagation - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.disableTransitGatewayRouteTablePropagation(actionDisableTransitGatewayRouteTablePropagationTransitGatewayRouteTableId, actionDisableTransitGatewayRouteTablePropagationTransitGatewayAttachmentId, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDisableTransitGatewayRouteTablePropagation', 'disableTransitGatewayRouteTablePropagation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDisableVgwRoutePropagationGatewayId = 'fakedata';
    const actionDisableVgwRoutePropagationRouteTableId = 'fakedata';
    describe('#disableVgwRoutePropagation - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.disableVgwRoutePropagation(actionDisableVgwRoutePropagationGatewayId, actionDisableVgwRoutePropagationRouteTableId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_ec2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDisableVgwRoutePropagation', 'disableVgwRoutePropagation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDisableVpcClassicLinkVpcId = 'fakedata';
    describe('#disableVpcClassicLink - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.disableVpcClassicLink(null, actionDisableVpcClassicLinkVpcId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDisableVpcClassicLink', 'disableVpcClassicLink', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disableVpcClassicLinkDnsSupport - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.disableVpcClassicLinkDnsSupport(null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDisableVpcClassicLinkDnsSupport', 'disableVpcClassicLinkDnsSupport', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disassociateAddress - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.disassociateAddress(null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_ec2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDisassociateAddress', 'disassociateAddress', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDisassociateClientVpnTargetNetworkAssociationId = 'fakedata';
    const actionDisassociateClientVpnTargetNetworkClientVpnEndpointId = 'fakedata';
    describe('#disassociateClientVpnTargetNetwork - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.disassociateClientVpnTargetNetwork(actionDisassociateClientVpnTargetNetworkClientVpnEndpointId, actionDisassociateClientVpnTargetNetworkAssociationId, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDisassociateClientVpnTargetNetwork', 'disassociateClientVpnTargetNetwork', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDisassociateIamInstanceProfileAssociationId = 'fakedata';
    describe('#disassociateIamInstanceProfile - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.disassociateIamInstanceProfile(actionDisassociateIamInstanceProfileAssociationId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDisassociateIamInstanceProfile', 'disassociateIamInstanceProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDisassociateRouteTableAssociationId = 'fakedata';
    describe('#disassociateRouteTable - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.disassociateRouteTable(actionDisassociateRouteTableAssociationId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_ec2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDisassociateRouteTable', 'disassociateRouteTable', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDisassociateSubnetCidrBlockAssociationId = 'fakedata';
    describe('#disassociateSubnetCidrBlock - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.disassociateSubnetCidrBlock(actionDisassociateSubnetCidrBlockAssociationId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDisassociateSubnetCidrBlock', 'disassociateSubnetCidrBlock', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDisassociateTransitGatewayRouteTableTransitGatewayRouteTableId = 'fakedata';
    const actionDisassociateTransitGatewayRouteTableTransitGatewayAttachmentId = 'fakedata';
    describe('#disassociateTransitGatewayRouteTable - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.disassociateTransitGatewayRouteTable(actionDisassociateTransitGatewayRouteTableTransitGatewayRouteTableId, actionDisassociateTransitGatewayRouteTableTransitGatewayAttachmentId, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDisassociateTransitGatewayRouteTable', 'disassociateTransitGatewayRouteTable', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionDisassociateVpcCidrBlockAssociationId = 'fakedata';
    describe('#disassociateVpcCidrBlock - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.disassociateVpcCidrBlock(actionDisassociateVpcCidrBlockAssociationId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionDisassociateVpcCidrBlock', 'disassociateVpcCidrBlock', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionEnableTransitGatewayRouteTablePropagationTransitGatewayRouteTableId = 'fakedata';
    const actionEnableTransitGatewayRouteTablePropagationTransitGatewayAttachmentId = 'fakedata';
    describe('#enableTransitGatewayRouteTablePropagation - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enableTransitGatewayRouteTablePropagation(actionEnableTransitGatewayRouteTablePropagationTransitGatewayRouteTableId, actionEnableTransitGatewayRouteTablePropagationTransitGatewayAttachmentId, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionEnableTransitGatewayRouteTablePropagation', 'enableTransitGatewayRouteTablePropagation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionEnableVgwRoutePropagationGatewayId = 'fakedata';
    const actionEnableVgwRoutePropagationRouteTableId = 'fakedata';
    describe('#enableVgwRoutePropagation - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.enableVgwRoutePropagation(actionEnableVgwRoutePropagationGatewayId, actionEnableVgwRoutePropagationRouteTableId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_ec2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionEnableVgwRoutePropagation', 'enableVgwRoutePropagation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionEnableVolumeIOVolumeId = 'fakedata';
    describe('#enableVolumeIO - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.enableVolumeIO(null, actionEnableVolumeIOVolumeId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_ec2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionEnableVolumeIO', 'enableVolumeIO', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionEnableVpcClassicLinkVpcId = 'fakedata';
    describe('#enableVpcClassicLink - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enableVpcClassicLink(null, actionEnableVpcClassicLinkVpcId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionEnableVpcClassicLink', 'enableVpcClassicLink', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#enableVpcClassicLinkDnsSupport - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.enableVpcClassicLinkDnsSupport(null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionEnableVpcClassicLinkDnsSupport', 'enableVpcClassicLinkDnsSupport', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionExportClientVpnClientCertificateRevocationListClientVpnEndpointId = 'fakedata';
    describe('#exportClientVpnClientCertificateRevocationList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.exportClientVpnClientCertificateRevocationList(actionExportClientVpnClientCertificateRevocationListClientVpnEndpointId, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionExportClientVpnClientCertificateRevocationList', 'exportClientVpnClientCertificateRevocationList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionExportClientVpnClientConfigurationClientVpnEndpointId = 'fakedata';
    describe('#exportClientVpnClientConfiguration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.exportClientVpnClientConfiguration(actionExportClientVpnClientConfigurationClientVpnEndpointId, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionExportClientVpnClientConfiguration', 'exportClientVpnClientConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionExportTransitGatewayRoutesTransitGatewayRouteTableId = 'fakedata';
    const actionExportTransitGatewayRoutesS3Bucket = 'fakedata';
    describe('#exportTransitGatewayRoutes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.exportTransitGatewayRoutes(actionExportTransitGatewayRoutesTransitGatewayRouteTableId, null, actionExportTransitGatewayRoutesS3Bucket, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionExportTransitGatewayRoutes', 'exportTransitGatewayRoutes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionGetConsoleOutputInstanceId = 'fakedata';
    describe('#getConsoleOutput - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getConsoleOutput(actionGetConsoleOutputInstanceId, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionGetConsoleOutput', 'getConsoleOutput', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionGetConsoleScreenshotInstanceId = 'fakedata';
    describe('#getConsoleScreenshot - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getConsoleScreenshot(null, actionGetConsoleScreenshotInstanceId, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionGetConsoleScreenshot', 'getConsoleScreenshot', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionGetHostReservationPurchasePreviewHostIdSet = [];
    const actionGetHostReservationPurchasePreviewOfferingId = 'fakedata';
    describe('#getHostReservationPurchasePreview - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getHostReservationPurchasePreview(actionGetHostReservationPurchasePreviewHostIdSet, actionGetHostReservationPurchasePreviewOfferingId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionGetHostReservationPurchasePreview', 'getHostReservationPurchasePreview', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionGetLaunchTemplateDataInstanceId = 'fakedata';
    describe('#getLaunchTemplateData - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getLaunchTemplateData(null, actionGetLaunchTemplateDataInstanceId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionGetLaunchTemplateData', 'getLaunchTemplateData', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionGetPasswordDataInstanceId = 'fakedata';
    describe('#getPasswordData - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPasswordData(actionGetPasswordDataInstanceId, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionGetPasswordData', 'getPasswordData', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionGetReservedInstancesExchangeQuoteReservedInstanceId = [];
    describe('#getReservedInstancesExchangeQuote - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getReservedInstancesExchangeQuote(null, actionGetReservedInstancesExchangeQuoteReservedInstanceId, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionGetReservedInstancesExchangeQuote', 'getReservedInstancesExchangeQuote', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionGetTransitGatewayAttachmentPropagationsTransitGatewayAttachmentId = 'fakedata';
    describe('#getTransitGatewayAttachmentPropagations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTransitGatewayAttachmentPropagations(actionGetTransitGatewayAttachmentPropagationsTransitGatewayAttachmentId, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionGetTransitGatewayAttachmentPropagations', 'getTransitGatewayAttachmentPropagations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionGetTransitGatewayRouteTableAssociationsTransitGatewayRouteTableId = 'fakedata';
    describe('#getTransitGatewayRouteTableAssociations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTransitGatewayRouteTableAssociations(actionGetTransitGatewayRouteTableAssociationsTransitGatewayRouteTableId, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionGetTransitGatewayRouteTableAssociations', 'getTransitGatewayRouteTableAssociations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionGetTransitGatewayRouteTablePropagationsTransitGatewayRouteTableId = 'fakedata';
    describe('#getTransitGatewayRouteTablePropagations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTransitGatewayRouteTablePropagations(actionGetTransitGatewayRouteTablePropagationsTransitGatewayRouteTableId, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionGetTransitGatewayRouteTablePropagations', 'getTransitGatewayRouteTablePropagations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionImportClientVpnClientCertificateRevocationListClientVpnEndpointId = 'fakedata';
    const actionImportClientVpnClientCertificateRevocationListCertificateRevocationList = 'fakedata';
    describe('#importClientVpnClientCertificateRevocationList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.importClientVpnClientCertificateRevocationList(actionImportClientVpnClientCertificateRevocationListClientVpnEndpointId, actionImportClientVpnClientCertificateRevocationListCertificateRevocationList, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionImportClientVpnClientCertificateRevocationList', 'importClientVpnClientCertificateRevocationList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importImage - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.importImage(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionImportImage', 'importImage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionImportInstancePlatform = 'fakedata';
    describe('#importInstance - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.importInstance(null, null, null, null, null, null, null, null, null, null, null, null, null, null, actionImportInstancePlatform, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionImportInstance', 'importInstance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionImportKeyPairKeyName = 'fakedata';
    const actionImportKeyPairPublicKeyMaterial = 'fakedata';
    describe('#importKeyPair - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.importKeyPair(null, actionImportKeyPairKeyName, actionImportKeyPairPublicKeyMaterial, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionImportKeyPair', 'importKeyPair', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importSnapshot - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.importSnapshot(null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionImportSnapshot', 'importSnapshot', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionImportVolumeAvailabilityZone = 'fakedata';
    describe('#importVolume - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.importVolume(actionImportVolumeAvailabilityZone, null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionImportVolume', 'importVolume', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionModifyCapacityReservationCapacityReservationId = 'fakedata';
    describe('#modifyCapacityReservation - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.modifyCapacityReservation(actionModifyCapacityReservationCapacityReservationId, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionModifyCapacityReservation', 'modifyCapacityReservation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionModifyClientVpnEndpointClientVpnEndpointId = 'fakedata';
    describe('#modifyClientVpnEndpoint - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.modifyClientVpnEndpoint(actionModifyClientVpnEndpointClientVpnEndpointId, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionModifyClientVpnEndpoint', 'modifyClientVpnEndpoint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionModifyFleetFleetId = 'fakedata';
    describe('#modifyFleet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.modifyFleet(null, null, actionModifyFleetFleetId, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionModifyFleet', 'modifyFleet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionModifyFpgaImageAttributeFpgaImageId = 'fakedata';
    describe('#modifyFpgaImageAttribute - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.modifyFpgaImageAttribute(null, actionModifyFpgaImageAttributeFpgaImageId, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionModifyFpgaImageAttribute', 'modifyFpgaImageAttribute', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionModifyHostsAutoPlacement = 'fakedata';
    const actionModifyHostsHostId = [];
    describe('#modifyHosts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.modifyHosts(actionModifyHostsAutoPlacement, actionModifyHostsHostId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionModifyHosts', 'modifyHosts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionModifyIdFormatResource = 'fakedata';
    const actionModifyIdFormatUseLongIds = true;
    describe('#modifyIdFormat - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.modifyIdFormat(actionModifyIdFormatResource, actionModifyIdFormatUseLongIds, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_ec2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionModifyIdFormat', 'modifyIdFormat', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionModifyIdentityIdFormatPrincipalArn = 'fakedata';
    const actionModifyIdentityIdFormatResource = 'fakedata';
    const actionModifyIdentityIdFormatUseLongIds = true;
    describe('#modifyIdentityIdFormat - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.modifyIdentityIdFormat(actionModifyIdentityIdFormatPrincipalArn, actionModifyIdentityIdFormatResource, actionModifyIdentityIdFormatUseLongIds, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_ec2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionModifyIdentityIdFormat', 'modifyIdentityIdFormat', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionModifyImageAttributeImageId = 'fakedata';
    describe('#modifyImageAttribute - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.modifyImageAttribute(null, null, actionModifyImageAttributeImageId, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_ec2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionModifyImageAttribute', 'modifyImageAttribute', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionModifyInstanceAttributeInstanceId = 'fakedata';
    describe('#modifyInstanceAttribute - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.modifyInstanceAttribute(null, null, null, null, null, null, null, null, actionModifyInstanceAttributeInstanceId, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_ec2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionModifyInstanceAttribute', 'modifyInstanceAttribute', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionModifyInstanceCapacityReservationAttributesInstanceId = 'fakedata';
    describe('#modifyInstanceCapacityReservationAttributes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.modifyInstanceCapacityReservationAttributes(actionModifyInstanceCapacityReservationAttributesInstanceId, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionModifyInstanceCapacityReservationAttributes', 'modifyInstanceCapacityReservationAttributes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionModifyInstanceCreditSpecificationInstanceCreditSpecification = [];
    describe('#modifyInstanceCreditSpecification - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.modifyInstanceCreditSpecification(null, null, actionModifyInstanceCreditSpecificationInstanceCreditSpecification, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionModifyInstanceCreditSpecification', 'modifyInstanceCreditSpecification', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionModifyInstanceEventStartTimeInstanceId = 'fakedata';
    const actionModifyInstanceEventStartTimeInstanceEventId = 'fakedata';
    const actionModifyInstanceEventStartTimeNotBefore = 'fakedata';
    describe('#modifyInstanceEventStartTime - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.modifyInstanceEventStartTime(null, actionModifyInstanceEventStartTimeInstanceId, actionModifyInstanceEventStartTimeInstanceEventId, actionModifyInstanceEventStartTimeNotBefore, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionModifyInstanceEventStartTime', 'modifyInstanceEventStartTime', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionModifyInstancePlacementInstanceId = 'fakedata';
    describe('#modifyInstancePlacement - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.modifyInstancePlacement(null, null, null, actionModifyInstancePlacementInstanceId, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionModifyInstancePlacement', 'modifyInstancePlacement', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#modifyLaunchTemplate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.modifyLaunchTemplate(null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionModifyLaunchTemplate', 'modifyLaunchTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionModifyNetworkInterfaceAttributeNetworkInterfaceId = 'fakedata';
    describe('#modifyNetworkInterfaceAttribute - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.modifyNetworkInterfaceAttribute(null, null, null, null, null, actionModifyNetworkInterfaceAttributeNetworkInterfaceId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_ec2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionModifyNetworkInterfaceAttribute', 'modifyNetworkInterfaceAttribute', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionModifyReservedInstancesReservedInstancesId = [];
    const actionModifyReservedInstancesReservedInstancesConfigurationSetItemType = [];
    describe('#modifyReservedInstances - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.modifyReservedInstances(actionModifyReservedInstancesReservedInstancesId, null, actionModifyReservedInstancesReservedInstancesConfigurationSetItemType, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionModifyReservedInstances', 'modifyReservedInstances', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionModifySnapshotAttributeSnapshotId = 'fakedata';
    describe('#modifySnapshotAttribute - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.modifySnapshotAttribute(null, null, null, null, null, actionModifySnapshotAttributeSnapshotId, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_ec2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionModifySnapshotAttribute', 'modifySnapshotAttribute', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionModifySpotFleetRequestSpotFleetRequestId = 'fakedata';
    describe('#modifySpotFleetRequest - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.modifySpotFleetRequest(null, actionModifySpotFleetRequestSpotFleetRequestId, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionModifySpotFleetRequest', 'modifySpotFleetRequest', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionModifySubnetAttributeSubnetId = 'fakedata';
    describe('#modifySubnetAttribute - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.modifySubnetAttribute(null, null, actionModifySubnetAttributeSubnetId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_ec2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionModifySubnetAttribute', 'modifySubnetAttribute', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionModifyTransitGatewayVpcAttachmentTransitGatewayAttachmentId = 'fakedata';
    describe('#modifyTransitGatewayVpcAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.modifyTransitGatewayVpcAttachment(actionModifyTransitGatewayVpcAttachmentTransitGatewayAttachmentId, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionModifyTransitGatewayVpcAttachment', 'modifyTransitGatewayVpcAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionModifyVolumeVolumeId = 'fakedata';
    describe('#modifyVolume - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.modifyVolume(null, actionModifyVolumeVolumeId, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionModifyVolume', 'modifyVolume', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionModifyVolumeAttributeVolumeId = 'fakedata';
    describe('#modifyVolumeAttribute - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.modifyVolumeAttribute(null, actionModifyVolumeAttributeVolumeId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_ec2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionModifyVolumeAttribute', 'modifyVolumeAttribute', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionModifyVpcAttributeVpcId = 'fakedata';
    describe('#modifyVpcAttribute - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.modifyVpcAttribute(null, null, actionModifyVpcAttributeVpcId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_ec2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionModifyVpcAttribute', 'modifyVpcAttribute', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionModifyVpcEndpointVpcEndpointId = 'fakedata';
    describe('#modifyVpcEndpoint - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.modifyVpcEndpoint(null, actionModifyVpcEndpointVpcEndpointId, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionModifyVpcEndpoint', 'modifyVpcEndpoint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionModifyVpcEndpointConnectionNotificationConnectionNotificationId = 'fakedata';
    describe('#modifyVpcEndpointConnectionNotification - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.modifyVpcEndpointConnectionNotification(null, actionModifyVpcEndpointConnectionNotificationConnectionNotificationId, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionModifyVpcEndpointConnectionNotification', 'modifyVpcEndpointConnectionNotification', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionModifyVpcEndpointServiceConfigurationServiceId = 'fakedata';
    describe('#modifyVpcEndpointServiceConfiguration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.modifyVpcEndpointServiceConfiguration(null, actionModifyVpcEndpointServiceConfigurationServiceId, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionModifyVpcEndpointServiceConfiguration', 'modifyVpcEndpointServiceConfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionModifyVpcEndpointServicePermissionsServiceId = 'fakedata';
    describe('#modifyVpcEndpointServicePermissions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.modifyVpcEndpointServicePermissions(null, actionModifyVpcEndpointServicePermissionsServiceId, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionModifyVpcEndpointServicePermissions', 'modifyVpcEndpointServicePermissions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionModifyVpcPeeringConnectionOptionsVpcPeeringConnectionId = 'fakedata';
    describe('#modifyVpcPeeringConnectionOptions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.modifyVpcPeeringConnectionOptions(null, null, null, null, null, null, null, actionModifyVpcPeeringConnectionOptionsVpcPeeringConnectionId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionModifyVpcPeeringConnectionOptions', 'modifyVpcPeeringConnectionOptions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionModifyVpcTenancyVpcId = 'fakedata';
    const actionModifyVpcTenancyInstanceTenancy = 'fakedata';
    describe('#modifyVpcTenancy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.modifyVpcTenancy(actionModifyVpcTenancyVpcId, actionModifyVpcTenancyInstanceTenancy, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionModifyVpcTenancy', 'modifyVpcTenancy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionModifyVpnConnectionVpnConnectionId = 'fakedata';
    describe('#modifyVpnConnection - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.modifyVpnConnection(actionModifyVpnConnectionVpnConnectionId, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionModifyVpnConnection', 'modifyVpnConnection', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionMonitorInstancesInstanceId = [];
    describe('#monitorInstances - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.monitorInstances(actionMonitorInstancesInstanceId, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionMonitorInstances', 'monitorInstances', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionMoveAddressToVpcPublicIp = 'fakedata';
    describe('#moveAddressToVpc - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.moveAddressToVpc(null, actionMoveAddressToVpcPublicIp, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionMoveAddressToVpc', 'moveAddressToVpc', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionProvisionByoipCidrCidr = 'fakedata';
    describe('#provisionByoipCidr - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.provisionByoipCidr(actionProvisionByoipCidrCidr, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionProvisionByoipCidr', 'provisionByoipCidr', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionPurchaseHostReservationHostIdSet = [];
    const actionPurchaseHostReservationOfferingId = 'fakedata';
    describe('#purchaseHostReservation - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.purchaseHostReservation(null, null, actionPurchaseHostReservationHostIdSet, null, actionPurchaseHostReservationOfferingId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionPurchaseHostReservation', 'purchaseHostReservation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionPurchaseReservedInstancesOfferingInstanceCount = 555;
    const actionPurchaseReservedInstancesOfferingReservedInstancesOfferingId = 'fakedata';
    describe('#purchaseReservedInstancesOffering - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.purchaseReservedInstancesOffering(actionPurchaseReservedInstancesOfferingInstanceCount, actionPurchaseReservedInstancesOfferingReservedInstancesOfferingId, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionPurchaseReservedInstancesOffering', 'purchaseReservedInstancesOffering', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionPurchaseScheduledInstancesPurchaseRequest = [];
    describe('#purchaseScheduledInstances - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.purchaseScheduledInstances(null, null, actionPurchaseScheduledInstancesPurchaseRequest, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionPurchaseScheduledInstances', 'purchaseScheduledInstances', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionRebootInstancesInstanceId = [];
    describe('#rebootInstances - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rebootInstances(actionRebootInstancesInstanceId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_ec2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionRebootInstances', 'rebootInstances', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionRegisterImageName = 'fakedata';
    describe('#registerImage - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.registerImage(null, null, null, null, null, null, null, actionRegisterImageName, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionRegisterImage', 'registerImage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionRejectTransitGatewayVpcAttachmentTransitGatewayAttachmentId = 'fakedata';
    describe('#rejectTransitGatewayVpcAttachment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rejectTransitGatewayVpcAttachment(actionRejectTransitGatewayVpcAttachmentTransitGatewayAttachmentId, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionRejectTransitGatewayVpcAttachment', 'rejectTransitGatewayVpcAttachment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionRejectVpcEndpointConnectionsServiceId = 'fakedata';
    const actionRejectVpcEndpointConnectionsVpcEndpointId = [];
    describe('#rejectVpcEndpointConnections - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rejectVpcEndpointConnections(null, actionRejectVpcEndpointConnectionsServiceId, actionRejectVpcEndpointConnectionsVpcEndpointId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionRejectVpcEndpointConnections', 'rejectVpcEndpointConnections', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionRejectVpcPeeringConnectionVpcPeeringConnectionId = 'fakedata';
    describe('#rejectVpcPeeringConnection - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rejectVpcPeeringConnection(null, actionRejectVpcPeeringConnectionVpcPeeringConnectionId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionRejectVpcPeeringConnection', 'rejectVpcPeeringConnection', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#releaseAddress - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.releaseAddress(null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_ec2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionReleaseAddress', 'releaseAddress', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionReleaseHostsHostId = [];
    describe('#releaseHosts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.releaseHosts(actionReleaseHostsHostId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionReleaseHosts', 'releaseHosts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionReplaceIamInstanceProfileAssociationAssociationId = 'fakedata';
    describe('#replaceIamInstanceProfileAssociation - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.replaceIamInstanceProfileAssociation(null, null, actionReplaceIamInstanceProfileAssociationAssociationId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionReplaceIamInstanceProfileAssociation', 'replaceIamInstanceProfileAssociation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionReplaceNetworkAclAssociationAssociationId = 'fakedata';
    const actionReplaceNetworkAclAssociationNetworkAclId = 'fakedata';
    describe('#replaceNetworkAclAssociation - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.replaceNetworkAclAssociation(actionReplaceNetworkAclAssociationAssociationId, null, actionReplaceNetworkAclAssociationNetworkAclId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionReplaceNetworkAclAssociation', 'replaceNetworkAclAssociation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionReplaceNetworkAclEntryEgress = true;
    const actionReplaceNetworkAclEntryNetworkAclId = 'fakedata';
    const actionReplaceNetworkAclEntryProtocol = 'fakedata';
    const actionReplaceNetworkAclEntryRuleAction = 'fakedata';
    const actionReplaceNetworkAclEntryRuleNumber = 555;
    describe('#replaceNetworkAclEntry - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.replaceNetworkAclEntry(null, null, actionReplaceNetworkAclEntryEgress, null, null, null, actionReplaceNetworkAclEntryNetworkAclId, null, null, actionReplaceNetworkAclEntryProtocol, actionReplaceNetworkAclEntryRuleAction, actionReplaceNetworkAclEntryRuleNumber, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_ec2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionReplaceNetworkAclEntry', 'replaceNetworkAclEntry', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionReplaceRouteRouteTableId = 'fakedata';
    describe('#replaceRoute - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.replaceRoute(null, null, null, null, null, null, null, null, null, actionReplaceRouteRouteTableId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_ec2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionReplaceRoute', 'replaceRoute', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionReplaceRouteTableAssociationAssociationId = 'fakedata';
    const actionReplaceRouteTableAssociationRouteTableId = 'fakedata';
    describe('#replaceRouteTableAssociation - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.replaceRouteTableAssociation(actionReplaceRouteTableAssociationAssociationId, null, actionReplaceRouteTableAssociationRouteTableId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionReplaceRouteTableAssociation', 'replaceRouteTableAssociation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionReplaceTransitGatewayRouteDestinationCidrBlock = 'fakedata';
    const actionReplaceTransitGatewayRouteTransitGatewayRouteTableId = 'fakedata';
    describe('#replaceTransitGatewayRoute - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.replaceTransitGatewayRoute(actionReplaceTransitGatewayRouteDestinationCidrBlock, actionReplaceTransitGatewayRouteTransitGatewayRouteTableId, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionReplaceTransitGatewayRoute', 'replaceTransitGatewayRoute', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionReportInstanceStatusInstanceId = [];
    const actionReportInstanceStatusReasonCode = [];
    const actionReportInstanceStatusStatus = 'fakedata';
    describe('#reportInstanceStatus - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.reportInstanceStatus(null, null, null, actionReportInstanceStatusInstanceId, actionReportInstanceStatusReasonCode, null, actionReportInstanceStatusStatus, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_ec2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionReportInstanceStatus', 'reportInstanceStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#requestSpotFleet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.requestSpotFleet(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionRequestSpotFleet', 'requestSpotFleet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#requestSpotInstances - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.requestSpotInstances(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionRequestSpotInstances', 'requestSpotInstances', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionResetFpgaImageAttributeFpgaImageId = 'fakedata';
    describe('#resetFpgaImageAttribute - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.resetFpgaImageAttribute(null, actionResetFpgaImageAttributeFpgaImageId, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionResetFpgaImageAttribute', 'resetFpgaImageAttribute', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionResetImageAttributeAttribute = 'fakedata';
    const actionResetImageAttributeImageId = 'fakedata';
    describe('#resetImageAttribute - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.resetImageAttribute(actionResetImageAttributeAttribute, actionResetImageAttributeImageId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_ec2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionResetImageAttribute', 'resetImageAttribute', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionResetInstanceAttributeAttribute = 'fakedata';
    const actionResetInstanceAttributeInstanceId = 'fakedata';
    describe('#resetInstanceAttribute - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.resetInstanceAttribute(actionResetInstanceAttributeAttribute, null, actionResetInstanceAttributeInstanceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_ec2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionResetInstanceAttribute', 'resetInstanceAttribute', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionResetNetworkInterfaceAttributeNetworkInterfaceId = 'fakedata';
    describe('#resetNetworkInterfaceAttribute - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.resetNetworkInterfaceAttribute(null, actionResetNetworkInterfaceAttributeNetworkInterfaceId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_ec2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionResetNetworkInterfaceAttribute', 'resetNetworkInterfaceAttribute', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionResetSnapshotAttributeAttribute = 'fakedata';
    const actionResetSnapshotAttributeSnapshotId = 'fakedata';
    describe('#resetSnapshotAttribute - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.resetSnapshotAttribute(actionResetSnapshotAttributeAttribute, actionResetSnapshotAttributeSnapshotId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_ec2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionResetSnapshotAttribute', 'resetSnapshotAttribute', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionRestoreAddressToClassicPublicIp = 'fakedata';
    describe('#restoreAddressToClassic - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.restoreAddressToClassic(null, actionRestoreAddressToClassicPublicIp, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionRestoreAddressToClassic', 'restoreAddressToClassic', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionRevokeClientVpnIngressClientVpnEndpointId = 'fakedata';
    const actionRevokeClientVpnIngressTargetNetworkCidr = 'fakedata';
    describe('#revokeClientVpnIngress - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.revokeClientVpnIngress(actionRevokeClientVpnIngressClientVpnEndpointId, actionRevokeClientVpnIngressTargetNetworkCidr, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionRevokeClientVpnIngress', 'revokeClientVpnIngress', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionRevokeSecurityGroupEgressGroupId = 'fakedata';
    describe('#revokeSecurityGroupEgress - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.revokeSecurityGroupEgress(null, actionRevokeSecurityGroupEgressGroupId, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_ec2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionRevokeSecurityGroupEgress', 'revokeSecurityGroupEgress', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#revokeSecurityGroupIngress - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.revokeSecurityGroupIngress(null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_ec2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionRevokeSecurityGroupIngress', 'revokeSecurityGroupIngress', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionRunInstancesMaxCount = 555;
    const actionRunInstancesMinCount = 555;
    describe('#runInstances - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.runInstances(null, null, null, null, null, null, null, actionRunInstancesMaxCount, actionRunInstancesMinCount, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionRunInstances', 'runInstances', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionRunScheduledInstancesScheduledInstanceId = 'fakedata';
    describe('#runScheduledInstances - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.runScheduledInstances(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, actionRunScheduledInstancesScheduledInstanceId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionRunScheduledInstances', 'runScheduledInstances', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionSearchTransitGatewayRoutesTransitGatewayRouteTableId = 'fakedata';
    const actionSearchTransitGatewayRoutesFilter = [];
    describe('#searchTransitGatewayRoutes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.searchTransitGatewayRoutes(actionSearchTransitGatewayRoutesTransitGatewayRouteTableId, actionSearchTransitGatewayRoutesFilter, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionSearchTransitGatewayRoutes', 'searchTransitGatewayRoutes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionStartInstancesInstanceId = [];
    describe('#startInstances - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.startInstances(actionStartInstancesInstanceId, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionStartInstances', 'startInstances', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionStopInstancesInstanceId = [];
    describe('#stopInstances - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.stopInstances(actionStopInstancesInstanceId, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionStopInstances', 'stopInstances', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionTerminateClientVpnConnectionsClientVpnEndpointId = 'fakedata';
    describe('#terminateClientVpnConnections - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.terminateClientVpnConnections(actionTerminateClientVpnConnectionsClientVpnEndpointId, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionTerminateClientVpnConnections', 'terminateClientVpnConnections', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionTerminateInstancesInstanceId = [];
    describe('#terminateInstances - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.terminateInstances(actionTerminateInstancesInstanceId, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionTerminateInstances', 'terminateInstances', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionUnassignIpv6AddressesNetworkInterfaceId = 'fakedata';
    const actionUnassignIpv6AddressesIpv6Addresses = [];
    describe('#unassignIpv6Addresses - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.unassignIpv6Addresses(actionUnassignIpv6AddressesIpv6Addresses, actionUnassignIpv6AddressesNetworkInterfaceId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionUnassignIpv6Addresses', 'unassignIpv6Addresses', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionUnassignPrivateIpAddressesNetworkInterfaceId = 'fakedata';
    const actionUnassignPrivateIpAddressesPrivateIpAddress = [];
    describe('#unassignPrivateIpAddresses - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.unassignPrivateIpAddresses(actionUnassignPrivateIpAddressesNetworkInterfaceId, actionUnassignPrivateIpAddressesPrivateIpAddress, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-aws_ec2-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionUnassignPrivateIpAddresses', 'unassignPrivateIpAddresses', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionUnmonitorInstancesInstanceId = [];
    describe('#unmonitorInstances - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.unmonitorInstances(actionUnmonitorInstancesInstanceId, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionUnmonitorInstances', 'unmonitorInstances', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionUpdateSecurityGroupRuleDescriptionsEgressIpPermissions = [];
    describe('#updateSecurityGroupRuleDescriptionsEgress - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateSecurityGroupRuleDescriptionsEgress(null, null, null, actionUpdateSecurityGroupRuleDescriptionsEgressIpPermissions, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionUpdateSecurityGroupRuleDescriptionsEgress', 'updateSecurityGroupRuleDescriptionsEgress', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionUpdateSecurityGroupRuleDescriptionsIngressIpPermissions = ['fakevalue1', 'fakevalue2'];
    describe('#updateSecurityGroupRuleDescriptionsIngress - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateSecurityGroupRuleDescriptionsIngress(null, null, null, actionUpdateSecurityGroupRuleDescriptionsIngressIpPermissions, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionUpdateSecurityGroupRuleDescriptionsIngress', 'updateSecurityGroupRuleDescriptionsIngress', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionWithdrawByoipCidrCidr = 'fakedata';
    describe('#withdrawByoipCidr - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.withdrawByoipCidr(actionWithdrawByoipCidrCidr, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActionWithdrawByoipCidr', 'withdrawByoipCidr', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
