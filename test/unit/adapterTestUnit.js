/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint global-require: warn */
/* eslint no-unused-vars: warn */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const path = require('path');
const util = require('util');
const execute = require('child_process').execSync;
const fs = require('fs-extra');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');
const Ajv = require('ajv');

const ajv = new Ajv({ strictSchema: false, allErrors: true, allowUnionTypes: true });
const anything = td.matchers.anything();
let logLevel = 'none';
const isRapidFail = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;
samProps.host = 'replace.hostorip.here';
samProps.authentication.username = 'username';
samProps.authentication.password = 'password';
samProps.protocol = 'http';
samProps.port = 80;
samProps.ssl.enabled = false;
samProps.ssl.accept_invalid_cert = false;
samProps.request.attempt_timeout = 1200000;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-aws_ec2',
      type: 'Awsec2',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

// require the adapter that we are going to be using
const Awsec2 = require('../../adapter');

// delete the .DS_Store directory in entities -- otherwise this will cause errors
const dirPath = path.join(__dirname, '../../entities/.DS_Store');
if (fs.existsSync(dirPath)) {
  try {
    fs.removeSync(dirPath);
    console.log('.DS_Store deleted');
  } catch (e) {
    console.log('Error when deleting .DS_Store:', e);
  }
}

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[unit] Awsec2 Adapter Test', () => {
  describe('Awsec2 Class Tests', () => {
    const a = new Awsec2(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('adapterBase.js', () => {
      it('should have an adapterBase.js', (done) => {
        try {
          fs.exists('adapterBase.js', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    let wffunctions = [];
    describe('#iapGetAdapterWorkflowFunctions', () => {
      it('should retrieve workflow functions', (done) => {
        try {
          wffunctions = a.iapGetAdapterWorkflowFunctions([]);

          try {
            assert.notEqual(0, wffunctions.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('package.json', () => {
      it('should have a package.json', (done) => {
        try {
          fs.exists('package.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json should be validated', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          // Define the JSON schema for package.json
          const packageJsonSchema = {
            type: 'object',
            properties: {
              name: { type: 'string' },
              version: { type: 'string' }
              // May need to add more properties as needed
            },
            required: ['name', 'version']
          };
          const validate = ajv.compile(packageJsonSchema);
          const isValid = validate(packageDotJson);

          if (isValid === false) {
            log.error('The package.json contains errors');
            assert.equal(true, isValid);
          } else {
            assert.equal(true, isValid);
          }

          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json standard fields should be customized', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(-1, packageDotJson.name.indexOf('aws_ec2'));
          assert.notEqual(undefined, packageDotJson.version);
          assert.notEqual(null, packageDotJson.version);
          assert.notEqual('', packageDotJson.version);
          assert.notEqual(undefined, packageDotJson.description);
          assert.notEqual(null, packageDotJson.description);
          assert.notEqual('', packageDotJson.description);
          assert.equal('adapter.js', packageDotJson.main);
          assert.notEqual(undefined, packageDotJson.wizardVersion);
          assert.notEqual(null, packageDotJson.wizardVersion);
          assert.notEqual('', packageDotJson.wizardVersion);
          assert.notEqual(undefined, packageDotJson.engineVersion);
          assert.notEqual(null, packageDotJson.engineVersion);
          assert.notEqual('', packageDotJson.engineVersion);
          assert.equal('http', packageDotJson.adapterType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper scripts should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.scripts);
          assert.notEqual(null, packageDotJson.scripts);
          assert.notEqual('', packageDotJson.scripts);
          assert.equal('node utils/setup.js', packageDotJson.scripts.preinstall);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js', packageDotJson.scripts.lint);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js --quiet', packageDotJson.scripts['lint:errors']);
          assert.equal('mocha test/unit/adapterBaseTestUnit.js --LOG=error', packageDotJson.scripts['test:baseunit']);
          assert.equal('mocha test/unit/adapterTestUnit.js --LOG=error', packageDotJson.scripts['test:unit']);
          assert.equal('mocha test/integration/adapterTestIntegration.js --LOG=error', packageDotJson.scripts['test:integration']);
          assert.equal('npm run test:baseunit && npm run test:unit && npm run test:integration', packageDotJson.scripts.test);
          assert.equal('npm publish --registry=https://registry.npmjs.org --access=public', packageDotJson.scripts.deploy);
          assert.equal('npm run deploy', packageDotJson.scripts.build);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper directories should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.repository);
          assert.notEqual(null, packageDotJson.repository);
          assert.notEqual('', packageDotJson.repository);
          assert.equal('git', packageDotJson.repository.type);
          assert.equal('git@gitlab.com:itentialopensource/adapters/', packageDotJson.repository.url.substring(0, 43));
          assert.equal('https://gitlab.com/itentialopensource/adapters/', packageDotJson.homepage.substring(0, 47));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.dependencies);
          assert.notEqual(null, packageDotJson.dependencies);
          assert.notEqual('', packageDotJson.dependencies);
          assert.equal('^8.17.1', packageDotJson.dependencies.ajv);
          assert.equal('^1.7.9', packageDotJson.dependencies.axios);
          assert.equal('^11.0.0', packageDotJson.dependencies.commander);
          assert.equal('^11.2.0', packageDotJson.dependencies['fs-extra']);
          assert.equal('^10.8.2', packageDotJson.dependencies.mocha);
          assert.equal('^2.0.1', packageDotJson.dependencies['mocha-param']);
          assert.equal('^0.4.4', packageDotJson.dependencies.ping);
          assert.equal('^1.4.10', packageDotJson.dependencies['readline-sync']);
          assert.equal('^7.6.3', packageDotJson.dependencies.semver);
          assert.equal('^3.17.0', packageDotJson.dependencies.winston);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dev dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.devDependencies);
          assert.notEqual(null, packageDotJson.devDependencies);
          assert.notEqual('', packageDotJson.devDependencies);
          assert.equal('^4.3.7', packageDotJson.devDependencies.chai);
          assert.equal('^8.44.0', packageDotJson.devDependencies.eslint);
          assert.equal('^15.0.0', packageDotJson.devDependencies['eslint-config-airbnb-base']);
          assert.equal('^2.27.5', packageDotJson.devDependencies['eslint-plugin-import']);
          assert.equal('^3.1.0', packageDotJson.devDependencies['eslint-plugin-json']);
          assert.equal('^3.18.0', packageDotJson.devDependencies.testdouble);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('pronghorn.json', () => {
      it('should have a pronghorn.json', (done) => {
        try {
          fs.exists('pronghorn.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should be customized', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(-1, pronghornDotJson.id.indexOf('aws_ec2'));
          assert.equal('Adapter', pronghornDotJson.type);
          assert.equal('Awsec2', pronghornDotJson.export);
          assert.equal('Awsec2', pronghornDotJson.title);
          assert.equal('adapter.js', pronghornDotJson.src);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should contain generic adapter methods', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(undefined, pronghornDotJson.methods);
          assert.notEqual(null, pronghornDotJson.methods);
          assert.notEqual('', pronghornDotJson.methods);
          assert.equal(true, Array.isArray(pronghornDotJson.methods));
          assert.notEqual(0, pronghornDotJson.methods.length);
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUpdateAdapterConfiguration'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapSuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUnsuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterQueue'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapFindAdapterPath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapTroubleshootAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterHealthcheck'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterConnectivity'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterBasicGet'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapMoveAdapterEntitiesToDB'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapDeactivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapActivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapPopulateEntityCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRetrieveEntitiesCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevice'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevicesFiltered'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'isAlive'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getConfig'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetDeviceCount'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapExpandedGenericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequestNoBasePath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterLint'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterTests'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterInventory'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should only expose workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');

          for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
            let found = false;
            let paramissue = false;

            for (let w = 0; w < wffunctions.length; w += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                const methLine = execute(`grep "  ${wffunctions[w]}(" adapter.js | grep "callback) {"`).toString();
                let wfparams = [];

                if (methLine && methLine.indexOf('(') >= 0 && methLine.indexOf(')') >= 0) {
                  const temp = methLine.substring(methLine.indexOf('(') + 1, methLine.lastIndexOf(')'));
                  wfparams = temp.split(',');

                  for (let t = 0; t < wfparams.length; t += 1) {
                    // remove default value from the parameter name
                    wfparams[t] = wfparams[t].substring(0, wfparams[t].search(/=/) > 0 ? wfparams[t].search(/#|\?|=/) : wfparams[t].length);
                    // remove spaces
                    wfparams[t] = wfparams[t].trim();

                    if (wfparams[t] === 'callback') {
                      wfparams.splice(t, 1);
                    }
                  }
                }

                // if there are inputs defined but not on the method line
                if (wfparams.length === 0 && (pronghornDotJson.methods[m].input
                    && pronghornDotJson.methods[m].input.length > 0)) {
                  paramissue = true;
                } else if (wfparams.length > 0 && (!pronghornDotJson.methods[m].input
                    || pronghornDotJson.methods[m].input.length === 0)) {
                  // if there are no inputs defined but there are on the method line
                  paramissue = true;
                } else {
                  for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                    let pfound = false;
                    for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                  for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                    let pfound = false;
                    for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                }

                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} not found in workflow functions`);
            }
            if (paramissue) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} has a parameter mismatch`);
            }
            assert.equal(true, found);
            assert.equal(false, paramissue);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('pronghorn.json should expose all workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          for (let w = 0; w < wffunctions.length; w += 1) {
            let found = false;

            for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${wffunctions[w]} not found in pronghorn.json`);
            }
            assert.equal(true, found);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json verify input/output schema objects', (done) => {
        const verifySchema = (methodName, schema) => {
          try {
            ajv.compile(schema);
          } catch (error) {
            const errorMessage = `Invalid schema found in '${methodName}' method.
          Schema => ${JSON.stringify(schema)}.
          Details => ${error.message}`;
            throw new Error(errorMessage);
          }
        };

        try {
          const pronghornDotJson = require('../../pronghorn.json');
          const { methods } = pronghornDotJson;
          for (let i = 0; i < methods.length; i += 1) {
            for (let j = 0; j < methods[i].input.length; j += 1) {
              const inputSchema = methods[i].input[j].schema;
              if (inputSchema) {
                verifySchema(methods[i].name, inputSchema);
              }
            }
            const outputSchema = methods[i].output.schema;
            if (outputSchema) {
              verifySchema(methods[i].name, outputSchema);
            }
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('propertiesSchema.json', () => {
      it('should have a propertiesSchema.json', (done) => {
        try {
          fs.exists('propertiesSchema.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should be customized', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.equal('adapter-aws_ec2', propertiesDotJson.$id);
          assert.equal('object', propertiesDotJson.type);
          assert.equal('http://json-schema.org/draft-07/schema#', propertiesDotJson.$schema);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should contain generic adapter properties', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.notEqual(undefined, propertiesDotJson.properties);
          assert.notEqual(null, propertiesDotJson.properties);
          assert.notEqual('', propertiesDotJson.properties);
          assert.equal('string', propertiesDotJson.properties.host.type);
          assert.equal('integer', propertiesDotJson.properties.port.type);
          assert.equal('boolean', propertiesDotJson.properties.stub.type);
          assert.equal('string', propertiesDotJson.properties.protocol.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.authentication);
          assert.notEqual(null, propertiesDotJson.definitions.authentication);
          assert.notEqual('', propertiesDotJson.definitions.authentication);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.auth_method.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.invalid_token_error.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.token_timeout.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token_cache.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field.type));
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field_format.type));
          assert.equal('boolean', propertiesDotJson.definitions.authentication.properties.auth_logging.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_id.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_secret.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.grant_type.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.ssl);
          assert.notEqual(null, propertiesDotJson.definitions.ssl);
          assert.notEqual('', propertiesDotJson.definitions.ssl);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ecdhCurve.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.cert_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.secure_protocol.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ciphers.type);
          assert.equal('string', propertiesDotJson.properties.base_path.type);
          assert.equal('string', propertiesDotJson.properties.version.type);
          assert.equal('string', propertiesDotJson.properties.cache_location.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_pathvars.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_queryvars.type);
          assert.equal(true, Array.isArray(propertiesDotJson.properties.save_metric.type));
          assert.notEqual(undefined, propertiesDotJson.definitions);
          assert.notEqual(null, propertiesDotJson.definitions);
          assert.notEqual('', propertiesDotJson.definitions);
          assert.notEqual(undefined, propertiesDotJson.definitions.healthcheck);
          assert.notEqual(null, propertiesDotJson.definitions.healthcheck);
          assert.notEqual('', propertiesDotJson.definitions.healthcheck);
          assert.equal('string', propertiesDotJson.definitions.healthcheck.properties.type.type);
          assert.equal('integer', propertiesDotJson.definitions.healthcheck.properties.frequency.type);
          assert.equal('object', propertiesDotJson.definitions.healthcheck.properties.query_object.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.throttle);
          assert.notEqual(null, propertiesDotJson.definitions.throttle);
          assert.notEqual('', propertiesDotJson.definitions.throttle);
          assert.equal('boolean', propertiesDotJson.definitions.throttle.properties.throttle_enabled.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.number_pronghorns.type);
          assert.equal('string', propertiesDotJson.definitions.throttle.properties.sync_async.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.max_in_queue.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.concurrent_max.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.expire_timeout.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.avg_runtime.type);
          assert.equal('array', propertiesDotJson.definitions.throttle.properties.priorities.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.request);
          assert.notEqual(null, propertiesDotJson.definitions.request);
          assert.notEqual('', propertiesDotJson.definitions.request);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_redirects.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_retries.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.request.properties.limit_retry_error.type));
          assert.equal('array', propertiesDotJson.definitions.request.properties.failover_codes.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.attempt_timeout.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.payload.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.uriOptions.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.addlHeaders.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.authData.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.healthcheck_on_timeout.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_raw.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.archiving.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_request.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.proxy);
          assert.notEqual(null, propertiesDotJson.definitions.proxy);
          assert.notEqual('', propertiesDotJson.definitions.proxy);
          assert.equal('boolean', propertiesDotJson.definitions.proxy.properties.enabled.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.proxy.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.protocol.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.password.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.mongo);
          assert.notEqual(null, propertiesDotJson.definitions.mongo);
          assert.notEqual('', propertiesDotJson.definitions.mongo);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.mongo.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.database.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.replSet.type);
          assert.equal('object', propertiesDotJson.definitions.mongo.properties.db_ssl.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.cert_file.type);
          assert.notEqual('', propertiesDotJson.definitions.devicebroker);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevice.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevicesFiltered.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.isAlive.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getConfig.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getCount.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('error.json', () => {
      it('should have an error.json', (done) => {
        try {
          fs.exists('error.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error.json should have standard adapter errors', (done) => {
        try {
          const errorDotJson = require('../../error.json');
          assert.notEqual(undefined, errorDotJson.errors);
          assert.notEqual(null, errorDotJson.errors);
          assert.notEqual('', errorDotJson.errors);
          assert.equal(true, Array.isArray(errorDotJson.errors));
          assert.notEqual(0, errorDotJson.errors.length);
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.100'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.101'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.102'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.110'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.111'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.112'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.113'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.114'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.115'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.116'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.300'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.301'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.302'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.303'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.304'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.305'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.310'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.311'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.312'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.320'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.321'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.400'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.401'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.402'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.500'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.501'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.502'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.503'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.600'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.900'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('sampleProperties.json', () => {
      it('should have a sampleProperties.json', (done) => {
        try {
          fs.exists('sampleProperties.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('sampleProperties.json should contain generic adapter properties', (done) => {
        try {
          const sampleDotJson = require('../../sampleProperties.json');
          assert.notEqual(-1, sampleDotJson.id.indexOf('aws_ec2'));
          assert.equal('Awsec2', sampleDotJson.type);
          assert.notEqual(undefined, sampleDotJson.properties);
          assert.notEqual(null, sampleDotJson.properties);
          assert.notEqual('', sampleDotJson.properties);
          assert.notEqual(undefined, sampleDotJson.properties.host);
          assert.notEqual(undefined, sampleDotJson.properties.port);
          assert.notEqual(undefined, sampleDotJson.properties.stub);
          assert.notEqual(undefined, sampleDotJson.properties.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.authentication);
          assert.notEqual(null, sampleDotJson.properties.authentication);
          assert.notEqual('', sampleDotJson.properties.authentication);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_method);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.username);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.password);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.invalid_token_error);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_cache);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field_format);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_logging);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_id);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_secret);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.grant_type);
          assert.notEqual(undefined, sampleDotJson.properties.ssl);
          assert.notEqual(null, sampleDotJson.properties.ssl);
          assert.notEqual('', sampleDotJson.properties.ssl);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ecdhCurve);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.secure_protocol);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ciphers);
          assert.notEqual(undefined, sampleDotJson.properties.base_path);
          assert.notEqual(undefined, sampleDotJson.properties.version);
          assert.notEqual(undefined, sampleDotJson.properties.cache_location);
          assert.notEqual(undefined, sampleDotJson.properties.encode_pathvars);
          assert.notEqual(undefined, sampleDotJson.properties.encode_queryvars);
          assert.notEqual(undefined, sampleDotJson.properties.save_metric);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck);
          assert.notEqual(null, sampleDotJson.properties.healthcheck);
          assert.notEqual('', sampleDotJson.properties.healthcheck);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.type);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.frequency);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.query_object);
          assert.notEqual(undefined, sampleDotJson.properties.throttle);
          assert.notEqual(null, sampleDotJson.properties.throttle);
          assert.notEqual('', sampleDotJson.properties.throttle);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.throttle_enabled);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.number_pronghorns);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.sync_async);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.max_in_queue);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.concurrent_max);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.expire_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.avg_runtime);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.priorities);
          assert.notEqual(undefined, sampleDotJson.properties.request);
          assert.notEqual(null, sampleDotJson.properties.request);
          assert.notEqual('', sampleDotJson.properties.request);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_redirects);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_retries);
          assert.notEqual(undefined, sampleDotJson.properties.request.limit_retry_error);
          assert.notEqual(undefined, sampleDotJson.properties.request.failover_codes);
          assert.notEqual(undefined, sampleDotJson.properties.request.attempt_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.payload);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.uriOptions);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.addlHeaders);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.authData);
          assert.notEqual(undefined, sampleDotJson.properties.request.healthcheck_on_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_raw);
          assert.notEqual(undefined, sampleDotJson.properties.request.archiving);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_request);
          assert.notEqual(undefined, sampleDotJson.properties.proxy);
          assert.notEqual(null, sampleDotJson.properties.proxy);
          assert.notEqual('', sampleDotJson.properties.proxy);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.host);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.port);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.username);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo);
          assert.notEqual(null, sampleDotJson.properties.mongo);
          assert.notEqual('', sampleDotJson.properties.mongo);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.host);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.port);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.database);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.username);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.replSet);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevice);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevicesFiltered);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.isAlive);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getConfig);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getCount);
          assert.notEqual(undefined, sampleDotJson.properties.cache);
          assert.notEqual(undefined, sampleDotJson.properties.cache.entities);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkProperties', () => {
      it('should have a checkProperties function', (done) => {
        try {
          assert.equal(true, typeof a.checkProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the sample properties should be good - if failure change the log level', (done) => {
        try {
          const samplePropsJson = require('../../sampleProperties.json');
          const clean = a.checkProperties(samplePropsJson.properties);

          try {
            assert.notEqual(0, Object.keys(clean));
            assert.equal(undefined, clean.exception);
            assert.notEqual(undefined, clean.host);
            assert.notEqual(null, clean.host);
            assert.notEqual('', clean.host);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('README.md', () => {
      it('should have a README', (done) => {
        try {
          fs.exists('README.md', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('README.md should be customized', (done) => {
        try {
          fs.readFile('README.md', 'utf8', (err, data) => {
            assert.equal(-1, data.indexOf('[System]'));
            assert.equal(-1, data.indexOf('[system]'));
            assert.equal(-1, data.indexOf('[version]'));
            assert.equal(-1, data.indexOf('[namespace]'));
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#connect', () => {
      it('should have a connect function', (done) => {
        try {
          assert.equal(true, typeof a.connect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should have a healthCheck function', (done) => {
        try {
          assert.equal(true, typeof a.healthCheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUpdateAdapterConfiguration', () => {
      it('should have a iapUpdateAdapterConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.iapUpdateAdapterConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapSuspendAdapter', () => {
      it('should have a iapSuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapSuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUnsuspendAdapter', () => {
      it('should have a iapUnsuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapUnsuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterQueue', () => {
      it('should have a iapGetAdapterQueue function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterQueue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapFindAdapterPath', () => {
      it('should have a iapFindAdapterPath function', (done) => {
        try {
          assert.equal(true, typeof a.iapFindAdapterPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('iapFindAdapterPath should find atleast one path that matches', (done) => {
        try {
          a.iapFindAdapterPath('{base_path}/{version}', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.equal(true, data.found);
              assert.notEqual(undefined, data.foundIn);
              assert.notEqual(null, data.foundIn);
              assert.notEqual(0, data.foundIn.length);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapTroubleshootAdapter', () => {
      it('should have a iapTroubleshootAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapTroubleshootAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterHealthcheck', () => {
      it('should have a iapRunAdapterHealthcheck function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterHealthcheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterConnectivity', () => {
      it('should have a iapRunAdapterConnectivity function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterConnectivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterBasicGet', () => {
      it('should have a iapRunAdapterBasicGet function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterBasicGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapMoveAdapterEntitiesToDB', () => {
      it('should have a iapMoveAdapterEntitiesToDB function', (done) => {
        try {
          assert.equal(true, typeof a.iapMoveAdapterEntitiesToDB === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkActionFiles', () => {
      it('should have a checkActionFiles function', (done) => {
        try {
          assert.equal(true, typeof a.checkActionFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the action files should be good - if failure change the log level as most issues are warnings', (done) => {
        try {
          const clean = a.checkActionFiles();

          try {
            for (let c = 0; c < clean.length; c += 1) {
              log.error(clean[c]);
            }
            assert.equal(0, clean.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#encryptProperty', () => {
      it('should have a encryptProperty function', (done) => {
        try {
          assert.equal(true, typeof a.encryptProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should get base64 encoded property', (done) => {
        try {
          a.encryptProperty('testing', 'base64', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{code}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get encrypted property', (done) => {
        try {
          a.encryptProperty('testing', 'encrypt', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{crypt}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapDeactivateTasks', () => {
      it('should have a iapDeactivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapDeactivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapActivateTasks', () => {
      it('should have a iapActivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapActivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapPopulateEntityCache', () => {
      it('should have a iapPopulateEntityCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapPopulateEntityCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRetrieveEntitiesCache', () => {
      it('should have a iapRetrieveEntitiesCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapRetrieveEntitiesCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#hasEntities', () => {
      it('should have a hasEntities function', (done) => {
        try {
          assert.equal(true, typeof a.hasEntities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevice', () => {
      it('should have a getDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevicesFiltered', () => {
      it('should have a getDevicesFiltered function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesFiltered === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#isAlive', () => {
      it('should have a isAlive function', (done) => {
        try {
          assert.equal(true, typeof a.isAlive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getConfig', () => {
      it('should have a getConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetDeviceCount', () => {
      it('should have a iapGetDeviceCount function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetDeviceCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapExpandedGenericAdapterRequest', () => {
      it('should have a iapExpandedGenericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.iapExpandedGenericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequest', () => {
      it('should have a genericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequestNoBasePath', () => {
      it('should have a genericAdapterRequestNoBasePath function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequestNoBasePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterLint', () => {
      it('should have a iapRunAdapterLint function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterLint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the lint results', (done) => {
        try {
          a.iapRunAdapterLint((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.status);
              assert.notEqual(null, data.status);
              assert.equal('SUCCESS', data.status);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRunAdapterTests', () => {
      it('should have a iapRunAdapterTests function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterTests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterInventory', () => {
      it('should have a iapGetAdapterInventory function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterInventory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the inventory', (done) => {
        try {
          a.iapGetAdapterInventory((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    describe('metadata.json', () => {
      it('should have a metadata.json', (done) => {
        try {
          fs.exists('metadata.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json is customized', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.equal('adapter-aws_ec2', metadataDotJson.name);
          assert.notEqual(undefined, metadataDotJson.webName);
          assert.notEqual(null, metadataDotJson.webName);
          assert.notEqual('', metadataDotJson.webName);
          assert.equal('Adapter', metadataDotJson.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json contains accurate documentation', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.documentation);
          assert.equal('https://www.npmjs.com/package/@itentialopensource/adapter-aws_ec2', metadataDotJson.documentation.npmLink);
          assert.equal('https://docs.itential.com/opensource/docs/troubleshooting-an-adapter', metadataDotJson.documentation.faqLink);
          assert.equal('https://gitlab.com/itentialopensource/adapters/contributing-guide', metadataDotJson.documentation.contributeLink);
          assert.equal('https://itential.atlassian.net/servicedesk/customer/portals', metadataDotJson.documentation.issueLink);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json has related items', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.relatedItems);
          assert.notEqual(undefined, metadataDotJson.relatedItems.adapters);
          assert.notEqual(undefined, metadataDotJson.relatedItems.integrations);
          assert.notEqual(undefined, metadataDotJson.relatedItems.ecosystemApplications);
          assert.notEqual(undefined, metadataDotJson.relatedItems.workflowProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.transformationProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.exampleProjects);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#acceptReservedInstancesExchangeQuote - errors', () => {
      it('should have a acceptReservedInstancesExchangeQuote function', (done) => {
        try {
          assert.equal(true, typeof a.acceptReservedInstancesExchangeQuote === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing reservedInstanceId', (done) => {
        try {
          a.acceptReservedInstancesExchangeQuote('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'reservedInstanceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-acceptReservedInstancesExchangeQuoteSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#acceptTransitGatewayVpcAttachment - errors', () => {
      it('should have a acceptTransitGatewayVpcAttachment function', (done) => {
        try {
          assert.equal(true, typeof a.acceptTransitGatewayVpcAttachment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing transitGatewayAttachmentId', (done) => {
        try {
          a.acceptTransitGatewayVpcAttachment(null, null, (data, error) => {
            try {
              const displayE = 'transitGatewayAttachmentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-acceptTransitGatewayVpcAttachmentSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#acceptVpcEndpointConnections - errors', () => {
      it('should have a acceptVpcEndpointConnections function', (done) => {
        try {
          assert.equal(true, typeof a.acceptVpcEndpointConnections === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceId', (done) => {
        try {
          a.acceptVpcEndpointConnections('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'serviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-acceptVpcEndpointConnectionsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpcEndpointId', (done) => {
        try {
          a.acceptVpcEndpointConnections('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'vpcEndpointId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-acceptVpcEndpointConnectionsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#acceptVpcPeeringConnection - errors', () => {
      it('should have a acceptVpcPeeringConnection function', (done) => {
        try {
          assert.equal(true, typeof a.acceptVpcPeeringConnection === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#advertiseByoipCidr - errors', () => {
      it('should have a advertiseByoipCidr function', (done) => {
        try {
          assert.equal(true, typeof a.advertiseByoipCidr === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cidr', (done) => {
        try {
          a.advertiseByoipCidr(null, null, (data, error) => {
            try {
              const displayE = 'cidr is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-advertiseByoipCidrSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#allocateAddress - errors', () => {
      it('should have a allocateAddress function', (done) => {
        try {
          assert.equal(true, typeof a.allocateAddress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#allocateHosts - errors', () => {
      it('should have a allocateHosts function', (done) => {
        try {
          assert.equal(true, typeof a.allocateHosts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing availabilityZone', (done) => {
        try {
          a.allocateHosts('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'availabilityZone is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-allocateHostsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing instanceType', (done) => {
        try {
          a.allocateHosts('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'instanceType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-allocateHostsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing quantity', (done) => {
        try {
          a.allocateHosts('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'quantity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-allocateHostsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applySecurityGroupsToClientVpnTargetNetwork - errors', () => {
      it('should have a applySecurityGroupsToClientVpnTargetNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.applySecurityGroupsToClientVpnTargetNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clientVpnEndpointId', (done) => {
        try {
          a.applySecurityGroupsToClientVpnTargetNetwork(null, null, null, null, (data, error) => {
            try {
              const displayE = 'clientVpnEndpointId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-applySecurityGroupsToClientVpnTargetNetworkSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpcId', (done) => {
        try {
          a.applySecurityGroupsToClientVpnTargetNetwork('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'vpcId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-applySecurityGroupsToClientVpnTargetNetworkSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing securityGroupId', (done) => {
        try {
          a.applySecurityGroupsToClientVpnTargetNetwork('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'securityGroupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-applySecurityGroupsToClientVpnTargetNetworkSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#assignIpv6Addresses - errors', () => {
      it('should have a assignIpv6Addresses function', (done) => {
        try {
          assert.equal(true, typeof a.assignIpv6Addresses === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkInterfaceId', (done) => {
        try {
          a.assignIpv6Addresses('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'networkInterfaceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-assignIpv6AddressesSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#assignPrivateIpAddresses - errors', () => {
      it('should have a assignPrivateIpAddresses function', (done) => {
        try {
          assert.equal(true, typeof a.assignPrivateIpAddresses === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkInterfaceId', (done) => {
        try {
          a.assignPrivateIpAddresses('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'networkInterfaceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-assignPrivateIpAddressesSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#associateAddress - errors', () => {
      it('should have a associateAddress function', (done) => {
        try {
          assert.equal(true, typeof a.associateAddress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#associateClientVpnTargetNetwork - errors', () => {
      it('should have a associateClientVpnTargetNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.associateClientVpnTargetNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clientVpnEndpointId', (done) => {
        try {
          a.associateClientVpnTargetNetwork(null, null, null, (data, error) => {
            try {
              const displayE = 'clientVpnEndpointId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-associateClientVpnTargetNetworkSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing subnetId', (done) => {
        try {
          a.associateClientVpnTargetNetwork('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'subnetId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-associateClientVpnTargetNetworkSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#associateDhcpOptions - errors', () => {
      it('should have a associateDhcpOptions function', (done) => {
        try {
          assert.equal(true, typeof a.associateDhcpOptions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dhcpOptionsId', (done) => {
        try {
          a.associateDhcpOptions(null, null, null, (data, error) => {
            try {
              const displayE = 'dhcpOptionsId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-associateDhcpOptionsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpcId', (done) => {
        try {
          a.associateDhcpOptions('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'vpcId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-associateDhcpOptionsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#associateIamInstanceProfile - errors', () => {
      it('should have a associateIamInstanceProfile function', (done) => {
        try {
          assert.equal(true, typeof a.associateIamInstanceProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing instanceId', (done) => {
        try {
          a.associateIamInstanceProfile('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'instanceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-associateIamInstanceProfileSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#associateRouteTable - errors', () => {
      it('should have a associateRouteTable function', (done) => {
        try {
          assert.equal(true, typeof a.associateRouteTable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing routeTableId', (done) => {
        try {
          a.associateRouteTable('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'routeTableId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-associateRouteTableSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing subnetId', (done) => {
        try {
          a.associateRouteTable('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'subnetId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-associateRouteTableSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#associateSubnetCidrBlock - errors', () => {
      it('should have a associateSubnetCidrBlock function', (done) => {
        try {
          assert.equal(true, typeof a.associateSubnetCidrBlock === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ipv6CidrBlock', (done) => {
        try {
          a.associateSubnetCidrBlock(null, null, (data, error) => {
            try {
              const displayE = 'ipv6CidrBlock is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-associateSubnetCidrBlockSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing subnetId', (done) => {
        try {
          a.associateSubnetCidrBlock('fakeparam', null, (data, error) => {
            try {
              const displayE = 'subnetId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-associateSubnetCidrBlockSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#associateTransitGatewayRouteTable - errors', () => {
      it('should have a associateTransitGatewayRouteTable function', (done) => {
        try {
          assert.equal(true, typeof a.associateTransitGatewayRouteTable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing transitGatewayRouteTableId', (done) => {
        try {
          a.associateTransitGatewayRouteTable(null, null, null, (data, error) => {
            try {
              const displayE = 'transitGatewayRouteTableId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-associateTransitGatewayRouteTableSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing transitGatewayAttachmentId', (done) => {
        try {
          a.associateTransitGatewayRouteTable('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'transitGatewayAttachmentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-associateTransitGatewayRouteTableSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#associateVpcCidrBlock - errors', () => {
      it('should have a associateVpcCidrBlock function', (done) => {
        try {
          assert.equal(true, typeof a.associateVpcCidrBlock === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpcId', (done) => {
        try {
          a.associateVpcCidrBlock('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'vpcId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-associateVpcCidrBlockSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#attachClassicLinkVpc - errors', () => {
      it('should have a attachClassicLinkVpc function', (done) => {
        try {
          assert.equal(true, typeof a.attachClassicLinkVpc === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing securityGroupId', (done) => {
        try {
          a.attachClassicLinkVpc('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'securityGroupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-attachClassicLinkVpcSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing instanceId', (done) => {
        try {
          a.attachClassicLinkVpc('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'instanceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-attachClassicLinkVpcSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpcId', (done) => {
        try {
          a.attachClassicLinkVpc('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'vpcId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-attachClassicLinkVpcSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#attachInternetGateway - errors', () => {
      it('should have a attachInternetGateway function', (done) => {
        try {
          assert.equal(true, typeof a.attachInternetGateway === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing internetGatewayId', (done) => {
        try {
          a.attachInternetGateway('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'internetGatewayId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-attachInternetGatewaySTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpcId', (done) => {
        try {
          a.attachInternetGateway('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'vpcId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-attachInternetGatewaySTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#attachNetworkInterface - errors', () => {
      it('should have a attachNetworkInterface function', (done) => {
        try {
          assert.equal(true, typeof a.attachNetworkInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceIndex', (done) => {
        try {
          a.attachNetworkInterface(null, null, null, null, (data, error) => {
            try {
              const displayE = 'deviceIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-attachNetworkInterfaceSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing instanceId', (done) => {
        try {
          a.attachNetworkInterface('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'instanceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-attachNetworkInterfaceSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkInterfaceId', (done) => {
        try {
          a.attachNetworkInterface('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'networkInterfaceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-attachNetworkInterfaceSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#attachVolume - errors', () => {
      it('should have a attachVolume function', (done) => {
        try {
          assert.equal(true, typeof a.attachVolume === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing device', (done) => {
        try {
          a.attachVolume(null, null, null, null, (data, error) => {
            try {
              const displayE = 'device is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-attachVolumeSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing instanceId', (done) => {
        try {
          a.attachVolume('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'instanceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-attachVolumeSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing volumeId', (done) => {
        try {
          a.attachVolume('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'volumeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-attachVolumeSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#attachVpnGateway - errors', () => {
      it('should have a attachVpnGateway function', (done) => {
        try {
          assert.equal(true, typeof a.attachVpnGateway === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpcId', (done) => {
        try {
          a.attachVpnGateway(null, null, null, (data, error) => {
            try {
              const displayE = 'vpcId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-attachVpnGatewaySTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpnGatewayId', (done) => {
        try {
          a.attachVpnGateway('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'vpnGatewayId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-attachVpnGatewaySTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#authorizeClientVpnIngress - errors', () => {
      it('should have a authorizeClientVpnIngress function', (done) => {
        try {
          assert.equal(true, typeof a.authorizeClientVpnIngress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clientVpnEndpointId', (done) => {
        try {
          a.authorizeClientVpnIngress(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'clientVpnEndpointId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-authorizeClientVpnIngressSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing targetNetworkCidr', (done) => {
        try {
          a.authorizeClientVpnIngress('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'targetNetworkCidr is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-authorizeClientVpnIngressSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#authorizeSecurityGroupEgress - errors', () => {
      it('should have a authorizeSecurityGroupEgress function', (done) => {
        try {
          assert.equal(true, typeof a.authorizeSecurityGroupEgress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupId', (done) => {
        try {
          a.authorizeSecurityGroupEgress('fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-authorizeSecurityGroupEgressSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#authorizeSecurityGroupIngress - errors', () => {
      it('should have a authorizeSecurityGroupIngress function', (done) => {
        try {
          assert.equal(true, typeof a.authorizeSecurityGroupIngress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#bundleInstance - errors', () => {
      it('should have a bundleInstance function', (done) => {
        try {
          assert.equal(true, typeof a.bundleInstance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing instanceId', (done) => {
        try {
          a.bundleInstance(null, null, null, (data, error) => {
            try {
              const displayE = 'instanceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-bundleInstanceSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cancelBundleTask - errors', () => {
      it('should have a cancelBundleTask function', (done) => {
        try {
          assert.equal(true, typeof a.cancelBundleTask === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bundleId', (done) => {
        try {
          a.cancelBundleTask(null, null, (data, error) => {
            try {
              const displayE = 'bundleId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-cancelBundleTaskSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cancelCapacityReservation - errors', () => {
      it('should have a cancelCapacityReservation function', (done) => {
        try {
          assert.equal(true, typeof a.cancelCapacityReservation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing capacityReservationId', (done) => {
        try {
          a.cancelCapacityReservation(null, null, (data, error) => {
            try {
              const displayE = 'capacityReservationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-cancelCapacityReservationSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cancelConversionTask - errors', () => {
      it('should have a cancelConversionTask function', (done) => {
        try {
          assert.equal(true, typeof a.cancelConversionTask === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing conversionTaskId', (done) => {
        try {
          a.cancelConversionTask(null, null, null, (data, error) => {
            try {
              const displayE = 'conversionTaskId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-cancelConversionTaskSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cancelExportTask - errors', () => {
      it('should have a cancelExportTask function', (done) => {
        try {
          assert.equal(true, typeof a.cancelExportTask === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing exportTaskId', (done) => {
        try {
          a.cancelExportTask(null, (data, error) => {
            try {
              const displayE = 'exportTaskId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-cancelExportTaskSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cancelImportTask - errors', () => {
      it('should have a cancelImportTask function', (done) => {
        try {
          assert.equal(true, typeof a.cancelImportTask === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cancelReservedInstancesListing - errors', () => {
      it('should have a cancelReservedInstancesListing function', (done) => {
        try {
          assert.equal(true, typeof a.cancelReservedInstancesListing === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing reservedInstancesListingId', (done) => {
        try {
          a.cancelReservedInstancesListing(null, (data, error) => {
            try {
              const displayE = 'reservedInstancesListingId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-cancelReservedInstancesListingSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cancelSpotFleetRequests - errors', () => {
      it('should have a cancelSpotFleetRequests function', (done) => {
        try {
          assert.equal(true, typeof a.cancelSpotFleetRequests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing spotFleetRequestId', (done) => {
        try {
          a.cancelSpotFleetRequests('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'spotFleetRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-cancelSpotFleetRequestsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing terminateInstances', (done) => {
        try {
          a.cancelSpotFleetRequests('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'terminateInstances is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-cancelSpotFleetRequestsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cancelSpotInstanceRequests - errors', () => {
      it('should have a cancelSpotInstanceRequests function', (done) => {
        try {
          assert.equal(true, typeof a.cancelSpotInstanceRequests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing spotInstanceRequestId', (done) => {
        try {
          a.cancelSpotInstanceRequests('fakeparam', null, (data, error) => {
            try {
              const displayE = 'spotInstanceRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-cancelSpotInstanceRequestsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#confirmProductInstance - errors', () => {
      it('should have a confirmProductInstance function', (done) => {
        try {
          assert.equal(true, typeof a.confirmProductInstance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing instanceId', (done) => {
        try {
          a.confirmProductInstance(null, null, null, (data, error) => {
            try {
              const displayE = 'instanceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-confirmProductInstanceSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing productCode', (done) => {
        try {
          a.confirmProductInstance('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'productCode is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-confirmProductInstanceSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#copyFpgaImage - errors', () => {
      it('should have a copyFpgaImage function', (done) => {
        try {
          assert.equal(true, typeof a.copyFpgaImage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sourceFpgaImageId', (done) => {
        try {
          a.copyFpgaImage('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'sourceFpgaImageId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-copyFpgaImageSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sourceRegion', (done) => {
        try {
          a.copyFpgaImage('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'sourceRegion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-copyFpgaImageSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#copyImage - errors', () => {
      it('should have a copyImage function', (done) => {
        try {
          assert.equal(true, typeof a.copyImage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.copyImage('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-copyImageSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sourceImageId', (done) => {
        try {
          a.copyImage('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'sourceImageId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-copyImageSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sourceRegion', (done) => {
        try {
          a.copyImage('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'sourceRegion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-copyImageSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#copySnapshot - errors', () => {
      it('should have a copySnapshot function', (done) => {
        try {
          assert.equal(true, typeof a.copySnapshot === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sourceRegion', (done) => {
        try {
          a.copySnapshot('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'sourceRegion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-copySnapshotSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sourceSnapshotId', (done) => {
        try {
          a.copySnapshot('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'sourceSnapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-copySnapshotSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createCapacityReservation - errors', () => {
      it('should have a createCapacityReservation function', (done) => {
        try {
          assert.equal(true, typeof a.createCapacityReservation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing instanceType', (done) => {
        try {
          a.createCapacityReservation('fakeparam', null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'instanceType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createCapacityReservationSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing instancePlatform', (done) => {
        try {
          a.createCapacityReservation('fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'instancePlatform is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createCapacityReservationSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing availabilityZone', (done) => {
        try {
          a.createCapacityReservation('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'availabilityZone is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createCapacityReservationSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing instanceCount', (done) => {
        try {
          a.createCapacityReservation('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'instanceCount is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createCapacityReservationSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createCapacityReservations - errors', () => {
      it('should have a createCapacityReservations function', (done) => {
        try {
          assert.equal(true, typeof a.createCapacityReservations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing capacityReservationArray', (done) => {
        try {
          a.createCapacityReservations(null, (data, error) => {
            try {
              const displayE = 'capacityReservationArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createCapacityReservationsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createClientVpnEndpoint - errors', () => {
      it('should have a createClientVpnEndpoint function', (done) => {
        try {
          assert.equal(true, typeof a.createClientVpnEndpoint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clientCidrBlock', (done) => {
        try {
          a.createClientVpnEndpoint(null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'clientCidrBlock is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createClientVpnEndpointSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serverCertificateArn', (done) => {
        try {
          a.createClientVpnEndpoint('fakeparam', null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'serverCertificateArn is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createClientVpnEndpointSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing authentication', (done) => {
        try {
          a.createClientVpnEndpoint('fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'authentication is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createClientVpnEndpointSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createClientVpnEndpoints - errors', () => {
      it('should have a createClientVpnEndpoints function', (done) => {
        try {
          assert.equal(true, typeof a.createClientVpnEndpoints === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clientVpnEndpointArray', (done) => {
        try {
          a.createClientVpnEndpoints(null, (data, error) => {
            try {
              const displayE = 'clientVpnEndpointArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createClientVpnEndpointsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createClientVpnRoute - errors', () => {
      it('should have a createClientVpnRoute function', (done) => {
        try {
          assert.equal(true, typeof a.createClientVpnRoute === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clientVpnEndpointId', (done) => {
        try {
          a.createClientVpnRoute(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'clientVpnEndpointId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createClientVpnRouteSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing destinationCidrBlock', (done) => {
        try {
          a.createClientVpnRoute('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'destinationCidrBlock is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createClientVpnRouteSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing targetVpcSubnetId', (done) => {
        try {
          a.createClientVpnRoute('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'targetVpcSubnetId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createClientVpnRouteSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createClientVpnRoutes - errors', () => {
      it('should have a createClientVpnRoutes function', (done) => {
        try {
          assert.equal(true, typeof a.createClientVpnRoutes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing createClientVpnRouteArray', (done) => {
        try {
          a.createClientVpnRoutes(null, (data, error) => {
            try {
              const displayE = 'createClientVpnRouteArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createClientVpnRoutesSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createCustomerGateway - errors', () => {
      it('should have a createCustomerGateway function', (done) => {
        try {
          assert.equal(true, typeof a.createCustomerGateway === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bgpAsn', (done) => {
        try {
          a.createCustomerGateway(null, null, null, null, (data, error) => {
            try {
              const displayE = 'bgpAsn is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createCustomerGatewaySTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ipAddress', (done) => {
        try {
          a.createCustomerGateway('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'ipAddress is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createCustomerGatewaySTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.createCustomerGateway('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createCustomerGatewaySTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createCustomerGateways - errors', () => {
      it('should have a createCustomerGateways function', (done) => {
        try {
          assert.equal(true, typeof a.createCustomerGateways === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing createCustomerGatewayArray', (done) => {
        try {
          a.createCustomerGateways(null, (data, error) => {
            try {
              const displayE = 'createCustomerGatewayArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createCustomerGatewaysSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createDefaultSubnet - errors', () => {
      it('should have a createDefaultSubnet function', (done) => {
        try {
          assert.equal(true, typeof a.createDefaultSubnet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing availabilityZone', (done) => {
        try {
          a.createDefaultSubnet(null, null, (data, error) => {
            try {
              const displayE = 'availabilityZone is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createDefaultSubnetSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createDefaultSubnets - errors', () => {
      it('should have a createDefaultSubnets function', (done) => {
        try {
          assert.equal(true, typeof a.createDefaultSubnets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing createDefaultSubnetArray', (done) => {
        try {
          a.createDefaultSubnets(null, (data, error) => {
            try {
              const displayE = 'createDefaultSubnetArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createDefaultSubnetsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createDefaultVpc - errors', () => {
      it('should have a createDefaultVpc function', (done) => {
        try {
          assert.equal(true, typeof a.createDefaultVpc === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createDefaultVpcs - errors', () => {
      it('should have a createDefaultVpcs function', (done) => {
        try {
          assert.equal(true, typeof a.createDefaultVpcs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing createDefaultVpcArray', (done) => {
        try {
          a.createDefaultVpcs(null, (data, error) => {
            try {
              const displayE = 'createDefaultVpcArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createDefaultVpcsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createDhcpOptions - errors', () => {
      it('should have a createDhcpOptions function', (done) => {
        try {
          assert.equal(true, typeof a.createDhcpOptions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dhcpConfiguration', (done) => {
        try {
          a.createDhcpOptions(null, null, (data, error) => {
            try {
              const displayE = 'dhcpConfiguration is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createDhcpOptionsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createMultipleDhcpOptions - errors', () => {
      it('should have a createMultipleDhcpOptions function', (done) => {
        try {
          assert.equal(true, typeof a.createMultipleDhcpOptions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dhcpOptionsArray', (done) => {
        try {
          a.createMultipleDhcpOptions(null, (data, error) => {
            try {
              const displayE = 'dhcpOptionsArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createMultipleDhcpOptionsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createEgressOnlyInternetGateway - errors', () => {
      it('should have a createEgressOnlyInternetGateway function', (done) => {
        try {
          assert.equal(true, typeof a.createEgressOnlyInternetGateway === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpcId', (done) => {
        try {
          a.createEgressOnlyInternetGateway('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'vpcId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createEgressOnlyInternetGatewaySTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createEgressOnlyInternetGateways - errors', () => {
      it('should have a createEgressOnlyInternetGateways function', (done) => {
        try {
          assert.equal(true, typeof a.createEgressOnlyInternetGateways === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing EgressOnlyInternetGatewaysArray', (done) => {
        try {
          a.createEgressOnlyInternetGateways(null, (data, error) => {
            try {
              const displayE = 'EgressOnlyInternetGatewaysArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createEgressOnlyInternetGatewaysSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createFleet - errors', () => {
      it('should have a createFleet function', (done) => {
        try {
          assert.equal(true, typeof a.createFleet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing launchTemplateConfigs', (done) => {
        try {
          a.createFleet('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'launchTemplateConfigs is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createFleetSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createFleets - errors', () => {
      it('should have a createFleets function', (done) => {
        try {
          assert.equal(true, typeof a.createFleets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fleetsArray', (done) => {
        try {
          a.createFleets(null, (data, error) => {
            try {
              const displayE = 'fleetsArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createFleetsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createFlowLogs - errors', () => {
      it('should have a createFlowLogs function', (done) => {
        try {
          assert.equal(true, typeof a.createFlowLogs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceId', (done) => {
        try {
          a.createFlowLogs('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'resourceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createFlowLogsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceType', (done) => {
        try {
          a.createFlowLogs('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'resourceType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createFlowLogsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing trafficType', (done) => {
        try {
          a.createFlowLogs('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'trafficType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createFlowLogsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createMultipleFlowLogs - errors', () => {
      it('should have a createMultipleFlowLogs function', (done) => {
        try {
          assert.equal(true, typeof a.createMultipleFlowLogs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing flowLogsArray', (done) => {
        try {
          a.createMultipleFlowLogs(null, (data, error) => {
            try {
              const displayE = 'flowLogsArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createMultipleFlowLogsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createFpgaImage - errors', () => {
      it('should have a createFpgaImage function', (done) => {
        try {
          assert.equal(true, typeof a.createFpgaImage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createFpgaImages - errors', () => {
      it('should have a createFpgaImages function', (done) => {
        try {
          assert.equal(true, typeof a.createFpgaImages === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing FpgaImagesArray', (done) => {
        try {
          a.createFpgaImages(null, (data, error) => {
            try {
              const displayE = 'FpgaImagesArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createFpgaImagesSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createImage - errors', () => {
      it('should have a createImage function', (done) => {
        try {
          assert.equal(true, typeof a.createImage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing instanceId', (done) => {
        try {
          a.createImage('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'instanceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createImageSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.createImage('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createImageSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createImages - errors', () => {
      it('should have a createImages function', (done) => {
        try {
          assert.equal(true, typeof a.createImages === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ImagesArray', (done) => {
        try {
          a.createImages(null, (data, error) => {
            try {
              const displayE = 'ImagesArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createImagesSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createInstanceExportTask - errors', () => {
      it('should have a createInstanceExportTask function', (done) => {
        try {
          assert.equal(true, typeof a.createInstanceExportTask === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing instanceId', (done) => {
        try {
          a.createInstanceExportTask('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'instanceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createInstanceExportTaskSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createInstanceExportTasks - errors', () => {
      it('should have a createInstanceExportTasks function', (done) => {
        try {
          assert.equal(true, typeof a.createInstanceExportTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing InstanceExportTasksArray', (done) => {
        try {
          a.createInstanceExportTasks(null, (data, error) => {
            try {
              const displayE = 'InstanceExportTasksArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createInstanceExportTasksSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createInternetGateway - errors', () => {
      it('should have a createInternetGateway function', (done) => {
        try {
          assert.equal(true, typeof a.createInternetGateway === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createInternetGateways - errors', () => {
      it('should have a createInternetGateways function', (done) => {
        try {
          assert.equal(true, typeof a.createInternetGateways === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing internetGatewayArray', (done) => {
        try {
          a.createInternetGateways(null, (data, error) => {
            try {
              const displayE = 'internetGatewayArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createInternetGatewaysSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createKeyPair - errors', () => {
      it('should have a createKeyPair function', (done) => {
        try {
          assert.equal(true, typeof a.createKeyPair === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing keyName', (done) => {
        try {
          a.createKeyPair(null, null, (data, error) => {
            try {
              const displayE = 'keyName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createKeyPairSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createKeyPairs - errors', () => {
      it('should have a createKeyPairs function', (done) => {
        try {
          assert.equal(true, typeof a.createKeyPairs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing KeyPairsArray', (done) => {
        try {
          a.createKeyPairs(null, (data, error) => {
            try {
              const displayE = 'KeyPairsArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createKeyPairsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createLaunchTemplate - errors', () => {
      it('should have a createLaunchTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.createLaunchTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing launchTemplateName', (done) => {
        try {
          a.createLaunchTemplate('fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'launchTemplateName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createLaunchTemplateSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createLaunchTemplates - errors', () => {
      it('should have a createLaunchTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.createLaunchTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing LaunchTemplatesArray', (done) => {
        try {
          a.createLaunchTemplates(null, (data, error) => {
            try {
              const displayE = 'LaunchTemplatesArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createLaunchTemplatesSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createLaunchTemplateVersion - errors', () => {
      it('should have a createLaunchTemplateVersion function', (done) => {
        try {
          assert.equal(true, typeof a.createLaunchTemplateVersion === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createLaunchTemplateVersions - errors', () => {
      it('should have a createLaunchTemplateVersions function', (done) => {
        try {
          assert.equal(true, typeof a.createLaunchTemplateVersions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNatGateway - errors', () => {
      it('should have a createNatGateway function', (done) => {
        try {
          assert.equal(true, typeof a.createNatGateway === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing allocationId', (done) => {
        try {
          a.createNatGateway(null, null, null, (data, error) => {
            try {
              const displayE = 'allocationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createNatGatewaySTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing subnetId', (done) => {
        try {
          a.createNatGateway('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'subnetId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createNatGatewaySTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNatGateways - errors', () => {
      it('should have a createNatGateways function', (done) => {
        try {
          assert.equal(true, typeof a.createNatGateways === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing NatGatewaysArray', (done) => {
        try {
          a.createNatGateways(null, (data, error) => {
            try {
              const displayE = 'NatGatewaysArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createNatGatewaysSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNetworkAcl - errors', () => {
      it('should have a createNetworkAcl function', (done) => {
        try {
          assert.equal(true, typeof a.createNetworkAcl === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpcId', (done) => {
        try {
          a.createNetworkAcl('fakeparam', null, (data, error) => {
            try {
              const displayE = 'vpcId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createNetworkAclSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNetworkAcls - errors', () => {
      it('should have a createNetworkAcls function', (done) => {
        try {
          assert.equal(true, typeof a.createNetworkAcls === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing NetworkAclsArray', (done) => {
        try {
          a.createNetworkAcls(null, (data, error) => {
            try {
              const displayE = 'NetworkAclsArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createNetworkAclsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNetworkAclEntry - errors', () => {
      it('should have a createNetworkAclEntry function', (done) => {
        try {
          assert.equal(true, typeof a.createNetworkAclEntry === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing egress', (done) => {
        try {
          a.createNetworkAclEntry('fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'egress is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createNetworkAclEntrySTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkAclId', (done) => {
        try {
          a.createNetworkAclEntry('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'networkAclId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createNetworkAclEntrySTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing protocol', (done) => {
        try {
          a.createNetworkAclEntry('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'protocol is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createNetworkAclEntrySTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleAction', (done) => {
        try {
          a.createNetworkAclEntry('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'ruleAction is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createNetworkAclEntrySTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleNumber', (done) => {
        try {
          a.createNetworkAclEntry('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'ruleNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createNetworkAclEntrySTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNetworkAclEntries - errors', () => {
      it('should have a createNetworkAclEntries function', (done) => {
        try {
          assert.equal(true, typeof a.createNetworkAclEntries === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing NetworkAclEntriesArray', (done) => {
        try {
          a.createNetworkAclEntries(null, (data, error) => {
            try {
              const displayE = 'NetworkAclEntriesArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createNetworkAclEntriesSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNetworkInterface - errors', () => {
      it('should have a createNetworkInterface function', (done) => {
        try {
          assert.equal(true, typeof a.createNetworkInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing subnetId', (done) => {
        try {
          a.createNetworkInterface('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'subnetId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createNetworkInterfaceSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNetworkInterfaces - errors', () => {
      it('should have a createNetworkInterfaces function', (done) => {
        try {
          assert.equal(true, typeof a.createNetworkInterfaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing NetworkInterfacesArray', (done) => {
        try {
          a.createNetworkInterfaces(null, (data, error) => {
            try {
              const displayE = 'NetworkInterfacesArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createNetworkInterfacesSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNetworkInterfacePermission - errors', () => {
      it('should have a createNetworkInterfacePermission function', (done) => {
        try {
          assert.equal(true, typeof a.createNetworkInterfacePermission === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkInterfaceId', (done) => {
        try {
          a.createNetworkInterfacePermission(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'networkInterfaceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createNetworkInterfacePermissionSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing permission', (done) => {
        try {
          a.createNetworkInterfacePermission('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'permission is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createNetworkInterfacePermissionSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNetworkInterfacePermissions - errors', () => {
      it('should have a createNetworkInterfacePermissions function', (done) => {
        try {
          assert.equal(true, typeof a.createNetworkInterfacePermissions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing InterfacePermissionsArray', (done) => {
        try {
          a.createNetworkInterfacePermissions(null, (data, error) => {
            try {
              const displayE = 'InterfacePermissionsArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createNetworkInterfacePermissionsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createPlacementGroup - errors', () => {
      it('should have a createPlacementGroup function', (done) => {
        try {
          assert.equal(true, typeof a.createPlacementGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createPlacementGroups - errors', () => {
      it('should have a createPlacementGroups function', (done) => {
        try {
          assert.equal(true, typeof a.createPlacementGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing PlacementGroupsArray', (done) => {
        try {
          a.createPlacementGroups(null, (data, error) => {
            try {
              const displayE = 'PlacementGroupsArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createPlacementGroupsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createReservedInstancesListing - errors', () => {
      it('should have a createReservedInstancesListing function', (done) => {
        try {
          assert.equal(true, typeof a.createReservedInstancesListing === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clientToken', (done) => {
        try {
          a.createReservedInstancesListing(null, null, null, null, (data, error) => {
            try {
              const displayE = 'clientToken is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createReservedInstancesListingSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing instanceCount', (done) => {
        try {
          a.createReservedInstancesListing('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'instanceCount is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createReservedInstancesListingSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing priceSchedules', (done) => {
        try {
          a.createReservedInstancesListing('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'priceSchedules is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createReservedInstancesListingSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing reservedInstancesId', (done) => {
        try {
          a.createReservedInstancesListing('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'reservedInstancesId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createReservedInstancesListingSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createReservedInstancesListings - errors', () => {
      it('should have a createReservedInstancesListings function', (done) => {
        try {
          assert.equal(true, typeof a.createReservedInstancesListings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ReservedInstancesListingArray', (done) => {
        try {
          a.createReservedInstancesListings(null, (data, error) => {
            try {
              const displayE = 'ReservedInstancesListingArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createReservedInstancesListingsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createRoute - errors', () => {
      it('should have a createRoute function', (done) => {
        try {
          assert.equal(true, typeof a.createRoute === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing routeTableId', (done) => {
        try {
          a.createRoute('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'routeTableId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createRouteSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createRoutes - errors', () => {
      it('should have a createRoutes function', (done) => {
        try {
          assert.equal(true, typeof a.createRoutes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing routeArray', (done) => {
        try {
          a.createRoutes(null, (data, error) => {
            try {
              const displayE = 'routeArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createRoutesSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createRouteTable - errors', () => {
      it('should have a createRouteTable function', (done) => {
        try {
          assert.equal(true, typeof a.createRouteTable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpcId', (done) => {
        try {
          a.createRouteTable('fakeparam', null, (data, error) => {
            try {
              const displayE = 'vpcId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createRouteTableSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createRouteTables - errors', () => {
      it('should have a createRouteTables function', (done) => {
        try {
          assert.equal(true, typeof a.createRouteTables === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing routeTableArray', (done) => {
        try {
          a.createRouteTables(null, (data, error) => {
            try {
              const displayE = 'routeTableArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createRouteTablesSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createSecurityGroup - errors', () => {
      it('should have a createSecurityGroup function', (done) => {
        try {
          assert.equal(true, typeof a.createSecurityGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupDescription', (done) => {
        try {
          a.createSecurityGroup(null, null, null, null, (data, error) => {
            try {
              const displayE = 'groupDescription is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createSecurityGroupSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupName', (done) => {
        try {
          a.createSecurityGroup('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'groupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createSecurityGroupSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createSecurityGroups - errors', () => {
      it('should have a createSecurityGroups function', (done) => {
        try {
          assert.equal(true, typeof a.createSecurityGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing securityGroupArray', (done) => {
        try {
          a.createSecurityGroups(null, (data, error) => {
            try {
              const displayE = 'securityGroupArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createSecurityGroupsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createSnapshot - errors', () => {
      it('should have a createSnapshot function', (done) => {
        try {
          assert.equal(true, typeof a.createSnapshot === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing volumeId', (done) => {
        try {
          a.createSnapshot('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'volumeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createSnapshotSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createSnapshots - errors', () => {
      it('should have a createSnapshots function', (done) => {
        try {
          assert.equal(true, typeof a.createSnapshots === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing SnapshotsArray', (done) => {
        try {
          a.createSnapshots(null, (data, error) => {
            try {
              const displayE = 'SnapshotsArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createSnapshotsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createSpotDatafeedSubscription - errors', () => {
      it('should have a createSpotDatafeedSubscription function', (done) => {
        try {
          assert.equal(true, typeof a.createSpotDatafeedSubscription === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bucket', (done) => {
        try {
          a.createSpotDatafeedSubscription(null, null, null, (data, error) => {
            try {
              const displayE = 'bucket is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createSpotDatafeedSubscriptionSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createSpotDatafeedSubscriptions - errors', () => {
      it('should have a createSpotDatafeedSubscriptions function', (done) => {
        try {
          assert.equal(true, typeof a.createSpotDatafeedSubscriptions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing SpotDatafeedSubscriptionsArray', (done) => {
        try {
          a.createSpotDatafeedSubscriptions(null, (data, error) => {
            try {
              const displayE = 'SpotDatafeedSubscriptionsArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createSpotDatafeedSubscriptionsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createSubnet - errors', () => {
      it('should have a createSubnet function', (done) => {
        try {
          assert.equal(true, typeof a.createSubnet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cidrBlock', (done) => {
        try {
          a.createSubnet('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'cidrBlock is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createSubnetSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpcId', (done) => {
        try {
          a.createSubnet('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'vpcId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createSubnetSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createSubnets - errors', () => {
      it('should have a createSubnets function', (done) => {
        try {
          assert.equal(true, typeof a.createSubnets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing subnetArray', (done) => {
        try {
          a.createSubnets(null, (data, error) => {
            try {
              const displayE = 'subnetArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createSubnetsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createTags - errors', () => {
      it('should have a createTags function', (done) => {
        try {
          assert.equal(true, typeof a.createTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceId', (done) => {
        try {
          a.createTags('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'resourceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createTagsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tag', (done) => {
        try {
          a.createTags('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'tag is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createTagsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createMultipleTags - errors', () => {
      it('should have a createMultipleTags function', (done) => {
        try {
          assert.equal(true, typeof a.createMultipleTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing MultipleTagsArray', (done) => {
        try {
          a.createMultipleTags(null, (data, error) => {
            try {
              const displayE = 'MultipleTagsArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createMultipleTagsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createTransitGateway - errors', () => {
      it('should have a createTransitGateway function', (done) => {
        try {
          assert.equal(true, typeof a.createTransitGateway === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createTransitGateways - errors', () => {
      it('should have a createTransitGateways function', (done) => {
        try {
          assert.equal(true, typeof a.createTransitGateways === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing TransitGatewaysArray', (done) => {
        try {
          a.createTransitGateways(null, (data, error) => {
            try {
              const displayE = 'TransitGatewaysArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createTransitGatewaysSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createTransitGatewayRoute - errors', () => {
      it('should have a createTransitGatewayRoute function', (done) => {
        try {
          assert.equal(true, typeof a.createTransitGatewayRoute === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing destinationCidrBlock', (done) => {
        try {
          a.createTransitGatewayRoute(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'destinationCidrBlock is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createTransitGatewayRouteSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing transitGatewayRouteTableId', (done) => {
        try {
          a.createTransitGatewayRoute('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'transitGatewayRouteTableId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createTransitGatewayRouteSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createTransitGatewayRoutes - errors', () => {
      it('should have a createTransitGatewayRoutes function', (done) => {
        try {
          assert.equal(true, typeof a.createTransitGatewayRoutes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing TransitGatewayRoutesArray', (done) => {
        try {
          a.createTransitGatewayRoutes(null, (data, error) => {
            try {
              const displayE = 'TransitGatewayRoutesArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createTransitGatewayRoutesSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createTransitGatewayRouteTable - errors', () => {
      it('should have a createTransitGatewayRouteTable function', (done) => {
        try {
          assert.equal(true, typeof a.createTransitGatewayRouteTable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing transitGatewayId', (done) => {
        try {
          a.createTransitGatewayRouteTable(null, null, null, (data, error) => {
            try {
              const displayE = 'transitGatewayId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createTransitGatewayRouteTableSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createTransitGatewayRouteTables - errors', () => {
      it('should have a createTransitGatewayRouteTables function', (done) => {
        try {
          assert.equal(true, typeof a.createTransitGatewayRouteTables === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing TransitGatewayRouteTablesArray', (done) => {
        try {
          a.createTransitGatewayRouteTables(null, (data, error) => {
            try {
              const displayE = 'TransitGatewayRouteTablesArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createTransitGatewayRouteTablesSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createTransitGatewayVpcAttachment - errors', () => {
      it('should have a createTransitGatewayVpcAttachment function', (done) => {
        try {
          assert.equal(true, typeof a.createTransitGatewayVpcAttachment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing transitGatewayId', (done) => {
        try {
          a.createTransitGatewayVpcAttachment(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'transitGatewayId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createTransitGatewayVpcAttachmentSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpcId', (done) => {
        try {
          a.createTransitGatewayVpcAttachment('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'vpcId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createTransitGatewayVpcAttachmentSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing subnetIds', (done) => {
        try {
          a.createTransitGatewayVpcAttachment('fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'subnetIds is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createTransitGatewayVpcAttachmentSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createTransitGatewayVpcAttachments - errors', () => {
      it('should have a createTransitGatewayVpcAttachments function', (done) => {
        try {
          assert.equal(true, typeof a.createTransitGatewayVpcAttachments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing TransitGatewayVpcAttachmentsArray', (done) => {
        try {
          a.createTransitGatewayVpcAttachments(null, (data, error) => {
            try {
              const displayE = 'TransitGatewayVpcAttachmentsArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createTransitGatewayVpcAttachmentsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createVolume - errors', () => {
      it('should have a createVolume function', (done) => {
        try {
          assert.equal(true, typeof a.createVolume === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing availabilityZone', (done) => {
        try {
          a.createVolume(null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'availabilityZone is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createVolumeSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createVolumes - errors', () => {
      it('should have a createVolumes function', (done) => {
        try {
          assert.equal(true, typeof a.createVolumes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing TransitGatewayVpcAttachmentssArray', (done) => {
        try {
          a.createVolumes(null, (data, error) => {
            try {
              const displayE = 'TransitGatewayVpcAttachmentssArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createVolumesSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createVpc - errors', () => {
      it('should have a createVpc function', (done) => {
        try {
          assert.equal(true, typeof a.createVpc === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cidrBlock', (done) => {
        try {
          a.createVpc(null, null, null, null, (data, error) => {
            try {
              const displayE = 'cidrBlock is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createVpcSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createVpcs - errors', () => {
      it('should have a createVpcs function', (done) => {
        try {
          assert.equal(true, typeof a.createVpcs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpcArray', (done) => {
        try {
          a.createVpcs(null, (data, error) => {
            try {
              const displayE = 'vpcArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createVpcsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createVpcEndpoint - errors', () => {
      it('should have a createVpcEndpoint function', (done) => {
        try {
          assert.equal(true, typeof a.createVpcEndpoint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpcId', (done) => {
        try {
          a.createVpcEndpoint('fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'vpcId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createVpcEndpointSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceName', (done) => {
        try {
          a.createVpcEndpoint('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'serviceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createVpcEndpointSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createVpcEndpoints - errors', () => {
      it('should have a createVpcEndpoints function', (done) => {
        try {
          assert.equal(true, typeof a.createVpcEndpoints === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing createVpcEndpointsArray', (done) => {
        try {
          a.createVpcEndpoints(null, (data, error) => {
            try {
              const displayE = 'createVpcEndpointsArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createVpcEndpointsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createVpcEndpointConnectionNotification - errors', () => {
      it('should have a createVpcEndpointConnectionNotification function', (done) => {
        try {
          assert.equal(true, typeof a.createVpcEndpointConnectionNotification === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectionNotificationArn', (done) => {
        try {
          a.createVpcEndpointConnectionNotification('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'connectionNotificationArn is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createVpcEndpointConnectionNotificationSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectionEvents', (done) => {
        try {
          a.createVpcEndpointConnectionNotification('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'connectionEvents is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createVpcEndpointConnectionNotificationSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createVpcEndpointConnectionNotifications - errors', () => {
      it('should have a createVpcEndpointConnectionNotifications function', (done) => {
        try {
          assert.equal(true, typeof a.createVpcEndpointConnectionNotifications === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing createVpcEndpointConnectionNotificationsArray', (done) => {
        try {
          a.createVpcEndpointConnectionNotifications(null, (data, error) => {
            try {
              const displayE = 'createVpcEndpointConnectionNotificationsArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createVpcEndpointConnectionNotificationsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createVpcEndpointServiceConfiguration - errors', () => {
      it('should have a createVpcEndpointServiceConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.createVpcEndpointServiceConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkLoadBalancerArn', (done) => {
        try {
          a.createVpcEndpointServiceConfiguration('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkLoadBalancerArn is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createVpcEndpointServiceConfigurationSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createVpcEndpointServiceConfigurations - errors', () => {
      it('should have a createVpcEndpointServiceConfigurations function', (done) => {
        try {
          assert.equal(true, typeof a.createVpcEndpointServiceConfigurations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing createVpcEndpointServiceConfigurationsArray', (done) => {
        try {
          a.createVpcEndpointServiceConfigurations(null, (data, error) => {
            try {
              const displayE = 'createVpcEndpointServiceConfigurationsArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createVpcEndpointServiceConfigurationsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createVpcPeeringConnection - errors', () => {
      it('should have a createVpcPeeringConnection function', (done) => {
        try {
          assert.equal(true, typeof a.createVpcPeeringConnection === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createVpcPeeringConnections - errors', () => {
      it('should have a createVpcPeeringConnections function', (done) => {
        try {
          assert.equal(true, typeof a.createVpcPeeringConnections === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing createVpcPeeringConnectionsArray', (done) => {
        try {
          a.createVpcPeeringConnections(null, (data, error) => {
            try {
              const displayE = 'createVpcPeeringConnectionsArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createVpcPeeringConnectionsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createVpnConnection - errors', () => {
      it('should have a createVpnConnection function', (done) => {
        try {
          assert.equal(true, typeof a.createVpnConnection === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing customerGatewayId', (done) => {
        try {
          a.createVpnConnection(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'customerGatewayId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createVpnConnectionSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.createVpnConnection('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createVpnConnectionSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createVpnConnections - errors', () => {
      it('should have a createVpnConnections function', (done) => {
        try {
          assert.equal(true, typeof a.createVpnConnections === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing createVpnConnectionsArray', (done) => {
        try {
          a.createVpnConnections(null, (data, error) => {
            try {
              const displayE = 'createVpnConnectionsArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createVpnConnectionsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createVpnConnectionRoute - errors', () => {
      it('should have a createVpnConnectionRoute function', (done) => {
        try {
          assert.equal(true, typeof a.createVpnConnectionRoute === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing destinationCidrBlock', (done) => {
        try {
          a.createVpnConnectionRoute(null, null, (data, error) => {
            try {
              const displayE = 'destinationCidrBlock is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createVpnConnectionRouteSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpnConnectionId', (done) => {
        try {
          a.createVpnConnectionRoute('fakeparam', null, (data, error) => {
            try {
              const displayE = 'vpnConnectionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createVpnConnectionRouteSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createVpnConnectionRoutes - errors', () => {
      it('should have a createVpnConnectionRoutes function', (done) => {
        try {
          assert.equal(true, typeof a.createVpnConnectionRoutes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing createVpnConnectionRoutesArray', (done) => {
        try {
          a.createVpnConnectionRoutes(null, (data, error) => {
            try {
              const displayE = 'createVpnConnectionRoutesArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createVpnConnectionRoutesSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createVpnGateway - errors', () => {
      it('should have a createVpnGateway function', (done) => {
        try {
          assert.equal(true, typeof a.createVpnGateway === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.createVpnGateway('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createVpnGatewaySTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createVpnGateways - errors', () => {
      it('should have a createVpnGateways function', (done) => {
        try {
          assert.equal(true, typeof a.createVpnGateways === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing createVpnGatewaysArray', (done) => {
        try {
          a.createVpnGateways(null, (data, error) => {
            try {
              const displayE = 'createVpnGatewaysArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-createVpnGatewaysSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteClientVpnEndpoint - errors', () => {
      it('should have a deleteClientVpnEndpoint function', (done) => {
        try {
          assert.equal(true, typeof a.deleteClientVpnEndpoint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clientVpnEndpointId', (done) => {
        try {
          a.deleteClientVpnEndpoint(null, null, (data, error) => {
            try {
              const displayE = 'clientVpnEndpointId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteClientVpnEndpointSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteClientVpnRoute - errors', () => {
      it('should have a deleteClientVpnRoute function', (done) => {
        try {
          assert.equal(true, typeof a.deleteClientVpnRoute === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clientVpnEndpointId', (done) => {
        try {
          a.deleteClientVpnRoute(null, null, null, null, (data, error) => {
            try {
              const displayE = 'clientVpnEndpointId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteClientVpnRouteSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing destinationCidrBlock', (done) => {
        try {
          a.deleteClientVpnRoute('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'destinationCidrBlock is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteClientVpnRouteSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCustomerGateway - errors', () => {
      it('should have a deleteCustomerGateway function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCustomerGateway === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing customerGatewayId', (done) => {
        try {
          a.deleteCustomerGateway(null, null, (data, error) => {
            try {
              const displayE = 'customerGatewayId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteCustomerGatewaySTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDhcpOptions - errors', () => {
      it('should have a deleteDhcpOptions function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDhcpOptions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dhcpOptionsId', (done) => {
        try {
          a.deleteDhcpOptions(null, null, (data, error) => {
            try {
              const displayE = 'dhcpOptionsId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteDhcpOptionsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteEgressOnlyInternetGateway - errors', () => {
      it('should have a deleteEgressOnlyInternetGateway function', (done) => {
        try {
          assert.equal(true, typeof a.deleteEgressOnlyInternetGateway === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing egressOnlyInternetGatewayId', (done) => {
        try {
          a.deleteEgressOnlyInternetGateway('fakeparam', null, (data, error) => {
            try {
              const displayE = 'egressOnlyInternetGatewayId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteEgressOnlyInternetGatewaySTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFleets - errors', () => {
      it('should have a deleteFleets function', (done) => {
        try {
          assert.equal(true, typeof a.deleteFleets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fleetId', (done) => {
        try {
          a.deleteFleets('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'fleetId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteFleetsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing terminateInstances', (done) => {
        try {
          a.deleteFleets('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'terminateInstances is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteFleetsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFlowLogs - errors', () => {
      it('should have a deleteFlowLogs function', (done) => {
        try {
          assert.equal(true, typeof a.deleteFlowLogs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing flowLogId', (done) => {
        try {
          a.deleteFlowLogs('fakeparam', null, (data, error) => {
            try {
              const displayE = 'flowLogId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteFlowLogsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFpgaImage - errors', () => {
      it('should have a deleteFpgaImage function', (done) => {
        try {
          assert.equal(true, typeof a.deleteFpgaImage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fpgaImageId', (done) => {
        try {
          a.deleteFpgaImage('fakeparam', null, (data, error) => {
            try {
              const displayE = 'fpgaImageId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteFpgaImageSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteInternetGateway - errors', () => {
      it('should have a deleteInternetGateway function', (done) => {
        try {
          assert.equal(true, typeof a.deleteInternetGateway === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing internetGatewayId', (done) => {
        try {
          a.deleteInternetGateway('fakeparam', null, (data, error) => {
            try {
              const displayE = 'internetGatewayId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteInternetGatewaySTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteInternetGateways - errors', () => {
      it('should have a deleteInternetGateways function', (done) => {
        try {
          assert.equal(true, typeof a.deleteInternetGateways === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing internetGatewayArray', (done) => {
        try {
          a.deleteInternetGateways(null, (data, error) => {
            try {
              const displayE = 'internetGatewayArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteInternetGatewaysSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteKeyPair - errors', () => {
      it('should have a deleteKeyPair function', (done) => {
        try {
          assert.equal(true, typeof a.deleteKeyPair === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing keyName', (done) => {
        try {
          a.deleteKeyPair(null, null, (data, error) => {
            try {
              const displayE = 'keyName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteKeyPairSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteLaunchTemplate - errors', () => {
      it('should have a deleteLaunchTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.deleteLaunchTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteLaunchTemplateVersions - errors', () => {
      it('should have a deleteLaunchTemplateVersions function', (done) => {
        try {
          assert.equal(true, typeof a.deleteLaunchTemplateVersions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing launchTemplateVersion', (done) => {
        try {
          a.deleteLaunchTemplateVersions('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'launchTemplateVersion is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteLaunchTemplateVersionsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNatGateway - errors', () => {
      it('should have a deleteNatGateway function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNatGateway === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing natGatewayId', (done) => {
        try {
          a.deleteNatGateway(null, (data, error) => {
            try {
              const displayE = 'natGatewayId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteNatGatewaySTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkAcl - errors', () => {
      it('should have a deleteNetworkAcl function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNetworkAcl === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkAclId', (done) => {
        try {
          a.deleteNetworkAcl('fakeparam', null, (data, error) => {
            try {
              const displayE = 'networkAclId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteNetworkAclSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkAclEntry - errors', () => {
      it('should have a deleteNetworkAclEntry function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNetworkAclEntry === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing egress', (done) => {
        try {
          a.deleteNetworkAclEntry('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'egress is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteNetworkAclEntrySTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkAclId', (done) => {
        try {
          a.deleteNetworkAclEntry('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkAclId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteNetworkAclEntrySTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleNumber', (done) => {
        try {
          a.deleteNetworkAclEntry('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'ruleNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteNetworkAclEntrySTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkInterface - errors', () => {
      it('should have a deleteNetworkInterface function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNetworkInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkInterfaceId', (done) => {
        try {
          a.deleteNetworkInterface('fakeparam', null, (data, error) => {
            try {
              const displayE = 'networkInterfaceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteNetworkInterfaceSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkInterfacePermission - errors', () => {
      it('should have a deleteNetworkInterfacePermission function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNetworkInterfacePermission === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkInterfacePermissionId', (done) => {
        try {
          a.deleteNetworkInterfacePermission(null, null, null, (data, error) => {
            try {
              const displayE = 'networkInterfacePermissionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteNetworkInterfacePermissionSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePlacementGroup - errors', () => {
      it('should have a deletePlacementGroup function', (done) => {
        try {
          assert.equal(true, typeof a.deletePlacementGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupName', (done) => {
        try {
          a.deletePlacementGroup('fakeparam', null, (data, error) => {
            try {
              const displayE = 'groupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deletePlacementGroupSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRoute - errors', () => {
      it('should have a deleteRoute function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRoute === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing routeTableId', (done) => {
        try {
          a.deleteRoute('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'routeTableId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteRouteSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRoutes - errors', () => {
      it('should have a deleteRoutes function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRoutes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing routeArray', (done) => {
        try {
          a.deleteRoutes(null, (data, error) => {
            try {
              const displayE = 'routeArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteRoutesSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRouteTables - errors', () => {
      it('should have a deleteRouteTables function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRouteTables === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing routeTableArray', (done) => {
        try {
          a.deleteRouteTables(null, (data, error) => {
            try {
              const displayE = 'routeTableArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteRouteTablesSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRouteTable - errors', () => {
      it('should have a deleteRouteTable function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRouteTable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing routeTableId', (done) => {
        try {
          a.deleteRouteTable('fakeparam', null, (data, error) => {
            try {
              const displayE = 'routeTableId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteRouteTableSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSecurityGroup - errors', () => {
      it('should have a deleteSecurityGroup function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSecurityGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSecurityGroups - errors', () => {
      it('should have a deleteSecurityGroups function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSecurityGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing securityGroupArray', (done) => {
        try {
          a.deleteSecurityGroups(null, (data, error) => {
            try {
              const displayE = 'securityGroupArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteSecurityGroupsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSnapshot - errors', () => {
      it('should have a deleteSnapshot function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSnapshot === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.deleteSnapshot(null, null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteSnapshotSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSpotDatafeedSubscription - errors', () => {
      it('should have a deleteSpotDatafeedSubscription function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSpotDatafeedSubscription === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSubnet - errors', () => {
      it('should have a deleteSubnet function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSubnet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing subnetId', (done) => {
        try {
          a.deleteSubnet(null, null, (data, error) => {
            try {
              const displayE = 'subnetId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteSubnetSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSubnets - errors', () => {
      it('should have a deleteSubnets function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSubnets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing subnetArray', (done) => {
        try {
          a.deleteSubnets(null, (data, error) => {
            try {
              const displayE = 'subnetArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteSubnetsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTags - errors', () => {
      it('should have a deleteTags function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceId', (done) => {
        try {
          a.deleteTags('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'resourceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteTagsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTransitGateway - errors', () => {
      it('should have a deleteTransitGateway function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTransitGateway === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing transitGatewayId', (done) => {
        try {
          a.deleteTransitGateway(null, null, (data, error) => {
            try {
              const displayE = 'transitGatewayId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteTransitGatewaySTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTransitGatewayRoute - errors', () => {
      it('should have a deleteTransitGatewayRoute function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTransitGatewayRoute === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing transitGatewayRouteTableId', (done) => {
        try {
          a.deleteTransitGatewayRoute(null, null, null, (data, error) => {
            try {
              const displayE = 'transitGatewayRouteTableId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteTransitGatewayRouteSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing destinationCidrBlock', (done) => {
        try {
          a.deleteTransitGatewayRoute('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'destinationCidrBlock is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteTransitGatewayRouteSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTransitGatewayRouteTable - errors', () => {
      it('should have a deleteTransitGatewayRouteTable function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTransitGatewayRouteTable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing transitGatewayRouteTableId', (done) => {
        try {
          a.deleteTransitGatewayRouteTable(null, null, (data, error) => {
            try {
              const displayE = 'transitGatewayRouteTableId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteTransitGatewayRouteTableSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTransitGatewayVpcAttachment - errors', () => {
      it('should have a deleteTransitGatewayVpcAttachment function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTransitGatewayVpcAttachment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing transitGatewayAttachmentId', (done) => {
        try {
          a.deleteTransitGatewayVpcAttachment(null, null, (data, error) => {
            try {
              const displayE = 'transitGatewayAttachmentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteTransitGatewayVpcAttachmentSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVolume - errors', () => {
      it('should have a deleteVolume function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVolume === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing volumeId', (done) => {
        try {
          a.deleteVolume(null, null, (data, error) => {
            try {
              const displayE = 'volumeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteVolumeSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVpc - errors', () => {
      it('should have a deleteVpc function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVpc === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpcId', (done) => {
        try {
          a.deleteVpc(null, null, (data, error) => {
            try {
              const displayE = 'vpcId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteVpcSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVpcs - errors', () => {
      it('should have a deleteVpcs function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVpcs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpcArray', (done) => {
        try {
          a.deleteVpcs(null, (data, error) => {
            try {
              const displayE = 'vpcArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteVpcsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVpcEndpointConnectionNotifications - errors', () => {
      it('should have a deleteVpcEndpointConnectionNotifications function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVpcEndpointConnectionNotifications === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectionNotificationId', (done) => {
        try {
          a.deleteVpcEndpointConnectionNotifications('fakeparam', null, (data, error) => {
            try {
              const displayE = 'connectionNotificationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteVpcEndpointConnectionNotificationsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVpcEndpointServiceConfigurations - errors', () => {
      it('should have a deleteVpcEndpointServiceConfigurations function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVpcEndpointServiceConfigurations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceId', (done) => {
        try {
          a.deleteVpcEndpointServiceConfigurations('fakeparam', null, (data, error) => {
            try {
              const displayE = 'serviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteVpcEndpointServiceConfigurationsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVpcEndpoints - errors', () => {
      it('should have a deleteVpcEndpoints function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVpcEndpoints === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpcEndpointId', (done) => {
        try {
          a.deleteVpcEndpoints('fakeparam', null, (data, error) => {
            try {
              const displayE = 'vpcEndpointId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteVpcEndpointsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVpcPeeringConnection - errors', () => {
      it('should have a deleteVpcPeeringConnection function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVpcPeeringConnection === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpcPeeringConnectionId', (done) => {
        try {
          a.deleteVpcPeeringConnection('fakeparam', null, (data, error) => {
            try {
              const displayE = 'vpcPeeringConnectionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteVpcPeeringConnectionSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVpnConnection - errors', () => {
      it('should have a deleteVpnConnection function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVpnConnection === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpnConnectionId', (done) => {
        try {
          a.deleteVpnConnection(null, null, (data, error) => {
            try {
              const displayE = 'vpnConnectionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteVpnConnectionSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVpnConnectionRoute - errors', () => {
      it('should have a deleteVpnConnectionRoute function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVpnConnectionRoute === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing destinationCidrBlock', (done) => {
        try {
          a.deleteVpnConnectionRoute(null, null, (data, error) => {
            try {
              const displayE = 'destinationCidrBlock is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteVpnConnectionRouteSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpnConnectionId', (done) => {
        try {
          a.deleteVpnConnectionRoute('fakeparam', null, (data, error) => {
            try {
              const displayE = 'vpnConnectionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteVpnConnectionRouteSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVpnGateway - errors', () => {
      it('should have a deleteVpnGateway function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVpnGateway === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpnGatewayId', (done) => {
        try {
          a.deleteVpnGateway(null, null, (data, error) => {
            try {
              const displayE = 'vpnGatewayId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteVpnGatewaySTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deprovisionByoipCidr - errors', () => {
      it('should have a deprovisionByoipCidr function', (done) => {
        try {
          assert.equal(true, typeof a.deprovisionByoipCidr === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cidr', (done) => {
        try {
          a.deprovisionByoipCidr(null, null, (data, error) => {
            try {
              const displayE = 'cidr is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deprovisionByoipCidrSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deregisterImage - errors', () => {
      it('should have a deregisterImage function', (done) => {
        try {
          assert.equal(true, typeof a.deregisterImage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing imageId', (done) => {
        try {
          a.deregisterImage(null, null, (data, error) => {
            try {
              const displayE = 'imageId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deregisterImageSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeAccountAttributes - errors', () => {
      it('should have a describeAccountAttributes function', (done) => {
        try {
          assert.equal(true, typeof a.describeAccountAttributes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeAddresses - errors', () => {
      it('should have a describeAddresses function', (done) => {
        try {
          assert.equal(true, typeof a.describeAddresses === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeAggregateIdFormat - errors', () => {
      it('should have a describeAggregateIdFormat function', (done) => {
        try {
          assert.equal(true, typeof a.describeAggregateIdFormat === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeAvailabilityZones - errors', () => {
      it('should have a describeAvailabilityZones function', (done) => {
        try {
          assert.equal(true, typeof a.describeAvailabilityZones === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeBundleTasks - errors', () => {
      it('should have a describeBundleTasks function', (done) => {
        try {
          assert.equal(true, typeof a.describeBundleTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeByoipCidrs - errors', () => {
      it('should have a describeByoipCidrs function', (done) => {
        try {
          assert.equal(true, typeof a.describeByoipCidrs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing maxResults', (done) => {
        try {
          a.describeByoipCidrs('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'maxResults is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-describeByoipCidrsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeCapacityReservations - errors', () => {
      it('should have a describeCapacityReservations function', (done) => {
        try {
          assert.equal(true, typeof a.describeCapacityReservations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeClassicLinkInstances - errors', () => {
      it('should have a describeClassicLinkInstances function', (done) => {
        try {
          assert.equal(true, typeof a.describeClassicLinkInstances === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeClientVpnAuthorizationRules - errors', () => {
      it('should have a describeClientVpnAuthorizationRules function', (done) => {
        try {
          assert.equal(true, typeof a.describeClientVpnAuthorizationRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clientVpnEndpointId', (done) => {
        try {
          a.describeClientVpnAuthorizationRules(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'clientVpnEndpointId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-describeClientVpnAuthorizationRulesSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeClientVpnConnections - errors', () => {
      it('should have a describeClientVpnConnections function', (done) => {
        try {
          assert.equal(true, typeof a.describeClientVpnConnections === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clientVpnEndpointId', (done) => {
        try {
          a.describeClientVpnConnections(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'clientVpnEndpointId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-describeClientVpnConnectionsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeClientVpnEndpoints - errors', () => {
      it('should have a describeClientVpnEndpoints function', (done) => {
        try {
          assert.equal(true, typeof a.describeClientVpnEndpoints === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeClientVpnRoutes - errors', () => {
      it('should have a describeClientVpnRoutes function', (done) => {
        try {
          assert.equal(true, typeof a.describeClientVpnRoutes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clientVpnEndpointId', (done) => {
        try {
          a.describeClientVpnRoutes(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'clientVpnEndpointId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-describeClientVpnRoutesSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeClientVpnTargetNetworks - errors', () => {
      it('should have a describeClientVpnTargetNetworks function', (done) => {
        try {
          assert.equal(true, typeof a.describeClientVpnTargetNetworks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clientVpnEndpointId', (done) => {
        try {
          a.describeClientVpnTargetNetworks(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'clientVpnEndpointId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-describeClientVpnTargetNetworksSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeConversionTasks - errors', () => {
      it('should have a describeConversionTasks function', (done) => {
        try {
          assert.equal(true, typeof a.describeConversionTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeCustomerGateways - errors', () => {
      it('should have a describeCustomerGateways function', (done) => {
        try {
          assert.equal(true, typeof a.describeCustomerGateways === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeDhcpOptions - errors', () => {
      it('should have a describeDhcpOptions function', (done) => {
        try {
          assert.equal(true, typeof a.describeDhcpOptions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeEgressOnlyInternetGateways - errors', () => {
      it('should have a describeEgressOnlyInternetGateways function', (done) => {
        try {
          assert.equal(true, typeof a.describeEgressOnlyInternetGateways === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeElasticGpus - errors', () => {
      it('should have a describeElasticGpus function', (done) => {
        try {
          assert.equal(true, typeof a.describeElasticGpus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeExportTasks - errors', () => {
      it('should have a describeExportTasks function', (done) => {
        try {
          assert.equal(true, typeof a.describeExportTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeFleetHistory - errors', () => {
      it('should have a describeFleetHistory function', (done) => {
        try {
          assert.equal(true, typeof a.describeFleetHistory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fleetId', (done) => {
        try {
          a.describeFleetHistory('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'fleetId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-describeFleetHistorySTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.describeFleetHistory('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-describeFleetHistorySTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeFleetInstances - errors', () => {
      it('should have a describeFleetInstances function', (done) => {
        try {
          assert.equal(true, typeof a.describeFleetInstances === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fleetId', (done) => {
        try {
          a.describeFleetInstances('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'fleetId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-describeFleetInstancesSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeFleets - errors', () => {
      it('should have a describeFleets function', (done) => {
        try {
          assert.equal(true, typeof a.describeFleets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeFlowLogs - errors', () => {
      it('should have a describeFlowLogs function', (done) => {
        try {
          assert.equal(true, typeof a.describeFlowLogs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeFpgaImageAttribute - errors', () => {
      it('should have a describeFpgaImageAttribute function', (done) => {
        try {
          assert.equal(true, typeof a.describeFpgaImageAttribute === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fpgaImageId', (done) => {
        try {
          a.describeFpgaImageAttribute('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'fpgaImageId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-describeFpgaImageAttributeSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attribute', (done) => {
        try {
          a.describeFpgaImageAttribute('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'attribute is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-describeFpgaImageAttributeSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeFpgaImages - errors', () => {
      it('should have a describeFpgaImages function', (done) => {
        try {
          assert.equal(true, typeof a.describeFpgaImages === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeHostReservationOfferings - errors', () => {
      it('should have a describeHostReservationOfferings function', (done) => {
        try {
          assert.equal(true, typeof a.describeHostReservationOfferings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeHostReservations - errors', () => {
      it('should have a describeHostReservations function', (done) => {
        try {
          assert.equal(true, typeof a.describeHostReservations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeHosts - errors', () => {
      it('should have a describeHosts function', (done) => {
        try {
          assert.equal(true, typeof a.describeHosts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeIamInstanceProfileAssociations - errors', () => {
      it('should have a describeIamInstanceProfileAssociations function', (done) => {
        try {
          assert.equal(true, typeof a.describeIamInstanceProfileAssociations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeIdFormat - errors', () => {
      it('should have a describeIdFormat function', (done) => {
        try {
          assert.equal(true, typeof a.describeIdFormat === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeIdentityIdFormat - errors', () => {
      it('should have a describeIdentityIdFormat function', (done) => {
        try {
          assert.equal(true, typeof a.describeIdentityIdFormat === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing principalArn', (done) => {
        try {
          a.describeIdentityIdFormat(null, null, (data, error) => {
            try {
              const displayE = 'principalArn is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-describeIdentityIdFormatSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeImageAttribute - errors', () => {
      it('should have a describeImageAttribute function', (done) => {
        try {
          assert.equal(true, typeof a.describeImageAttribute === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attribute', (done) => {
        try {
          a.describeImageAttribute(null, null, null, (data, error) => {
            try {
              const displayE = 'attribute is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-describeImageAttributeSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing imageId', (done) => {
        try {
          a.describeImageAttribute('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'imageId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-describeImageAttributeSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeImages - errors', () => {
      it('should have a describeImages function', (done) => {
        try {
          assert.equal(true, typeof a.describeImages === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeImportImageTasks - errors', () => {
      it('should have a describeImportImageTasks function', (done) => {
        try {
          assert.equal(true, typeof a.describeImportImageTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeImportSnapshotTasks - errors', () => {
      it('should have a describeImportSnapshotTasks function', (done) => {
        try {
          assert.equal(true, typeof a.describeImportSnapshotTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeInstanceAttribute - errors', () => {
      it('should have a describeInstanceAttribute function', (done) => {
        try {
          assert.equal(true, typeof a.describeInstanceAttribute === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attribute', (done) => {
        try {
          a.describeInstanceAttribute(null, null, null, (data, error) => {
            try {
              const displayE = 'attribute is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-describeInstanceAttributeSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing instanceId', (done) => {
        try {
          a.describeInstanceAttribute('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'instanceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-describeInstanceAttributeSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeInstanceCreditSpecifications - errors', () => {
      it('should have a describeInstanceCreditSpecifications function', (done) => {
        try {
          assert.equal(true, typeof a.describeInstanceCreditSpecifications === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeInstanceStatus - errors', () => {
      it('should have a describeInstanceStatus function', (done) => {
        try {
          assert.equal(true, typeof a.describeInstanceStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeInstances - errors', () => {
      it('should have a describeInstances function', (done) => {
        try {
          assert.equal(true, typeof a.describeInstances === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeInternetGateways - errors', () => {
      it('should have a describeInternetGateways function', (done) => {
        try {
          assert.equal(true, typeof a.describeInternetGateways === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeKeyPairs - errors', () => {
      it('should have a describeKeyPairs function', (done) => {
        try {
          assert.equal(true, typeof a.describeKeyPairs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeLaunchTemplateVersions - errors', () => {
      it('should have a describeLaunchTemplateVersions function', (done) => {
        try {
          assert.equal(true, typeof a.describeLaunchTemplateVersions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeLaunchTemplates - errors', () => {
      it('should have a describeLaunchTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.describeLaunchTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeMovingAddresses - errors', () => {
      it('should have a describeMovingAddresses function', (done) => {
        try {
          assert.equal(true, typeof a.describeMovingAddresses === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeNatGateways - errors', () => {
      it('should have a describeNatGateways function', (done) => {
        try {
          assert.equal(true, typeof a.describeNatGateways === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeNetworkAcls - errors', () => {
      it('should have a describeNetworkAcls function', (done) => {
        try {
          assert.equal(true, typeof a.describeNetworkAcls === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeNetworkInterfaceAttribute - errors', () => {
      it('should have a describeNetworkInterfaceAttribute function', (done) => {
        try {
          assert.equal(true, typeof a.describeNetworkInterfaceAttribute === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkInterfaceId', (done) => {
        try {
          a.describeNetworkInterfaceAttribute('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'networkInterfaceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-describeNetworkInterfaceAttributeSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeNetworkInterfacePermissions - errors', () => {
      it('should have a describeNetworkInterfacePermissions function', (done) => {
        try {
          assert.equal(true, typeof a.describeNetworkInterfacePermissions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeNetworkInterfaces - errors', () => {
      it('should have a describeNetworkInterfaces function', (done) => {
        try {
          assert.equal(true, typeof a.describeNetworkInterfaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describePlacementGroups - errors', () => {
      it('should have a describePlacementGroups function', (done) => {
        try {
          assert.equal(true, typeof a.describePlacementGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describePrefixLists - errors', () => {
      it('should have a describePrefixLists function', (done) => {
        try {
          assert.equal(true, typeof a.describePrefixLists === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describePrincipalIdFormat - errors', () => {
      it('should have a describePrincipalIdFormat function', (done) => {
        try {
          assert.equal(true, typeof a.describePrincipalIdFormat === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describePublicIpv4Pools - errors', () => {
      it('should have a describePublicIpv4Pools function', (done) => {
        try {
          assert.equal(true, typeof a.describePublicIpv4Pools === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeRegions - errors', () => {
      it('should have a describeRegions function', (done) => {
        try {
          assert.equal(true, typeof a.describeRegions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeReservedInstances - errors', () => {
      it('should have a describeReservedInstances function', (done) => {
        try {
          assert.equal(true, typeof a.describeReservedInstances === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeReservedInstancesListings - errors', () => {
      it('should have a describeReservedInstancesListings function', (done) => {
        try {
          assert.equal(true, typeof a.describeReservedInstancesListings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeReservedInstancesModifications - errors', () => {
      it('should have a describeReservedInstancesModifications function', (done) => {
        try {
          assert.equal(true, typeof a.describeReservedInstancesModifications === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeReservedInstancesOfferings - errors', () => {
      it('should have a describeReservedInstancesOfferings function', (done) => {
        try {
          assert.equal(true, typeof a.describeReservedInstancesOfferings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeRouteTables - errors', () => {
      it('should have a describeRouteTables function', (done) => {
        try {
          assert.equal(true, typeof a.describeRouteTables === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeScheduledInstanceAvailability - errors', () => {
      it('should have a describeScheduledInstanceAvailability function', (done) => {
        try {
          assert.equal(true, typeof a.describeScheduledInstanceAvailability === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeScheduledInstances - errors', () => {
      it('should have a describeScheduledInstances function', (done) => {
        try {
          assert.equal(true, typeof a.describeScheduledInstances === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeSecurityGroupReferences - errors', () => {
      it('should have a describeSecurityGroupReferences function', (done) => {
        try {
          assert.equal(true, typeof a.describeSecurityGroupReferences === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupId', (done) => {
        try {
          a.describeSecurityGroupReferences('fakeparam', null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-describeSecurityGroupReferencesSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeSecurityGroups - errors', () => {
      it('should have a describeSecurityGroups function', (done) => {
        try {
          assert.equal(true, typeof a.describeSecurityGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeSnapshotAttribute - errors', () => {
      it('should have a describeSnapshotAttribute function', (done) => {
        try {
          assert.equal(true, typeof a.describeSnapshotAttribute === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attribute', (done) => {
        try {
          a.describeSnapshotAttribute(null, null, null, (data, error) => {
            try {
              const displayE = 'attribute is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-describeSnapshotAttributeSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.describeSnapshotAttribute('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-describeSnapshotAttributeSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeSnapshots - errors', () => {
      it('should have a describeSnapshots function', (done) => {
        try {
          assert.equal(true, typeof a.describeSnapshots === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeSpotDatafeedSubscription - errors', () => {
      it('should have a describeSpotDatafeedSubscription function', (done) => {
        try {
          assert.equal(true, typeof a.describeSpotDatafeedSubscription === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeSpotFleetInstances - errors', () => {
      it('should have a describeSpotFleetInstances function', (done) => {
        try {
          assert.equal(true, typeof a.describeSpotFleetInstances === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing spotFleetRequestId', (done) => {
        try {
          a.describeSpotFleetInstances('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'spotFleetRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-describeSpotFleetInstancesSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeSpotFleetRequestHistory - errors', () => {
      it('should have a describeSpotFleetRequestHistory function', (done) => {
        try {
          assert.equal(true, typeof a.describeSpotFleetRequestHistory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing spotFleetRequestId', (done) => {
        try {
          a.describeSpotFleetRequestHistory('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'spotFleetRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-describeSpotFleetRequestHistorySTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.describeSpotFleetRequestHistory('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-describeSpotFleetRequestHistorySTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeSpotFleetRequests - errors', () => {
      it('should have a describeSpotFleetRequests function', (done) => {
        try {
          assert.equal(true, typeof a.describeSpotFleetRequests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeSpotInstanceRequests - errors', () => {
      it('should have a describeSpotInstanceRequests function', (done) => {
        try {
          assert.equal(true, typeof a.describeSpotInstanceRequests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeSpotPriceHistory - errors', () => {
      it('should have a describeSpotPriceHistory function', (done) => {
        try {
          assert.equal(true, typeof a.describeSpotPriceHistory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeStaleSecurityGroups - errors', () => {
      it('should have a describeStaleSecurityGroups function', (done) => {
        try {
          assert.equal(true, typeof a.describeStaleSecurityGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpcId', (done) => {
        try {
          a.describeStaleSecurityGroups('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'vpcId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-describeStaleSecurityGroupsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeSubnets - errors', () => {
      it('should have a describeSubnets function', (done) => {
        try {
          assert.equal(true, typeof a.describeSubnets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeTags - errors', () => {
      it('should have a describeTags function', (done) => {
        try {
          assert.equal(true, typeof a.describeTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeTransitGatewayAttachments - errors', () => {
      it('should have a describeTransitGatewayAttachments function', (done) => {
        try {
          assert.equal(true, typeof a.describeTransitGatewayAttachments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeTransitGatewayRouteTables - errors', () => {
      it('should have a describeTransitGatewayRouteTables function', (done) => {
        try {
          assert.equal(true, typeof a.describeTransitGatewayRouteTables === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeTransitGatewayVpcAttachments - errors', () => {
      it('should have a describeTransitGatewayVpcAttachments function', (done) => {
        try {
          assert.equal(true, typeof a.describeTransitGatewayVpcAttachments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeTransitGateways - errors', () => {
      it('should have a describeTransitGateways function', (done) => {
        try {
          assert.equal(true, typeof a.describeTransitGateways === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeVolumeAttribute - errors', () => {
      it('should have a describeVolumeAttribute function', (done) => {
        try {
          assert.equal(true, typeof a.describeVolumeAttribute === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attribute', (done) => {
        try {
          a.describeVolumeAttribute(null, null, null, (data, error) => {
            try {
              const displayE = 'attribute is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-describeVolumeAttributeSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing volumeId', (done) => {
        try {
          a.describeVolumeAttribute('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'volumeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-describeVolumeAttributeSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeVolumeStatus - errors', () => {
      it('should have a describeVolumeStatus function', (done) => {
        try {
          assert.equal(true, typeof a.describeVolumeStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeVolumes - errors', () => {
      it('should have a describeVolumes function', (done) => {
        try {
          assert.equal(true, typeof a.describeVolumes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeVolumesModifications - errors', () => {
      it('should have a describeVolumesModifications function', (done) => {
        try {
          assert.equal(true, typeof a.describeVolumesModifications === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeVpcAttribute - errors', () => {
      it('should have a describeVpcAttribute function', (done) => {
        try {
          assert.equal(true, typeof a.describeVpcAttribute === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attribute', (done) => {
        try {
          a.describeVpcAttribute(null, null, null, (data, error) => {
            try {
              const displayE = 'attribute is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-describeVpcAttributeSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpcId', (done) => {
        try {
          a.describeVpcAttribute('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'vpcId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-describeVpcAttributeSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeVpcClassicLink - errors', () => {
      it('should have a describeVpcClassicLink function', (done) => {
        try {
          assert.equal(true, typeof a.describeVpcClassicLink === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeVpcClassicLinkDnsSupport - errors', () => {
      it('should have a describeVpcClassicLinkDnsSupport function', (done) => {
        try {
          assert.equal(true, typeof a.describeVpcClassicLinkDnsSupport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeVpcEndpointConnectionNotifications - errors', () => {
      it('should have a describeVpcEndpointConnectionNotifications function', (done) => {
        try {
          assert.equal(true, typeof a.describeVpcEndpointConnectionNotifications === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeVpcEndpointConnections - errors', () => {
      it('should have a describeVpcEndpointConnections function', (done) => {
        try {
          assert.equal(true, typeof a.describeVpcEndpointConnections === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeVpcEndpointServiceConfigurations - errors', () => {
      it('should have a describeVpcEndpointServiceConfigurations function', (done) => {
        try {
          assert.equal(true, typeof a.describeVpcEndpointServiceConfigurations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeVpcEndpointServicePermissions - errors', () => {
      it('should have a describeVpcEndpointServicePermissions function', (done) => {
        try {
          assert.equal(true, typeof a.describeVpcEndpointServicePermissions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceId', (done) => {
        try {
          a.describeVpcEndpointServicePermissions('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'serviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-describeVpcEndpointServicePermissionsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeVpcEndpointServices - errors', () => {
      it('should have a describeVpcEndpointServices function', (done) => {
        try {
          assert.equal(true, typeof a.describeVpcEndpointServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeVpcEndpoints - errors', () => {
      it('should have a describeVpcEndpoints function', (done) => {
        try {
          assert.equal(true, typeof a.describeVpcEndpoints === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeVpcPeeringConnections - errors', () => {
      it('should have a describeVpcPeeringConnections function', (done) => {
        try {
          assert.equal(true, typeof a.describeVpcPeeringConnections === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeVpcs - errors', () => {
      it('should have a describeVpcs function', (done) => {
        try {
          assert.equal(true, typeof a.describeVpcs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeVpcDetails - errors', () => {
      it('should have a describeVpcDetails function', (done) => {
        try {
          assert.equal(true, typeof a.describeVpcDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpcId', (done) => {
        try {
          a.describeVpcDetails(null, (data, error) => {
            try {
              const displayE = 'vpcId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-describeVpcDetailsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeVpnConnections - errors', () => {
      it('should have a describeVpnConnections function', (done) => {
        try {
          assert.equal(true, typeof a.describeVpnConnections === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#describeVpnGateways - errors', () => {
      it('should have a describeVpnGateways function', (done) => {
        try {
          assert.equal(true, typeof a.describeVpnGateways === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#detachClassicLinkVpc - errors', () => {
      it('should have a detachClassicLinkVpc function', (done) => {
        try {
          assert.equal(true, typeof a.detachClassicLinkVpc === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing instanceId', (done) => {
        try {
          a.detachClassicLinkVpc('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'instanceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-detachClassicLinkVpcSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpcId', (done) => {
        try {
          a.detachClassicLinkVpc('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'vpcId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-detachClassicLinkVpcSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#detachInternetGateway - errors', () => {
      it('should have a detachInternetGateway function', (done) => {
        try {
          assert.equal(true, typeof a.detachInternetGateway === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing internetGatewayId', (done) => {
        try {
          a.detachInternetGateway('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'internetGatewayId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-detachInternetGatewaySTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpcId', (done) => {
        try {
          a.detachInternetGateway('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'vpcId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-detachInternetGatewaySTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#detachNetworkInterface - errors', () => {
      it('should have a detachNetworkInterface function', (done) => {
        try {
          assert.equal(true, typeof a.detachNetworkInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attachmentId', (done) => {
        try {
          a.detachNetworkInterface(null, null, null, (data, error) => {
            try {
              const displayE = 'attachmentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-detachNetworkInterfaceSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#detachVolume - errors', () => {
      it('should have a detachVolume function', (done) => {
        try {
          assert.equal(true, typeof a.detachVolume === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing volumeId', (done) => {
        try {
          a.detachVolume('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'volumeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-detachVolumeSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#detachVpnGateway - errors', () => {
      it('should have a detachVpnGateway function', (done) => {
        try {
          assert.equal(true, typeof a.detachVpnGateway === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpcId', (done) => {
        try {
          a.detachVpnGateway(null, null, null, (data, error) => {
            try {
              const displayE = 'vpcId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-detachVpnGatewaySTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpnGatewayId', (done) => {
        try {
          a.detachVpnGateway('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'vpnGatewayId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-detachVpnGatewaySTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteClientVpnEndpoints - errors', () => {
      it('should have a deleteClientVpnEndpoints function', (done) => {
        try {
          assert.equal(true, typeof a.deleteClientVpnEndpoints === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ClientVpnEndpointsArray', (done) => {
        try {
          a.deleteClientVpnEndpoints(null, (data, error) => {
            try {
              const displayE = 'ClientVpnEndpointsArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteClientVpnEndpointsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteClientVpnRoutes - errors', () => {
      it('should have a deleteClientVpnRoutes function', (done) => {
        try {
          assert.equal(true, typeof a.deleteClientVpnRoutes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ClientVpnRoutesArray', (done) => {
        try {
          a.deleteClientVpnRoutes(null, (data, error) => {
            try {
              const displayE = 'ClientVpnRoutesArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteClientVpnRoutesSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCustomerGateways - errors', () => {
      it('should have a deleteCustomerGateways function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCustomerGateways === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing CustomerGatewaysArray', (done) => {
        try {
          a.deleteCustomerGateways(null, (data, error) => {
            try {
              const displayE = 'CustomerGatewaysArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteCustomerGatewaysSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteMultipleDhcpOptions - errors', () => {
      it('should have a deleteMultipleDhcpOptions function', (done) => {
        try {
          assert.equal(true, typeof a.deleteMultipleDhcpOptions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing MultipleDhcpOptionsArray', (done) => {
        try {
          a.deleteMultipleDhcpOptions(null, (data, error) => {
            try {
              const displayE = 'MultipleDhcpOptionsArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteMultipleDhcpOptionsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteMultipleFleets - errors', () => {
      it('should have a deleteMultipleFleets function', (done) => {
        try {
          assert.equal(true, typeof a.deleteMultipleFleets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing MultipleFleetsArray', (done) => {
        try {
          a.deleteMultipleFleets(null, (data, error) => {
            try {
              const displayE = 'MultipleFleetsArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteMultipleFleetsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteEgressOnlyInternetGateways - errors', () => {
      it('should have a deleteEgressOnlyInternetGateways function', (done) => {
        try {
          assert.equal(true, typeof a.deleteEgressOnlyInternetGateways === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing EgressOnlyInternetGatewaysArray', (done) => {
        try {
          a.deleteEgressOnlyInternetGateways(null, (data, error) => {
            try {
              const displayE = 'EgressOnlyInternetGatewaysArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteEgressOnlyInternetGatewaysSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteFpgaImages - errors', () => {
      it('should have a deleteFpgaImages function', (done) => {
        try {
          assert.equal(true, typeof a.deleteFpgaImages === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing FpgaImagesArray', (done) => {
        try {
          a.deleteFpgaImages(null, (data, error) => {
            try {
              const displayE = 'FpgaImagesArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteFpgaImagesSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteMultipleFlowLogs - errors', () => {
      it('should have a deleteMultipleFlowLogs function', (done) => {
        try {
          assert.equal(true, typeof a.deleteMultipleFlowLogs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing MultipleFlowLogsArray', (done) => {
        try {
          a.deleteMultipleFlowLogs(null, (data, error) => {
            try {
              const displayE = 'MultipleFlowLogsArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteMultipleFlowLogsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteLaunchTemplates - errors', () => {
      it('should have a deleteLaunchTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.deleteLaunchTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing LaunchTemplatesArray', (done) => {
        try {
          a.deleteLaunchTemplates(null, (data, error) => {
            try {
              const displayE = 'LaunchTemplatesArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteLaunchTemplatesSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteKeyPairs - errors', () => {
      it('should have a deleteKeyPairs function', (done) => {
        try {
          assert.equal(true, typeof a.deleteKeyPairs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing KeyPairsArray', (done) => {
        try {
          a.deleteKeyPairs(null, (data, error) => {
            try {
              const displayE = 'KeyPairsArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteKeyPairsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteMultipleLaunchTemplateVersions - errors', () => {
      it('should have a deleteMultipleLaunchTemplateVersions function', (done) => {
        try {
          assert.equal(true, typeof a.deleteMultipleLaunchTemplateVersions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing MultipleLaunchTemplateVersionsArray', (done) => {
        try {
          a.deleteMultipleLaunchTemplateVersions(null, (data, error) => {
            try {
              const displayE = 'MultipleLaunchTemplateVersionsArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteMultipleLaunchTemplateVersionsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNatGateways - errors', () => {
      it('should have a deleteNatGateways function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNatGateways === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing NatGatewaysArray', (done) => {
        try {
          a.deleteNatGateways(null, (data, error) => {
            try {
              const displayE = 'NatGatewaysArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteNatGatewaysSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkAcls - errors', () => {
      it('should have a deleteNetworkAcls function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNetworkAcls === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing NetworkAclsArray', (done) => {
        try {
          a.deleteNetworkAcls(null, (data, error) => {
            try {
              const displayE = 'NetworkAclsArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteNetworkAclsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkAclEntries - errors', () => {
      it('should have a deleteNetworkAclEntries function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNetworkAclEntries === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing NetworkAclEntriesArray', (done) => {
        try {
          a.deleteNetworkAclEntries(null, (data, error) => {
            try {
              const displayE = 'NetworkAclEntriesArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteNetworkAclEntriesSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkInterfaces - errors', () => {
      it('should have a deleteNetworkInterfaces function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNetworkInterfaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing NetworkInterfacesArray', (done) => {
        try {
          a.deleteNetworkInterfaces(null, (data, error) => {
            try {
              const displayE = 'NetworkInterfacesArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteNetworkInterfacesSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkInterfacePermissions - errors', () => {
      it('should have a deleteNetworkInterfacePermissions function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNetworkInterfacePermissions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing NetworkInterfacePermissionsArray', (done) => {
        try {
          a.deleteNetworkInterfacePermissions(null, (data, error) => {
            try {
              const displayE = 'NetworkInterfacePermissionsArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteNetworkInterfacePermissionsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSnapshots - errors', () => {
      it('should have a deleteSnapshots function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSnapshots === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing SnapshotsArray', (done) => {
        try {
          a.deleteSnapshots(null, (data, error) => {
            try {
              const displayE = 'SnapshotsArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteSnapshotsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSpotDatafeedSubscriptions - errors', () => {
      it('should have a deleteSpotDatafeedSubscriptions function', (done) => {
        try {
          assert.equal(true, typeof a.deleteSpotDatafeedSubscriptions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing SpotDatafeedSubscriptionsArray', (done) => {
        try {
          a.deleteSpotDatafeedSubscriptions(null, (data, error) => {
            try {
              const displayE = 'SpotDatafeedSubscriptionsArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteSpotDatafeedSubscriptionsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTransitGateways - errors', () => {
      it('should have a deleteTransitGateways function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTransitGateways === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing TransitGatewaysArray', (done) => {
        try {
          a.deleteTransitGateways(null, (data, error) => {
            try {
              const displayE = 'TransitGatewaysArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteTransitGatewaysSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTransitGatewayRoutes - errors', () => {
      it('should have a deleteTransitGatewayRoutes function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTransitGatewayRoutes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing TransitGatewayRoutesArray', (done) => {
        try {
          a.deleteTransitGatewayRoutes(null, (data, error) => {
            try {
              const displayE = 'TransitGatewayRoutesArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteTransitGatewayRoutesSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTransitGatewayVpcAttachments - errors', () => {
      it('should have a deleteTransitGatewayVpcAttachments function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTransitGatewayVpcAttachments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing TransitGatewayVpcAttachmentsArray', (done) => {
        try {
          a.deleteTransitGatewayVpcAttachments(null, (data, error) => {
            try {
              const displayE = 'TransitGatewayVpcAttachmentsArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteTransitGatewayVpcAttachmentsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVolumes - errors', () => {
      it('should have a deleteVolumes function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVolumes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing VolumesArray', (done) => {
        try {
          a.deleteVolumes(null, (data, error) => {
            try {
              const displayE = 'VolumesArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteVolumesSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteMultipleVpcEndpointConnectionNotifications - errors', () => {
      it('should have a deleteMultipleVpcEndpointConnectionNotifications function', (done) => {
        try {
          assert.equal(true, typeof a.deleteMultipleVpcEndpointConnectionNotifications === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing MultipleVpcEndpointConnectionNotificationsArray', (done) => {
        try {
          a.deleteMultipleVpcEndpointConnectionNotifications(null, (data, error) => {
            try {
              const displayE = 'MultipleVpcEndpointConnectionNotificationsArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteMultipleVpcEndpointConnectionNotificationsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteMultipleVpcEndpointServiceConfigurations - errors', () => {
      it('should have a deleteMultipleVpcEndpointServiceConfigurations function', (done) => {
        try {
          assert.equal(true, typeof a.deleteMultipleVpcEndpointServiceConfigurations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing MultipleVpcEndpointServiceConfigurationsArray', (done) => {
        try {
          a.deleteMultipleVpcEndpointServiceConfigurations(null, (data, error) => {
            try {
              const displayE = 'MultipleVpcEndpointServiceConfigurationsArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteMultipleVpcEndpointServiceConfigurationsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVpcPeeringConnections - errors', () => {
      it('should have a deleteVpcPeeringConnections function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVpcPeeringConnections === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing VpcPeeringConnectionsArray', (done) => {
        try {
          a.deleteVpcPeeringConnections(null, (data, error) => {
            try {
              const displayE = 'VpcPeeringConnectionsArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteVpcPeeringConnectionsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteMultipleVpcEndpoints - errors', () => {
      it('should have a deleteMultipleVpcEndpoints function', (done) => {
        try {
          assert.equal(true, typeof a.deleteMultipleVpcEndpoints === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing MultipleVpcEndpointsArray', (done) => {
        try {
          a.deleteMultipleVpcEndpoints(null, (data, error) => {
            try {
              const displayE = 'MultipleVpcEndpointsArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteMultipleVpcEndpointsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVpnConnections - errors', () => {
      it('should have a deleteVpnConnections function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVpnConnections === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing VpnConnectionsArray', (done) => {
        try {
          a.deleteVpnConnections(null, (data, error) => {
            try {
              const displayE = 'VpnConnectionsArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteVpnConnectionsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVpnConnectionRoutes - errors', () => {
      it('should have a deleteVpnConnectionRoutes function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVpnConnectionRoutes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing VpnConnectionRoutesArray', (done) => {
        try {
          a.deleteVpnConnectionRoutes(null, (data, error) => {
            try {
              const displayE = 'VpnConnectionRoutesArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteVpnConnectionRoutesSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVpnGateways - errors', () => {
      it('should have a deleteVpnGateways function', (done) => {
        try {
          assert.equal(true, typeof a.deleteVpnGateways === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing VpnGatewaysArray', (done) => {
        try {
          a.deleteVpnGateways(null, (data, error) => {
            try {
              const displayE = 'VpnGatewaysArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteVpnGatewaysSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePlacementGroups - errors', () => {
      it('should have a deletePlacementGroups function', (done) => {
        try {
          assert.equal(true, typeof a.deletePlacementGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing PlacementGroupsArray', (done) => {
        try {
          a.deletePlacementGroups(null, (data, error) => {
            try {
              const displayE = 'PlacementGroupsArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deletePlacementGroupsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteMultipleTags - errors', () => {
      it('should have a deleteMultipleTags function', (done) => {
        try {
          assert.equal(true, typeof a.deleteMultipleTags === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing MultipleTagsArray', (done) => {
        try {
          a.deleteMultipleTags(null, (data, error) => {
            try {
              const displayE = 'MultipleTagsArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteMultipleTagsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTransitGatewayRouteTables - errors', () => {
      it('should have a deleteTransitGatewayRouteTables function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTransitGatewayRouteTables === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing TransitGatewayRouteTablesArray', (done) => {
        try {
          a.deleteTransitGatewayRouteTables(null, (data, error) => {
            try {
              const displayE = 'TransitGatewayRouteTablesArray is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-deleteTransitGatewayRouteTablesSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disableTransitGatewayRouteTablePropagation - errors', () => {
      it('should have a disableTransitGatewayRouteTablePropagation function', (done) => {
        try {
          assert.equal(true, typeof a.disableTransitGatewayRouteTablePropagation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing transitGatewayRouteTableId', (done) => {
        try {
          a.disableTransitGatewayRouteTablePropagation(null, null, null, (data, error) => {
            try {
              const displayE = 'transitGatewayRouteTableId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-disableTransitGatewayRouteTablePropagationSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing transitGatewayAttachmentId', (done) => {
        try {
          a.disableTransitGatewayRouteTablePropagation('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'transitGatewayAttachmentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-disableTransitGatewayRouteTablePropagationSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disableVgwRoutePropagation - errors', () => {
      it('should have a disableVgwRoutePropagation function', (done) => {
        try {
          assert.equal(true, typeof a.disableVgwRoutePropagation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing gatewayId', (done) => {
        try {
          a.disableVgwRoutePropagation(null, null, (data, error) => {
            try {
              const displayE = 'gatewayId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-disableVgwRoutePropagationSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing routeTableId', (done) => {
        try {
          a.disableVgwRoutePropagation('fakeparam', null, (data, error) => {
            try {
              const displayE = 'routeTableId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-disableVgwRoutePropagationSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disableVpcClassicLink - errors', () => {
      it('should have a disableVpcClassicLink function', (done) => {
        try {
          assert.equal(true, typeof a.disableVpcClassicLink === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpcId', (done) => {
        try {
          a.disableVpcClassicLink('fakeparam', null, (data, error) => {
            try {
              const displayE = 'vpcId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-disableVpcClassicLinkSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disableVpcClassicLinkDnsSupport - errors', () => {
      it('should have a disableVpcClassicLinkDnsSupport function', (done) => {
        try {
          assert.equal(true, typeof a.disableVpcClassicLinkDnsSupport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disassociateAddress - errors', () => {
      it('should have a disassociateAddress function', (done) => {
        try {
          assert.equal(true, typeof a.disassociateAddress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disassociateClientVpnTargetNetwork - errors', () => {
      it('should have a disassociateClientVpnTargetNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.disassociateClientVpnTargetNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clientVpnEndpointId', (done) => {
        try {
          a.disassociateClientVpnTargetNetwork(null, null, null, (data, error) => {
            try {
              const displayE = 'clientVpnEndpointId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-disassociateClientVpnTargetNetworkSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing associationId', (done) => {
        try {
          a.disassociateClientVpnTargetNetwork('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'associationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-disassociateClientVpnTargetNetworkSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disassociateIamInstanceProfile - errors', () => {
      it('should have a disassociateIamInstanceProfile function', (done) => {
        try {
          assert.equal(true, typeof a.disassociateIamInstanceProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing associationId', (done) => {
        try {
          a.disassociateIamInstanceProfile(null, (data, error) => {
            try {
              const displayE = 'associationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-disassociateIamInstanceProfileSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disassociateRouteTable - errors', () => {
      it('should have a disassociateRouteTable function', (done) => {
        try {
          assert.equal(true, typeof a.disassociateRouteTable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing associationId', (done) => {
        try {
          a.disassociateRouteTable(null, null, (data, error) => {
            try {
              const displayE = 'associationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-disassociateRouteTableSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disassociateSubnetCidrBlock - errors', () => {
      it('should have a disassociateSubnetCidrBlock function', (done) => {
        try {
          assert.equal(true, typeof a.disassociateSubnetCidrBlock === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing associationId', (done) => {
        try {
          a.disassociateSubnetCidrBlock(null, (data, error) => {
            try {
              const displayE = 'associationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-disassociateSubnetCidrBlockSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disassociateTransitGatewayRouteTable - errors', () => {
      it('should have a disassociateTransitGatewayRouteTable function', (done) => {
        try {
          assert.equal(true, typeof a.disassociateTransitGatewayRouteTable === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing transitGatewayRouteTableId', (done) => {
        try {
          a.disassociateTransitGatewayRouteTable(null, null, null, (data, error) => {
            try {
              const displayE = 'transitGatewayRouteTableId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-disassociateTransitGatewayRouteTableSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing transitGatewayAttachmentId', (done) => {
        try {
          a.disassociateTransitGatewayRouteTable('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'transitGatewayAttachmentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-disassociateTransitGatewayRouteTableSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disassociateVpcCidrBlock - errors', () => {
      it('should have a disassociateVpcCidrBlock function', (done) => {
        try {
          assert.equal(true, typeof a.disassociateVpcCidrBlock === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing associationId', (done) => {
        try {
          a.disassociateVpcCidrBlock(null, (data, error) => {
            try {
              const displayE = 'associationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-disassociateVpcCidrBlockSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#enableTransitGatewayRouteTablePropagation - errors', () => {
      it('should have a enableTransitGatewayRouteTablePropagation function', (done) => {
        try {
          assert.equal(true, typeof a.enableTransitGatewayRouteTablePropagation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing transitGatewayRouteTableId', (done) => {
        try {
          a.enableTransitGatewayRouteTablePropagation(null, null, null, (data, error) => {
            try {
              const displayE = 'transitGatewayRouteTableId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-enableTransitGatewayRouteTablePropagationSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing transitGatewayAttachmentId', (done) => {
        try {
          a.enableTransitGatewayRouteTablePropagation('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'transitGatewayAttachmentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-enableTransitGatewayRouteTablePropagationSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#enableVgwRoutePropagation - errors', () => {
      it('should have a enableVgwRoutePropagation function', (done) => {
        try {
          assert.equal(true, typeof a.enableVgwRoutePropagation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing gatewayId', (done) => {
        try {
          a.enableVgwRoutePropagation(null, null, (data, error) => {
            try {
              const displayE = 'gatewayId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-enableVgwRoutePropagationSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing routeTableId', (done) => {
        try {
          a.enableVgwRoutePropagation('fakeparam', null, (data, error) => {
            try {
              const displayE = 'routeTableId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-enableVgwRoutePropagationSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#enableVolumeIO - errors', () => {
      it('should have a enableVolumeIO function', (done) => {
        try {
          assert.equal(true, typeof a.enableVolumeIO === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing volumeId', (done) => {
        try {
          a.enableVolumeIO('fakeparam', null, (data, error) => {
            try {
              const displayE = 'volumeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-enableVolumeIOSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#enableVpcClassicLink - errors', () => {
      it('should have a enableVpcClassicLink function', (done) => {
        try {
          assert.equal(true, typeof a.enableVpcClassicLink === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpcId', (done) => {
        try {
          a.enableVpcClassicLink('fakeparam', null, (data, error) => {
            try {
              const displayE = 'vpcId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-enableVpcClassicLinkSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#enableVpcClassicLinkDnsSupport - errors', () => {
      it('should have a enableVpcClassicLinkDnsSupport function', (done) => {
        try {
          assert.equal(true, typeof a.enableVpcClassicLinkDnsSupport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#exportClientVpnClientCertificateRevocationList - errors', () => {
      it('should have a exportClientVpnClientCertificateRevocationList function', (done) => {
        try {
          assert.equal(true, typeof a.exportClientVpnClientCertificateRevocationList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clientVpnEndpointId', (done) => {
        try {
          a.exportClientVpnClientCertificateRevocationList(null, null, (data, error) => {
            try {
              const displayE = 'clientVpnEndpointId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-exportClientVpnClientCertificateRevocationListSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#exportClientVpnClientConfiguration - errors', () => {
      it('should have a exportClientVpnClientConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.exportClientVpnClientConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clientVpnEndpointId', (done) => {
        try {
          a.exportClientVpnClientConfiguration(null, null, (data, error) => {
            try {
              const displayE = 'clientVpnEndpointId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-exportClientVpnClientConfigurationSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#exportTransitGatewayRoutes - errors', () => {
      it('should have a exportTransitGatewayRoutes function', (done) => {
        try {
          assert.equal(true, typeof a.exportTransitGatewayRoutes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing transitGatewayRouteTableId', (done) => {
        try {
          a.exportTransitGatewayRoutes(null, null, null, null, (data, error) => {
            try {
              const displayE = 'transitGatewayRouteTableId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-exportTransitGatewayRoutesSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing s3Bucket', (done) => {
        try {
          a.exportTransitGatewayRoutes('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 's3Bucket is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-exportTransitGatewayRoutesSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getConsoleOutput - errors', () => {
      it('should have a getConsoleOutput function', (done) => {
        try {
          assert.equal(true, typeof a.getConsoleOutput === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing instanceId', (done) => {
        try {
          a.getConsoleOutput(null, null, null, (data, error) => {
            try {
              const displayE = 'instanceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-getConsoleOutputSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getConsoleScreenshot - errors', () => {
      it('should have a getConsoleScreenshot function', (done) => {
        try {
          assert.equal(true, typeof a.getConsoleScreenshot === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing instanceId', (done) => {
        try {
          a.getConsoleScreenshot('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'instanceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-getConsoleScreenshotSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHostReservationPurchasePreview - errors', () => {
      it('should have a getHostReservationPurchasePreview function', (done) => {
        try {
          assert.equal(true, typeof a.getHostReservationPurchasePreview === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing hostIdSet', (done) => {
        try {
          a.getHostReservationPurchasePreview(null, null, (data, error) => {
            try {
              const displayE = 'hostIdSet is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-getHostReservationPurchasePreviewSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing offeringId', (done) => {
        try {
          a.getHostReservationPurchasePreview('fakeparam', null, (data, error) => {
            try {
              const displayE = 'offeringId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-getHostReservationPurchasePreviewSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLaunchTemplateData - errors', () => {
      it('should have a getLaunchTemplateData function', (done) => {
        try {
          assert.equal(true, typeof a.getLaunchTemplateData === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing instanceId', (done) => {
        try {
          a.getLaunchTemplateData('fakeparam', null, (data, error) => {
            try {
              const displayE = 'instanceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-getLaunchTemplateDataSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPasswordData - errors', () => {
      it('should have a getPasswordData function', (done) => {
        try {
          assert.equal(true, typeof a.getPasswordData === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing instanceId', (done) => {
        try {
          a.getPasswordData(null, null, (data, error) => {
            try {
              const displayE = 'instanceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-getPasswordDataSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getReservedInstancesExchangeQuote - errors', () => {
      it('should have a getReservedInstancesExchangeQuote function', (done) => {
        try {
          assert.equal(true, typeof a.getReservedInstancesExchangeQuote === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing reservedInstanceId', (done) => {
        try {
          a.getReservedInstancesExchangeQuote('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'reservedInstanceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-getReservedInstancesExchangeQuoteSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTransitGatewayAttachmentPropagations - errors', () => {
      it('should have a getTransitGatewayAttachmentPropagations function', (done) => {
        try {
          assert.equal(true, typeof a.getTransitGatewayAttachmentPropagations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing transitGatewayAttachmentId', (done) => {
        try {
          a.getTransitGatewayAttachmentPropagations(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'transitGatewayAttachmentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-getTransitGatewayAttachmentPropagationsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTransitGatewayRouteTableAssociations - errors', () => {
      it('should have a getTransitGatewayRouteTableAssociations function', (done) => {
        try {
          assert.equal(true, typeof a.getTransitGatewayRouteTableAssociations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing transitGatewayRouteTableId', (done) => {
        try {
          a.getTransitGatewayRouteTableAssociations(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'transitGatewayRouteTableId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-getTransitGatewayRouteTableAssociationsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTransitGatewayRouteTablePropagations - errors', () => {
      it('should have a getTransitGatewayRouteTablePropagations function', (done) => {
        try {
          assert.equal(true, typeof a.getTransitGatewayRouteTablePropagations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing transitGatewayRouteTableId', (done) => {
        try {
          a.getTransitGatewayRouteTablePropagations(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'transitGatewayRouteTableId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-getTransitGatewayRouteTablePropagationsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importClientVpnClientCertificateRevocationList - errors', () => {
      it('should have a importClientVpnClientCertificateRevocationList function', (done) => {
        try {
          assert.equal(true, typeof a.importClientVpnClientCertificateRevocationList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clientVpnEndpointId', (done) => {
        try {
          a.importClientVpnClientCertificateRevocationList(null, null, null, (data, error) => {
            try {
              const displayE = 'clientVpnEndpointId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-importClientVpnClientCertificateRevocationListSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing certificateRevocationList', (done) => {
        try {
          a.importClientVpnClientCertificateRevocationList('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'certificateRevocationList is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-importClientVpnClientCertificateRevocationListSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importImage - errors', () => {
      it('should have a importImage function', (done) => {
        try {
          assert.equal(true, typeof a.importImage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importInstance - errors', () => {
      it('should have a importInstance function', (done) => {
        try {
          assert.equal(true, typeof a.importInstance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing platform', (done) => {
        try {
          a.importInstance('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'platform is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-importInstanceSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importKeyPair - errors', () => {
      it('should have a importKeyPair function', (done) => {
        try {
          assert.equal(true, typeof a.importKeyPair === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing keyName', (done) => {
        try {
          a.importKeyPair('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'keyName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-importKeyPairSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing publicKeyMaterial', (done) => {
        try {
          a.importKeyPair('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'publicKeyMaterial is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-importKeyPairSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importSnapshot - errors', () => {
      it('should have a importSnapshot function', (done) => {
        try {
          assert.equal(true, typeof a.importSnapshot === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#importVolume - errors', () => {
      it('should have a importVolume function', (done) => {
        try {
          assert.equal(true, typeof a.importVolume === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing availabilityZone', (done) => {
        try {
          a.importVolume(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'availabilityZone is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-importVolumeSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#modifyCapacityReservation - errors', () => {
      it('should have a modifyCapacityReservation function', (done) => {
        try {
          assert.equal(true, typeof a.modifyCapacityReservation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing capacityReservationId', (done) => {
        try {
          a.modifyCapacityReservation(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'capacityReservationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-modifyCapacityReservationSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#modifyClientVpnEndpoint - errors', () => {
      it('should have a modifyClientVpnEndpoint function', (done) => {
        try {
          assert.equal(true, typeof a.modifyClientVpnEndpoint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clientVpnEndpointId', (done) => {
        try {
          a.modifyClientVpnEndpoint(null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'clientVpnEndpointId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-modifyClientVpnEndpointSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#modifyFleet - errors', () => {
      it('should have a modifyFleet function', (done) => {
        try {
          assert.equal(true, typeof a.modifyFleet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fleetId', (done) => {
        try {
          a.modifyFleet('fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'fleetId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-modifyFleetSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#modifyFpgaImageAttribute - errors', () => {
      it('should have a modifyFpgaImageAttribute function', (done) => {
        try {
          assert.equal(true, typeof a.modifyFpgaImageAttribute === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fpgaImageId', (done) => {
        try {
          a.modifyFpgaImageAttribute('fakeparam', null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'fpgaImageId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-modifyFpgaImageAttributeSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#modifyHosts - errors', () => {
      it('should have a modifyHosts function', (done) => {
        try {
          assert.equal(true, typeof a.modifyHosts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing autoPlacement', (done) => {
        try {
          a.modifyHosts(null, null, (data, error) => {
            try {
              const displayE = 'autoPlacement is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-modifyHostsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing hostId', (done) => {
        try {
          a.modifyHosts('fakeparam', null, (data, error) => {
            try {
              const displayE = 'hostId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-modifyHostsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#modifyIdFormat - errors', () => {
      it('should have a modifyIdFormat function', (done) => {
        try {
          assert.equal(true, typeof a.modifyIdFormat === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resource', (done) => {
        try {
          a.modifyIdFormat(null, null, (data, error) => {
            try {
              const displayE = 'resource is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-modifyIdFormatSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing useLongIds', (done) => {
        try {
          a.modifyIdFormat('fakeparam', null, (data, error) => {
            try {
              const displayE = 'useLongIds is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-modifyIdFormatSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#modifyIdentityIdFormat - errors', () => {
      it('should have a modifyIdentityIdFormat function', (done) => {
        try {
          assert.equal(true, typeof a.modifyIdentityIdFormat === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing principalArn', (done) => {
        try {
          a.modifyIdentityIdFormat(null, null, null, (data, error) => {
            try {
              const displayE = 'principalArn is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-modifyIdentityIdFormatSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resource', (done) => {
        try {
          a.modifyIdentityIdFormat('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'resource is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-modifyIdentityIdFormatSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing useLongIds', (done) => {
        try {
          a.modifyIdentityIdFormat('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'useLongIds is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-modifyIdentityIdFormatSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#modifyImageAttribute - errors', () => {
      it('should have a modifyImageAttribute function', (done) => {
        try {
          assert.equal(true, typeof a.modifyImageAttribute === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing imageId', (done) => {
        try {
          a.modifyImageAttribute('fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'imageId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-modifyImageAttributeSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#modifyInstanceAttribute - errors', () => {
      it('should have a modifyInstanceAttribute function', (done) => {
        try {
          assert.equal(true, typeof a.modifyInstanceAttribute === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing instanceId', (done) => {
        try {
          a.modifyInstanceAttribute('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'instanceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-modifyInstanceAttributeSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#modifyInstanceCapacityReservationAttributes - errors', () => {
      it('should have a modifyInstanceCapacityReservationAttributes function', (done) => {
        try {
          assert.equal(true, typeof a.modifyInstanceCapacityReservationAttributes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing instanceId', (done) => {
        try {
          a.modifyInstanceCapacityReservationAttributes(null, null, null, null, (data, error) => {
            try {
              const displayE = 'instanceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-modifyInstanceCapacityReservationAttributesSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#modifyInstanceCreditSpecification - errors', () => {
      it('should have a modifyInstanceCreditSpecification function', (done) => {
        try {
          assert.equal(true, typeof a.modifyInstanceCreditSpecification === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing instanceCreditSpecification', (done) => {
        try {
          a.modifyInstanceCreditSpecification('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'instanceCreditSpecification is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-modifyInstanceCreditSpecificationSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#modifyInstanceEventStartTime - errors', () => {
      it('should have a modifyInstanceEventStartTime function', (done) => {
        try {
          assert.equal(true, typeof a.modifyInstanceEventStartTime === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing instanceId', (done) => {
        try {
          a.modifyInstanceEventStartTime('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'instanceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-modifyInstanceEventStartTimeSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing instanceEventId', (done) => {
        try {
          a.modifyInstanceEventStartTime('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'instanceEventId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-modifyInstanceEventStartTimeSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing notBefore', (done) => {
        try {
          a.modifyInstanceEventStartTime('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'notBefore is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-modifyInstanceEventStartTimeSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#modifyInstancePlacement - errors', () => {
      it('should have a modifyInstancePlacement function', (done) => {
        try {
          assert.equal(true, typeof a.modifyInstancePlacement === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing instanceId', (done) => {
        try {
          a.modifyInstancePlacement('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'instanceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-modifyInstancePlacementSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#modifyLaunchTemplate - errors', () => {
      it('should have a modifyLaunchTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.modifyLaunchTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#modifyNetworkInterfaceAttribute - errors', () => {
      it('should have a modifyNetworkInterfaceAttribute function', (done) => {
        try {
          assert.equal(true, typeof a.modifyNetworkInterfaceAttribute === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkInterfaceId', (done) => {
        try {
          a.modifyNetworkInterfaceAttribute('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkInterfaceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-modifyNetworkInterfaceAttributeSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#modifyReservedInstances - errors', () => {
      it('should have a modifyReservedInstances function', (done) => {
        try {
          assert.equal(true, typeof a.modifyReservedInstances === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing reservedInstancesId', (done) => {
        try {
          a.modifyReservedInstances(null, null, null, (data, error) => {
            try {
              const displayE = 'reservedInstancesId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-modifyReservedInstancesSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing reservedInstancesConfigurationSetItemType', (done) => {
        try {
          a.modifyReservedInstances('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'reservedInstancesConfigurationSetItemType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-modifyReservedInstancesSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#modifySnapshotAttribute - errors', () => {
      it('should have a modifySnapshotAttribute function', (done) => {
        try {
          assert.equal(true, typeof a.modifySnapshotAttribute === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.modifySnapshotAttribute('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-modifySnapshotAttributeSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#modifySpotFleetRequest - errors', () => {
      it('should have a modifySpotFleetRequest function', (done) => {
        try {
          assert.equal(true, typeof a.modifySpotFleetRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing spotFleetRequestId', (done) => {
        try {
          a.modifySpotFleetRequest('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'spotFleetRequestId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-modifySpotFleetRequestSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#modifySubnetAttribute - errors', () => {
      it('should have a modifySubnetAttribute function', (done) => {
        try {
          assert.equal(true, typeof a.modifySubnetAttribute === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing subnetId', (done) => {
        try {
          a.modifySubnetAttribute('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'subnetId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-modifySubnetAttributeSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#modifyTransitGatewayVpcAttachment - errors', () => {
      it('should have a modifyTransitGatewayVpcAttachment function', (done) => {
        try {
          assert.equal(true, typeof a.modifyTransitGatewayVpcAttachment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing transitGatewayAttachmentId', (done) => {
        try {
          a.modifyTransitGatewayVpcAttachment(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'transitGatewayAttachmentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-modifyTransitGatewayVpcAttachmentSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#modifyVolume - errors', () => {
      it('should have a modifyVolume function', (done) => {
        try {
          assert.equal(true, typeof a.modifyVolume === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing volumeId', (done) => {
        try {
          a.modifyVolume('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'volumeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-modifyVolumeSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#modifyVolumeAttribute - errors', () => {
      it('should have a modifyVolumeAttribute function', (done) => {
        try {
          assert.equal(true, typeof a.modifyVolumeAttribute === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing volumeId', (done) => {
        try {
          a.modifyVolumeAttribute('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'volumeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-modifyVolumeAttributeSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#modifyVpcAttribute - errors', () => {
      it('should have a modifyVpcAttribute function', (done) => {
        try {
          assert.equal(true, typeof a.modifyVpcAttribute === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpcId', (done) => {
        try {
          a.modifyVpcAttribute('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'vpcId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-modifyVpcAttributeSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#modifyVpcEndpoint - errors', () => {
      it('should have a modifyVpcEndpoint function', (done) => {
        try {
          assert.equal(true, typeof a.modifyVpcEndpoint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpcEndpointId', (done) => {
        try {
          a.modifyVpcEndpoint('fakeparam', null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'vpcEndpointId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-modifyVpcEndpointSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#modifyVpcEndpointConnectionNotification - errors', () => {
      it('should have a modifyVpcEndpointConnectionNotification function', (done) => {
        try {
          assert.equal(true, typeof a.modifyVpcEndpointConnectionNotification === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing connectionNotificationId', (done) => {
        try {
          a.modifyVpcEndpointConnectionNotification('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'connectionNotificationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-modifyVpcEndpointConnectionNotificationSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#modifyVpcEndpointServiceConfiguration - errors', () => {
      it('should have a modifyVpcEndpointServiceConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.modifyVpcEndpointServiceConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceId', (done) => {
        try {
          a.modifyVpcEndpointServiceConfiguration('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'serviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-modifyVpcEndpointServiceConfigurationSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#modifyVpcEndpointServicePermissions - errors', () => {
      it('should have a modifyVpcEndpointServicePermissions function', (done) => {
        try {
          assert.equal(true, typeof a.modifyVpcEndpointServicePermissions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceId', (done) => {
        try {
          a.modifyVpcEndpointServicePermissions('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'serviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-modifyVpcEndpointServicePermissionsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#modifyVpcPeeringConnectionOptions - errors', () => {
      it('should have a modifyVpcPeeringConnectionOptions function', (done) => {
        try {
          assert.equal(true, typeof a.modifyVpcPeeringConnectionOptions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpcPeeringConnectionId', (done) => {
        try {
          a.modifyVpcPeeringConnectionOptions('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'vpcPeeringConnectionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-modifyVpcPeeringConnectionOptionsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#modifyVpcTenancy - errors', () => {
      it('should have a modifyVpcTenancy function', (done) => {
        try {
          assert.equal(true, typeof a.modifyVpcTenancy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpcId', (done) => {
        try {
          a.modifyVpcTenancy(null, null, null, (data, error) => {
            try {
              const displayE = 'vpcId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-modifyVpcTenancySTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing instanceTenancy', (done) => {
        try {
          a.modifyVpcTenancy('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'instanceTenancy is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-modifyVpcTenancySTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#modifyVpnConnection - errors', () => {
      it('should have a modifyVpnConnection function', (done) => {
        try {
          assert.equal(true, typeof a.modifyVpnConnection === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpnConnectionId', (done) => {
        try {
          a.modifyVpnConnection(null, null, null, null, (data, error) => {
            try {
              const displayE = 'vpnConnectionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-modifyVpnConnectionSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#monitorInstances - errors', () => {
      it('should have a monitorInstances function', (done) => {
        try {
          assert.equal(true, typeof a.monitorInstances === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing instanceId', (done) => {
        try {
          a.monitorInstances(null, null, (data, error) => {
            try {
              const displayE = 'instanceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-monitorInstancesSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#moveAddressToVpc - errors', () => {
      it('should have a moveAddressToVpc function', (done) => {
        try {
          assert.equal(true, typeof a.moveAddressToVpc === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing publicIp', (done) => {
        try {
          a.moveAddressToVpc('fakeparam', null, (data, error) => {
            try {
              const displayE = 'publicIp is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-moveAddressToVpcSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#provisionByoipCidr - errors', () => {
      it('should have a provisionByoipCidr function', (done) => {
        try {
          assert.equal(true, typeof a.provisionByoipCidr === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cidr', (done) => {
        try {
          a.provisionByoipCidr(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'cidr is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-provisionByoipCidrSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#purchaseHostReservation - errors', () => {
      it('should have a purchaseHostReservation function', (done) => {
        try {
          assert.equal(true, typeof a.purchaseHostReservation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing hostIdSet', (done) => {
        try {
          a.purchaseHostReservation('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'hostIdSet is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-purchaseHostReservationSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing offeringId', (done) => {
        try {
          a.purchaseHostReservation('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'offeringId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-purchaseHostReservationSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#purchaseReservedInstancesOffering - errors', () => {
      it('should have a purchaseReservedInstancesOffering function', (done) => {
        try {
          assert.equal(true, typeof a.purchaseReservedInstancesOffering === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing instanceCount', (done) => {
        try {
          a.purchaseReservedInstancesOffering(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'instanceCount is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-purchaseReservedInstancesOfferingSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing reservedInstancesOfferingId', (done) => {
        try {
          a.purchaseReservedInstancesOffering('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'reservedInstancesOfferingId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-purchaseReservedInstancesOfferingSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#purchaseScheduledInstances - errors', () => {
      it('should have a purchaseScheduledInstances function', (done) => {
        try {
          assert.equal(true, typeof a.purchaseScheduledInstances === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing purchaseRequest', (done) => {
        try {
          a.purchaseScheduledInstances('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'purchaseRequest is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-purchaseScheduledInstancesSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rebootInstances - errors', () => {
      it('should have a rebootInstances function', (done) => {
        try {
          assert.equal(true, typeof a.rebootInstances === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing instanceId', (done) => {
        try {
          a.rebootInstances(null, null, (data, error) => {
            try {
              const displayE = 'instanceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-rebootInstancesSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#registerImage - errors', () => {
      it('should have a registerImage function', (done) => {
        try {
          assert.equal(true, typeof a.registerImage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing name', (done) => {
        try {
          a.registerImage('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'name is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-registerImageSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rejectTransitGatewayVpcAttachment - errors', () => {
      it('should have a rejectTransitGatewayVpcAttachment function', (done) => {
        try {
          assert.equal(true, typeof a.rejectTransitGatewayVpcAttachment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing transitGatewayAttachmentId', (done) => {
        try {
          a.rejectTransitGatewayVpcAttachment(null, null, (data, error) => {
            try {
              const displayE = 'transitGatewayAttachmentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-rejectTransitGatewayVpcAttachmentSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rejectVpcEndpointConnections - errors', () => {
      it('should have a rejectVpcEndpointConnections function', (done) => {
        try {
          assert.equal(true, typeof a.rejectVpcEndpointConnections === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serviceId', (done) => {
        try {
          a.rejectVpcEndpointConnections('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'serviceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-rejectVpcEndpointConnectionsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpcEndpointId', (done) => {
        try {
          a.rejectVpcEndpointConnections('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'vpcEndpointId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-rejectVpcEndpointConnectionsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rejectVpcPeeringConnection - errors', () => {
      it('should have a rejectVpcPeeringConnection function', (done) => {
        try {
          assert.equal(true, typeof a.rejectVpcPeeringConnection === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing vpcPeeringConnectionId', (done) => {
        try {
          a.rejectVpcPeeringConnection('fakeparam', null, (data, error) => {
            try {
              const displayE = 'vpcPeeringConnectionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-rejectVpcPeeringConnectionSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#releaseAddress - errors', () => {
      it('should have a releaseAddress function', (done) => {
        try {
          assert.equal(true, typeof a.releaseAddress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#releaseHosts - errors', () => {
      it('should have a releaseHosts function', (done) => {
        try {
          assert.equal(true, typeof a.releaseHosts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing hostId', (done) => {
        try {
          a.releaseHosts(null, (data, error) => {
            try {
              const displayE = 'hostId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-releaseHostsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceIamInstanceProfileAssociation - errors', () => {
      it('should have a replaceIamInstanceProfileAssociation function', (done) => {
        try {
          assert.equal(true, typeof a.replaceIamInstanceProfileAssociation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing associationId', (done) => {
        try {
          a.replaceIamInstanceProfileAssociation('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'associationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-replaceIamInstanceProfileAssociationSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceNetworkAclAssociation - errors', () => {
      it('should have a replaceNetworkAclAssociation function', (done) => {
        try {
          assert.equal(true, typeof a.replaceNetworkAclAssociation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing associationId', (done) => {
        try {
          a.replaceNetworkAclAssociation(null, null, null, (data, error) => {
            try {
              const displayE = 'associationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-replaceNetworkAclAssociationSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkAclId', (done) => {
        try {
          a.replaceNetworkAclAssociation('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'networkAclId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-replaceNetworkAclAssociationSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceNetworkAclEntry - errors', () => {
      it('should have a replaceNetworkAclEntry function', (done) => {
        try {
          assert.equal(true, typeof a.replaceNetworkAclEntry === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing egress', (done) => {
        try {
          a.replaceNetworkAclEntry('fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'egress is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-replaceNetworkAclEntrySTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkAclId', (done) => {
        try {
          a.replaceNetworkAclEntry('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'networkAclId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-replaceNetworkAclEntrySTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing protocol', (done) => {
        try {
          a.replaceNetworkAclEntry('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'protocol is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-replaceNetworkAclEntrySTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleAction', (done) => {
        try {
          a.replaceNetworkAclEntry('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'ruleAction is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-replaceNetworkAclEntrySTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ruleNumber', (done) => {
        try {
          a.replaceNetworkAclEntry('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'ruleNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-replaceNetworkAclEntrySTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceRoute - errors', () => {
      it('should have a replaceRoute function', (done) => {
        try {
          assert.equal(true, typeof a.replaceRoute === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing routeTableId', (done) => {
        try {
          a.replaceRoute('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'routeTableId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-replaceRouteSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceRouteTableAssociation - errors', () => {
      it('should have a replaceRouteTableAssociation function', (done) => {
        try {
          assert.equal(true, typeof a.replaceRouteTableAssociation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing associationId', (done) => {
        try {
          a.replaceRouteTableAssociation(null, null, null, (data, error) => {
            try {
              const displayE = 'associationId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-replaceRouteTableAssociationSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing routeTableId', (done) => {
        try {
          a.replaceRouteTableAssociation('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'routeTableId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-replaceRouteTableAssociationSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#replaceTransitGatewayRoute - errors', () => {
      it('should have a replaceTransitGatewayRoute function', (done) => {
        try {
          assert.equal(true, typeof a.replaceTransitGatewayRoute === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing destinationCidrBlock', (done) => {
        try {
          a.replaceTransitGatewayRoute(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'destinationCidrBlock is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-replaceTransitGatewayRouteSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing transitGatewayRouteTableId', (done) => {
        try {
          a.replaceTransitGatewayRoute('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'transitGatewayRouteTableId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-replaceTransitGatewayRouteSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reportInstanceStatus - errors', () => {
      it('should have a reportInstanceStatus function', (done) => {
        try {
          assert.equal(true, typeof a.reportInstanceStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing instanceId', (done) => {
        try {
          a.reportInstanceStatus('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'instanceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-reportInstanceStatusSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing reasonCode', (done) => {
        try {
          a.reportInstanceStatus('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'reasonCode is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-reportInstanceStatusSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing status', (done) => {
        try {
          a.reportInstanceStatus('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'status is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-reportInstanceStatusSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#requestSpotFleet - errors', () => {
      it('should have a requestSpotFleet function', (done) => {
        try {
          assert.equal(true, typeof a.requestSpotFleet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#requestSpotInstances - errors', () => {
      it('should have a requestSpotInstances function', (done) => {
        try {
          assert.equal(true, typeof a.requestSpotInstances === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#resetFpgaImageAttribute - errors', () => {
      it('should have a resetFpgaImageAttribute function', (done) => {
        try {
          assert.equal(true, typeof a.resetFpgaImageAttribute === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fpgaImageId', (done) => {
        try {
          a.resetFpgaImageAttribute('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'fpgaImageId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-resetFpgaImageAttributeSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#resetImageAttribute - errors', () => {
      it('should have a resetImageAttribute function', (done) => {
        try {
          assert.equal(true, typeof a.resetImageAttribute === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attribute', (done) => {
        try {
          a.resetImageAttribute(null, null, null, (data, error) => {
            try {
              const displayE = 'attribute is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-resetImageAttributeSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing imageId', (done) => {
        try {
          a.resetImageAttribute('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'imageId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-resetImageAttributeSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#resetInstanceAttribute - errors', () => {
      it('should have a resetInstanceAttribute function', (done) => {
        try {
          assert.equal(true, typeof a.resetInstanceAttribute === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attribute', (done) => {
        try {
          a.resetInstanceAttribute(null, null, null, (data, error) => {
            try {
              const displayE = 'attribute is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-resetInstanceAttributeSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing instanceId', (done) => {
        try {
          a.resetInstanceAttribute('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'instanceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-resetInstanceAttributeSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#resetNetworkInterfaceAttribute - errors', () => {
      it('should have a resetNetworkInterfaceAttribute function', (done) => {
        try {
          assert.equal(true, typeof a.resetNetworkInterfaceAttribute === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkInterfaceId', (done) => {
        try {
          a.resetNetworkInterfaceAttribute('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'networkInterfaceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-resetNetworkInterfaceAttributeSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#resetSnapshotAttribute - errors', () => {
      it('should have a resetSnapshotAttribute function', (done) => {
        try {
          assert.equal(true, typeof a.resetSnapshotAttribute === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attribute', (done) => {
        try {
          a.resetSnapshotAttribute(null, null, null, (data, error) => {
            try {
              const displayE = 'attribute is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-resetSnapshotAttributeSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing snapshotId', (done) => {
        try {
          a.resetSnapshotAttribute('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'snapshotId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-resetSnapshotAttributeSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#restoreAddressToClassic - errors', () => {
      it('should have a restoreAddressToClassic function', (done) => {
        try {
          assert.equal(true, typeof a.restoreAddressToClassic === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing publicIp', (done) => {
        try {
          a.restoreAddressToClassic('fakeparam', null, (data, error) => {
            try {
              const displayE = 'publicIp is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-restoreAddressToClassicSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#revokeClientVpnIngress - errors', () => {
      it('should have a revokeClientVpnIngress function', (done) => {
        try {
          assert.equal(true, typeof a.revokeClientVpnIngress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clientVpnEndpointId', (done) => {
        try {
          a.revokeClientVpnIngress(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'clientVpnEndpointId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-revokeClientVpnIngressSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing targetNetworkCidr', (done) => {
        try {
          a.revokeClientVpnIngress('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'targetNetworkCidr is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-revokeClientVpnIngressSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#revokeSecurityGroupEgress - errors', () => {
      it('should have a revokeSecurityGroupEgress function', (done) => {
        try {
          assert.equal(true, typeof a.revokeSecurityGroupEgress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupId', (done) => {
        try {
          a.revokeSecurityGroupEgress('fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-revokeSecurityGroupEgressSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#revokeSecurityGroupIngress - errors', () => {
      it('should have a revokeSecurityGroupIngress function', (done) => {
        try {
          assert.equal(true, typeof a.revokeSecurityGroupIngress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#runInstances - errors', () => {
      it('should have a runInstances function', (done) => {
        try {
          assert.equal(true, typeof a.runInstances === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing maxCount', (done) => {
        try {
          a.runInstances('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'maxCount is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-runInstancesSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing minCount', (done) => {
        try {
          a.runInstances('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'minCount is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-runInstancesSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#runScheduledInstances - errors', () => {
      it('should have a runScheduledInstances function', (done) => {
        try {
          assert.equal(true, typeof a.runScheduledInstances === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing scheduledInstanceId', (done) => {
        try {
          a.runScheduledInstances('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'scheduledInstanceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-runScheduledInstancesSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#searchTransitGatewayRoutes - errors', () => {
      it('should have a searchTransitGatewayRoutes function', (done) => {
        try {
          assert.equal(true, typeof a.searchTransitGatewayRoutes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing transitGatewayRouteTableId', (done) => {
        try {
          a.searchTransitGatewayRoutes(null, null, null, null, (data, error) => {
            try {
              const displayE = 'transitGatewayRouteTableId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-searchTransitGatewayRoutesSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filter', (done) => {
        try {
          a.searchTransitGatewayRoutes('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'filter is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-searchTransitGatewayRoutesSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#startInstances - errors', () => {
      it('should have a startInstances function', (done) => {
        try {
          assert.equal(true, typeof a.startInstances === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing instanceId', (done) => {
        try {
          a.startInstances(null, null, null, (data, error) => {
            try {
              const displayE = 'instanceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-startInstancesSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#stopInstances - errors', () => {
      it('should have a stopInstances function', (done) => {
        try {
          assert.equal(true, typeof a.stopInstances === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing instanceId', (done) => {
        try {
          a.stopInstances(null, null, null, null, (data, error) => {
            try {
              const displayE = 'instanceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-stopInstancesSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#terminateClientVpnConnections - errors', () => {
      it('should have a terminateClientVpnConnections function', (done) => {
        try {
          assert.equal(true, typeof a.terminateClientVpnConnections === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing clientVpnEndpointId', (done) => {
        try {
          a.terminateClientVpnConnections(null, null, null, null, (data, error) => {
            try {
              const displayE = 'clientVpnEndpointId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-terminateClientVpnConnectionsSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#terminateInstances - errors', () => {
      it('should have a terminateInstances function', (done) => {
        try {
          assert.equal(true, typeof a.terminateInstances === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing instanceId', (done) => {
        try {
          a.terminateInstances(null, null, (data, error) => {
            try {
              const displayE = 'instanceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-terminateInstancesSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#unassignIpv6Addresses - errors', () => {
      it('should have a unassignIpv6Addresses function', (done) => {
        try {
          assert.equal(true, typeof a.unassignIpv6Addresses === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ipv6Addresses', (done) => {
        try {
          a.unassignIpv6Addresses(null, null, (data, error) => {
            try {
              const displayE = 'ipv6Addresses is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-unassignIpv6AddressesSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkInterfaceId', (done) => {
        try {
          a.unassignIpv6Addresses('fakeparam', null, (data, error) => {
            try {
              const displayE = 'networkInterfaceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-unassignIpv6AddressesSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#unassignPrivateIpAddresses - errors', () => {
      it('should have a unassignPrivateIpAddresses function', (done) => {
        try {
          assert.equal(true, typeof a.unassignPrivateIpAddresses === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkInterfaceId', (done) => {
        try {
          a.unassignPrivateIpAddresses(null, null, (data, error) => {
            try {
              const displayE = 'networkInterfaceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-unassignPrivateIpAddressesSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing privateIpAddress', (done) => {
        try {
          a.unassignPrivateIpAddresses('fakeparam', null, (data, error) => {
            try {
              const displayE = 'privateIpAddress is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-unassignPrivateIpAddressesSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#unmonitorInstances - errors', () => {
      it('should have a unmonitorInstances function', (done) => {
        try {
          assert.equal(true, typeof a.unmonitorInstances === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing instanceId', (done) => {
        try {
          a.unmonitorInstances(null, null, (data, error) => {
            try {
              const displayE = 'instanceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-unmonitorInstancesSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateSecurityGroupRuleDescriptionsEgress - errors', () => {
      it('should have a updateSecurityGroupRuleDescriptionsEgress function', (done) => {
        try {
          assert.equal(true, typeof a.updateSecurityGroupRuleDescriptionsEgress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ipPermissions', (done) => {
        try {
          a.updateSecurityGroupRuleDescriptionsEgress('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'ipPermissions is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-updateSecurityGroupRuleDescriptionsEgressSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateSecurityGroupRuleDescriptionsIngress - errors', () => {
      it('should have a updateSecurityGroupRuleDescriptionsIngress function', (done) => {
        try {
          assert.equal(true, typeof a.updateSecurityGroupRuleDescriptionsIngress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ipPermissions', (done) => {
        try {
          a.updateSecurityGroupRuleDescriptionsIngress('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'ipPermissions is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-updateSecurityGroupRuleDescriptionsIngressSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#withdrawByoipCidr - errors', () => {
      it('should have a withdrawByoipCidr function', (done) => {
        try {
          assert.equal(true, typeof a.withdrawByoipCidr === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cidr', (done) => {
        try {
          a.withdrawByoipCidr(null, null, (data, error) => {
            try {
              const displayE = 'cidr is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-aws_ec2-adapter-withdrawByoipCidrSTSRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
